-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 23, 2019 at 03:43 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sipolarfix`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `id` int(11) NOT NULL,
  `nama_admin` varchar(150) NOT NULL,
  `email_admin` varchar(150) NOT NULL,
  `password` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`id`, `nama_admin`, `email_admin`, `password`) VALUES
(1, 'dosen cakep ku', 'dosencakepku@gmail.com', 'dosencakep@gmail.com'),
(2, 'dosen cantik', 'dosencantik@gmail.com', 'dosencantik@gmail.com'),
(3, 'gema antika hariadi', 'gemaantikahr@gmail.com', '37cfefd8b28269106a4201ca5f217ddb39e037ff');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_barang`
--

CREATE TABLE `tbl_barang` (
  `id` int(11) NOT NULL,
  `id_ruang` int(11) NOT NULL,
  `nama_barang` varchar(150) NOT NULL,
  `banyak_barang` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `kepemilikan` varchar(150) NOT NULL,
  `kondisi` char(50) NOT NULL,
  `tanggal` date NOT NULL,
  `tanggal_cek` date NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_barang`
--

INSERT INTO `tbl_barang` (`id`, `id_ruang`, `nama_barang`, `banyak_barang`, `keterangan`, `kepemilikan`, `kondisi`, `tanggal`, `tanggal_cek`, `status`) VALUES
(1, 6, 'CPU komputer praktikan', 40, '', 'kampus', 'terawat', '2019-09-10', '2019-09-17', 0),
(5, 3, 'CPU komputer lama - praktik bongkar pasang', 5, 'cakeep', 'kampus', 'terawat', '2019-09-10', '2019-10-01', 1),
(6, 3, 'Printer Laboran', 1, '', 'kampus', 'terawat', '2019-09-10', '2019-10-23', 1),
(7, 3, 'Ethernet Switch', 5, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(8, 3, 'Ethernet Switch ( belum di pakai)', 3, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(9, 3, 'Routerboard ', 1, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(10, 3, 'Routerboard Mikrotik ', 2, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(11, 3, 'Wireless Speaker', 1, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(12, 3, 'LCD Projector', 1, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(13, 3, 'Layar Proyektor ', 1, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(14, 3, 'AC ', 4, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(15, 3, 'Atraktor ', 1, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(16, 3, 'Tang Crimping', 1, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(17, 3, 'Cable Tester', 1, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(18, 4, 'CPU komputer praktikan', 40, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(19, 4, 'CPU Komputer Asisten ', 2, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(20, 4, 'LCD Proyektor', 1, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(21, 4, 'Layar Proyektor ', 1, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(22, 4, 'Wireless Speaker', 1, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(23, 4, 'Hub 32 Port', 1, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(24, 4, 'Hub 16 Port', 2, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(25, 4, 'AC ', 3, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(26, 4, 'Spliter Monitor', 1, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(27, 4, 'Papan Tulis', 1, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(28, 4, 'Higrometer', 1, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(29, 4, 'Almari', 1, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(30, 4, 'GPS Garmin', 3, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(31, 6, 'CPU Komputer Praktikan ', 32, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(32, 6, 'CPU Komputer Asisten ', 2, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(33, 6, 'CPU Komputer Laboran', 1, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(34, 6, 'Printer Laboran HP', 1, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(35, 6, 'CPU Komputer Ka.Lab', 1, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(36, 6, 'Printer Ka.Lab', 1, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(37, 6, 'Hp Samsung', 6, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(38, 6, 'LCD Proyektor', 1, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(39, 6, 'Layar Proyektor ', 1, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(40, 6, 'Hub 24 Port', 2, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(41, 6, 'Hub 16 Port', 1, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(42, 6, 'Tang Crimping', 5, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(43, 6, 'Cable Tester', 5, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(44, 6, 'Wireless Home Router', 5, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(45, 6, 'Wireless LAN Card ', 5, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(46, 6, 'Wireless Speaker', 1, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(47, 6, 'Splitter Monitor ', 1, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(48, 6, 'Mikrotik Router ', 28, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(49, 6, 'Mikrotik Cloud Router ', 1, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(50, 6, 'Cisco Router', 1, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(51, 6, 'Papan Tulis', 1, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(52, 6, 'Speaker Aktif ', 1, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(53, 6, 'Higrometer ', 1, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(54, 6, 'Almari ', 1, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(55, 6, 'Filling Cabinet', 1, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(56, 5, 'CPU Komputer Praktikan ', 26, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(57, 5, 'CPU Komputer editing', 2, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(58, 5, 'CPU Komputer Laboran', 1, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(59, 5, 'CPU Komputer Router ', 1, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(60, 5, 'Hub Switch', 3, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(61, 5, 'Air Conditioner', 3, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(62, 5, 'Papan Tulis', 2, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(63, 5, 'HandyCam', 5, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(64, 5, 'Printer ', 1, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(65, 5, 'LCD Projector', 1, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(66, 5, 'LCD Screen', 1, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(67, 5, 'VGA Splitter', 1, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(68, 5, 'Tang Crimping', 1, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(69, 5, 'Higrometer', 1, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(70, 5, 'Wireless Speaker ', 1, '', 'kampus', 'terawat', '2019-09-10', '0000-00-00', 0),
(71, 4, 'Printer', 30, 'oke', 'kampus', 'terawat', '2019-09-24', '0000-00-00', 0),
(72, 4, 'CPU komputer praktikan', 44, '', '', '', '2019-10-04', '0000-00-00', 0),
(73, 3, 'cintaku adalah cintamu', 3, 'aw', 'sewa', 'tidak terawat', '2019-10-04', '0000-00-00', 0),
(74, 5, 'islam bangkit', 2020, 'pokoknya bangkit coy', 'Kampus', 'Terawat', '2019-10-13', '2019-10-13', 0),
(75, 3, 'kamera', 4, 'tiap hari di bersihin', 'Kampus', 'Terawat', '2019-10-15', '0000-00-00', 0),
(76, 3, 'xiaomi redmi', 20, 'mantap', 'Kampus', 'Terawat', '2019-10-25', '0000-00-00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_barang_cadangan`
--

CREATE TABLE `tbl_barang_cadangan` (
  `id_barang` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `id_ruang` int(11) NOT NULL,
  `nama_barang` varchar(150) NOT NULL,
  `banyak_barang` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `kepemilikan` varchar(150) NOT NULL,
  `kondisi` char(50) NOT NULL,
  `tanggal` date NOT NULL,
  `tanggal_cek` date NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_barang_cadangan`
--

INSERT INTO `tbl_barang_cadangan` (`id_barang`, `id`, `id_ruang`, `nama_barang`, `banyak_barang`, `keterangan`, `kepemilikan`, `kondisi`, `tanggal`, `tanggal_cek`, `status`) VALUES
(1, 5, 3, 'CPU komputer lama - praktik bongkar pasang', 1234, 'buat main aja', 'kampus', 'terawat', '2019-10-15', '0000-00-00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_jam`
--

CREATE TABLE `tbl_jam` (
  `id_jam` int(11) NOT NULL,
  `mulai` time NOT NULL,
  `jam_ke` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_jam`
--

INSERT INTO `tbl_jam` (`id_jam`, `mulai`, `jam_ke`) VALUES
(1, '07:30:00', 1),
(2, '09:00:00', 2),
(3, '10:30:00', 3),
(4, '12:00:00', 4),
(5, '13:30:00', 5),
(6, '15:00:00', 6),
(7, '16:30:00', 7),
(8, '18:00:00', 8),
(9, '19:30:00', 9),
(10, '21:00:00', 10);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_jam_temp`
--

CREATE TABLE `tbl_jam_temp` (
  `id` int(11) NOT NULL,
  `jam_ke` int(11) NOT NULL,
  `mulai` char(20) NOT NULL,
  `selesai` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_jam_temp`
--

INSERT INTO `tbl_jam_temp` (`id`, `jam_ke`, `mulai`, `selesai`) VALUES
(1, 1, '7:30 - 9:00', '7:30 - 9:00'),
(2, 2, '9:00 - 10:30', '9:00 - 10:30'),
(3, 3, '10:30 - 12:00', '10:30 - 12:00'),
(4, 4, '12:00 - 13:30', '12:00 - 13:30'),
(5, 5, '13:30 - 15:00', '13:30 - 15:00'),
(6, 6, '15:00 - 16:30', '15:00 - 16:30'),
(7, 7, '16:30 - 18:00', '16:30 - 18:00'),
(8, 8, '18.00 - 19.30', '18.00 - 19.30'),
(9, 9, '19.30 - 21.00', '19.30 - 21.00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_komputer`
--

CREATE TABLE `tbl_komputer` (
  `id_komputer` int(11) NOT NULL,
  `kode_komputer` varchar(150) NOT NULL,
  `spek_komputer` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_komputer`
--

INSERT INTO `tbl_komputer` (`id_komputer`, `kode_komputer`, `spek_komputer`) VALUES
(1, '1', 'Core I 1'),
(2, '2', 'Core I 2'),
(3, '3', 'Core I 3'),
(4, '4', 'Core I 4'),
(5, '5', 'Core I 5'),
(6, '6', 'Core I 6'),
(7, '7', 'Core I 7'),
(8, '8', 'Core I 8'),
(9, '9', 'Core I 9'),
(10, '10', 'Core I 10'),
(11, '11', 'Core I 11'),
(12, '12', 'Core I 12'),
(13, '13', 'Core I 13'),
(14, '14', 'Core I 14'),
(15, '15', 'Core I 15'),
(16, '16', 'Core I 16'),
(17, '17', 'Core I 17'),
(18, '18', 'Core I 18'),
(19, '19', 'Core I 19'),
(20, '20', 'Core I 20'),
(21, '21', 'Core I 21'),
(22, '22', 'Core I 22'),
(23, '23', 'Core I 23'),
(24, '24', 'Core I 24'),
(25, '25', 'Core I 25'),
(26, '26', 'Core I 26'),
(27, '27', 'Core I 27'),
(28, '28', 'Core I 28'),
(29, '29', 'Core I 29'),
(30, '30', 'Core I 30'),
(31, '31', 'Core I 31'),
(32, '32', 'Core I 32'),
(33, '33', 'Core I 33'),
(34, '34', 'Core I 34'),
(35, '35', 'Core I 35'),
(36, '36', 'Core I 36'),
(37, '37', 'Core I 37'),
(38, '38', 'Core I 38'),
(39, '39', 'Core I 39'),
(40, '40', 'Core I 40'),
(41, '41', 'Core I 41'),
(42, '42', 'Core I 42');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_lapor_kejadian`
--

CREATE TABLE `tbl_lapor_kejadian` (
  `id` int(11) NOT NULL,
  `nama_kejadian` varchar(150) NOT NULL,
  `tempat_kejadian` varchar(150) NOT NULL,
  `foto_kejadian` varchar(150) NOT NULL,
  `create_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_mahasiswa`
--

CREATE TABLE `tbl_mahasiswa` (
  `id` int(11) NOT NULL,
  `nim` varchar(50) NOT NULL,
  `nama_mahasiswa` varchar(150) NOT NULL,
  `email_mahasiswa` varchar(150) NOT NULL,
  `password` varchar(150) NOT NULL,
  `aktif` int(11) NOT NULL,
  `kode` varchar(150) NOT NULL,
  `no_hp` char(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_mahasiswa`
--

INSERT INTO `tbl_mahasiswa` (`id`, `nim`, `nama_mahasiswa`, `email_mahasiswa`, `password`, `aktif`, `kode`, `no_hp`) VALUES
(14, '161', 'mahasiswa1', 'mahasiswa1@gmail.com', 'db76d156fab9fae03712079c61689536', 1, 'qgVwiadDGOhm', ''),
(15, '162', 'mahasiswa2', 'mahasiswa2@gmail.com', 'b62cdfbd51bb423ee46b66dd3773aa4a', 1, 'q9RbPtvlr13p', ''),
(16, '163', 'mahasiswa3', 'mahasiswa3@gmail.com', '9bd71c424a7aac330a6679e422753485', 1, 'lGnFpD9ftV2w', '0896809882325'),
(17, '1600018095', 'gema antika hariadi', 'gemaantikahr@gmail.com', '7772c4f394bc0b0d54de4361274cea7b', 1, 'Eu9RTvW5ZgfD', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_peminjaman`
--

CREATE TABLE `tbl_peminjaman` (
  `id` int(11) NOT NULL,
  `nim_mahasiswa` varchar(150) NOT NULL,
  `id_ruang` int(11) NOT NULL,
  `tgl_pinjam` date NOT NULL,
  `jam_mulai` time NOT NULL,
  `jam_selesai` time NOT NULL,
  `durasi` varchar(150) NOT NULL,
  `keterangan` varchar(150) NOT NULL,
  `status` int(11) NOT NULL,
  `alasan` varchar(150) NOT NULL,
  `create_at` datetime NOT NULL,
  `kode_peminjaman` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_peminjaman`
--

INSERT INTO `tbl_peminjaman` (`id`, `nim_mahasiswa`, `id_ruang`, `tgl_pinjam`, `jam_mulai`, `jam_selesai`, `durasi`, `keterangan`, `status`, `alasan`, `create_at`, `kode_peminjaman`) VALUES
(21, '1600018095', 7, '2019-11-23', '07:30:00', '21:00:00', '1,2,3,4,5,6,7,8,9,10', 'boking semua', 1, '', '2019-11-23 20:10:15', '1600018095715'),
(22, '1600018095', 3, '2019-11-23', '09:00:00', '21:00:00', '2,10', 'fet', 1, '', '2019-11-23 20:13:06', '1600018095306');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_peminjaman_alat`
--

CREATE TABLE `tbl_peminjaman_alat` (
  `id_peminjaman` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `nim_mahasiswa` varchar(150) NOT NULL,
  `tgl_pinjam` date NOT NULL,
  `lama_peminjaman` int(11) NOT NULL,
  `tgl_kembali` date NOT NULL,
  `tgl_pengembalian` date NOT NULL,
  `tujuan` text NOT NULL,
  `id_status` int(11) NOT NULL,
  `alasan_penolakan` text NOT NULL,
  `create_at` date NOT NULL,
  `kode_peminjaman` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_peminjaman_alat`
--

INSERT INTO `tbl_peminjaman_alat` (`id_peminjaman`, `id`, `nim_mahasiswa`, `tgl_pinjam`, `lama_peminjaman`, `tgl_kembali`, `tgl_pengembalian`, `tujuan`, `id_status`, `alasan_penolakan`, `create_at`, `kode_peminjaman`) VALUES
(17, 76, '1600018095', '2019-11-20', 3, '2019-11-22', '2019-11-19', 'buat main game', 2, '', '2019-11-19', '16000180957635'),
(18, 5, '1600018095', '2019-11-18', 3, '2019-11-20', '0000-00-00', 'buat render game', 3, 'kelewatan', '2019-11-19', '1600018095526'),
(19, 76, '1600018095', '2019-11-20', 3, '2019-11-22', '2019-11-19', 'buat download mobile', 2, '', '2019-11-19', '16000180957603'),
(20, 76, '1600018095', '2019-11-05', 3, '2019-11-07', '0000-00-00', 'asd', 3, 'sd', '2019-11-19', '16000180957611'),
(21, 5, '1600018095', '2019-11-19', 3, '2019-11-21', '0000-00-00', 'buat render', 3, '-', '2019-11-19', '1600018095543');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_peminjaman_komputer`
--

CREATE TABLE `tbl_peminjaman_komputer` (
  `id` int(11) NOT NULL,
  `kode_komputer` varchar(150) NOT NULL,
  `id_ruang` int(11) NOT NULL,
  `nim_mahasiswa` varchar(150) NOT NULL,
  `tgl_mulai` date NOT NULL,
  `tgl_selesai` date NOT NULL,
  `lama_peminjaman` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `create_at` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `kode_peminjaman_komputer` char(20) NOT NULL,
  `alasan_penolakan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_peminjaman_komputer`
--

INSERT INTO `tbl_peminjaman_komputer` (`id`, `kode_komputer`, `id_ruang`, `nim_mahasiswa`, `tgl_mulai`, `tgl_selesai`, `lama_peminjaman`, `keterangan`, `create_at`, `status`, `kode_peminjaman_komputer`, `alasan_penolakan`) VALUES
(85, '1', 3, '1600018095', '2019-11-19', '0000-00-00', 1, 'untuk main game', '2019-11-19 12:20:25', 2, '1600018095125', ''),
(86, '19', 3, '1600018095', '2019-11-21', '0000-00-00', 3, 'sjfdkjsdf', '2019-11-21 07:08:32', 1, '16000180951932', ''),
(87, '19', 3, '1600018095', '2019-11-22', '0000-00-00', 3, 'sjfdkjsdf', '2019-11-21 07:08:32', 1, '16000180951932', ''),
(88, '19', 3, '1600018095', '2019-11-23', '0000-00-00', 3, 'sjfdkjsdf', '2019-11-21 07:08:32', 1, '16000180951932', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ruang`
--

CREATE TABLE `tbl_ruang` (
  `id_ruang` int(11) NOT NULL,
  `nama_ruang` varchar(100) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `kapasitas` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ruang`
--

INSERT INTO `tbl_ruang` (`id_ruang`, `nama_ruang`, `status`, `kapasitas`, `created_at`, `updated_at`) VALUES
(3, 'LAB KOMDAS', '', 42, '0000-00-00 00:00:00', '2019-10-28 04:10:21'),
(4, 'LAB BASIS DATA', '', 42, '0000-00-00 00:00:00', '2019-10-28 04:10:27'),
(5, 'LAB MULTIMEDIA', '', 42, '0000-00-00 00:00:00', '2019-10-28 04:10:25'),
(6, 'LAB JARINGAN', '', 42, '0000-00-00 00:00:00', '2019-10-28 04:10:33'),
(7, 'LAB RISET', '', 42, '0000-00-00 00:00:00', '2019-10-28 04:10:31');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_status`
--

CREATE TABLE `tbl_status` (
  `id` int(11) NOT NULL,
  `keterangan` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_status`
--

INSERT INTO `tbl_status` (`id`, `keterangan`) VALUES
(1, 'Di Proses'),
(2, 'Di Terima'),
(3, 'Di Tolak'),
(4, 'Selesai');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_barang`
--
ALTER TABLE `tbl_barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_barang_cadangan`
--
ALTER TABLE `tbl_barang_cadangan`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indexes for table `tbl_jam`
--
ALTER TABLE `tbl_jam`
  ADD PRIMARY KEY (`id_jam`);

--
-- Indexes for table `tbl_jam_temp`
--
ALTER TABLE `tbl_jam_temp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_komputer`
--
ALTER TABLE `tbl_komputer`
  ADD PRIMARY KEY (`id_komputer`);

--
-- Indexes for table `tbl_lapor_kejadian`
--
ALTER TABLE `tbl_lapor_kejadian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_mahasiswa`
--
ALTER TABLE `tbl_mahasiswa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_peminjaman`
--
ALTER TABLE `tbl_peminjaman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_peminjaman_alat`
--
ALTER TABLE `tbl_peminjaman_alat`
  ADD PRIMARY KEY (`id_peminjaman`);

--
-- Indexes for table `tbl_peminjaman_komputer`
--
ALTER TABLE `tbl_peminjaman_komputer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ruang`
--
ALTER TABLE `tbl_ruang`
  ADD PRIMARY KEY (`id_ruang`);

--
-- Indexes for table `tbl_status`
--
ALTER TABLE `tbl_status`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_barang`
--
ALTER TABLE `tbl_barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- AUTO_INCREMENT for table `tbl_barang_cadangan`
--
ALTER TABLE `tbl_barang_cadangan`
  MODIFY `id_barang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_jam`
--
ALTER TABLE `tbl_jam`
  MODIFY `id_jam` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_jam_temp`
--
ALTER TABLE `tbl_jam_temp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_komputer`
--
ALTER TABLE `tbl_komputer`
  MODIFY `id_komputer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `tbl_lapor_kejadian`
--
ALTER TABLE `tbl_lapor_kejadian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_mahasiswa`
--
ALTER TABLE `tbl_mahasiswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tbl_peminjaman`
--
ALTER TABLE `tbl_peminjaman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `tbl_peminjaman_alat`
--
ALTER TABLE `tbl_peminjaman_alat`
  MODIFY `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `tbl_peminjaman_komputer`
--
ALTER TABLE `tbl_peminjaman_komputer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `tbl_ruang`
--
ALTER TABLE `tbl_ruang`
  MODIFY `id_ruang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_status`
--
ALTER TABLE `tbl_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
