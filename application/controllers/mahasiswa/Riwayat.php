<?php 
class Riwayat extends CI_Controller
{
    public function __construct(){
        parent::__construct();
           $this->load->helper('url');
           $this->load->library('Session');
           $this->load->model('m_mhs_peminjaman');
           $this->load->model('m_mhs_riwayat');
    }

	public function index()
	{
        if(!$this->session->userdata('email_mahasiswa')){
            redirect('mahasiswa/login');
        }else{
        $nim = $this->session->userdata('nim');
        $data['riwayat'] = $this->m_mhs_riwayat->riwayat_peminjaman($nim);
        $this->load->view("mahasiswa/riwayat/v_riwayat", $data);
        }
    }
    public function riwayat_komputer(){
        if(!$this->session->userdata('email_mahasiswa')){
            redirect('mahasiswa/login');
        }else{
        $nim = $this->session->userdata('nim');
        $data['riwayat'] = $this->m_mhs_riwayat->riwayat_peminjaman_komputer($nim);
        $this->load->view("mahasiswa/riwayat/v_riwayat_komputer", $data);
        }
    }
    public function riwayat_alat(){
        if(!$this->session->userdata('email_mahasiswa')){
            redirect('mahasiswa/login');
        }else{
        $nimnya = $this->session->userdata('nim');
        $nim = array('nim_mahasiswa'=>$nimnya);
        $data['riwayat'] = $this->m_mhs_riwayat->riwayat_alat($nim);
        $this->load->view("mahasiswa/riwayat/v_riwayat_alat", $data);
        }
    }
}




?>