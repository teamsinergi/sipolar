<?php

class Peminjaman extends CI_Controller {

    public function __construct(){
         parent::__construct();
            $this->load->helper('url');
            $this->load->library('Session');
            $this->load->model('m_mhs_peminjaman');
    }

    public function index(){
        if(!$this->session->userdata('email_mahasiswa')){
            redirect('mahasiswa/login');
        }else{
            $date = date('Y-m-d');
            // $data['tanggal'] = $date;
            $data['ruang'] = $this->m_mhs_peminjaman->ruangan();
            $data['jam'] = $this->m_mhs_peminjaman->jam();
            $this->load->view("mahasiswa/peminjaman/v_peminjaman", $data);
        }
    }

    public function simpan_pinjam(){
        if(!$this->session->userdata('email_mahasiswa')){
            redirect('mahasiswa/login');
        }else{
            // $mulai = $this->input->post('xjammulai');
            // $selesai = $this->input->post('xjamselesai');
            $waktu = array(); // simpan rentang waktu ke dalam array
            $durasinya = $this->input->post('xdurasi');
            $jumlahdurasi = count($durasinya);
            for($x=0;$x<$jumlahdurasi;$x++){
                array_push($waktu, $durasinya[$x]); // simpan rentang waktu ke dalam array
            }
            $durasi = implode(",",$waktu); // ubah array ke string
        $nim = $this->input->post('xnim');
        $nama = $this->input->post('xnama');
        $ruangan = $this->input->post('xruangan');
        $tanggalpinjam = $this->input->post('xtanggal');
        $jammulai = $this->m_mhs_peminjaman->ambil_jam_mulai($durasinya[0]);
        $jamselesai = $this->m_mhs_peminjaman->ambil_jam_selesai(end($durasinya));
        $keterangan = $this->input->post('xketerangan');
        $query = $this->m_mhs_peminjaman->simpan_pinjam($nim, $ruangan, $tanggalpinjam, $jammulai, $jamselesai, $keterangan, $durasi);
        if($query){
            $this->session->set_flashdata('pesan','Data terkirim');
        }
        redirect('mahasiswa/riwayat');
        }
    }

    public function cetak_surat($kode){
        if(!$this->session->userdata('email_mahasiswa')){
            redirect('mahasiswa/login');
        }else{
        $this->load->library('mypdf');
        $tanggal = $this->m_mhs_peminjaman->tanggal_create($kode);
        $data['no_surat'] = $this->m_mhs_peminjaman->nomer_surat($tanggal);
        $data['nama_lab'] = $this->m_mhs_peminjaman->nama_lab_terpinjam($kode);
        $data['identitas'] = $this->m_mhs_peminjaman->identitas($kode);
        $detik = date('i');
        $this->mypdf->generate('mahasiswa/surat/index', $data, 'Surat_pemijaman'.date("h:i:sa"), 'A4', 'potrait');
        $this->load->view('mahasiswa/surat/index'); 
        }

        //$this->mypdf->generate('Laporan/dompdf', $data, 'laporan-mahasiswa', 'A4', 'potrait');
    }
    public function cetak_surat_komputer($kode){
        if(!$this->session->userdata('email_mahasiswa')){
            redirect('mahasiswa/login');
        }else{
        $this->load->library('mypdf');
        $tanggal = $this->m_mhs_peminjaman->tanggal_create_komputer($kode);
        $data['no_surat'] = $this->m_mhs_peminjaman->nomer_surat_komputer($tanggal);
        $data['nama_lab'] = $this->m_mhs_peminjaman->nama_lab_terpinjam_komputer($kode);
        $data['identitas'] = $this->m_mhs_peminjaman->identitas_komputer($kode);
        $data['tanggal'] = $this->m_mhs_peminjaman->tanggal_pinjam($kode);
        $data['kode_komputer'] = $this->m_mhs_peminjaman->kode_komputer($kode);
        $data['lab'] = $this->m_mhs_peminjaman->namalab($kode);
        $data['tgl_selesai'] = $this->m_mhs_peminjaman->tanggal_selesai_komputer($kode);        
        $this->mypdf->generate('mahasiswa/surat/index_komputer', $data, 'Surat_pemijaman '.date("h:i:sa"), 'A4', 'potrait');
        $this->load->view('mahasiswa/surat/index_komputer');
        }
        //$this->mypdf->generate('Laporan/dompdf', $data, 'laporan-mahasiswa', 'A4', 'potrait');
    }
    public function pinjam_komputer(){
        if(!$this->session->userdata('email_mahasiswa')){
            redirect('mahasiswa/login');
        }else{
        $date = date('Y-m-d');
        $tahun = substr($date,0,4);
        $sebelumbulan = substr($date,5);
        $bulan = substr($sebelumbulan,0,2);
        $tanggal = substr($date,8,8);
        $data['komputer'] = $this->m_mhs_peminjaman->list_komputer();
        $data['komputerterpinjam'] = $this->m_mhs_peminjaman->list_komputer_terpinjam($tahun,$bulan,$tanggal);
        $data['tanggal'] = $date;
        $this->load->view("mahasiswa/peminjaman/komputer/v_peminjaman_komputer3", $data);
        }
    }
    public function cek_komputer(){
        if(!$this->session->userdata('email_mahasiswa')){
            redirect('mahasiswa/login');
        }else{
        $date = $this->input->post("xtanggal");
        $tahun = substr($date,0,4);
        $sebelumbulan = substr($date,5);
        $bulan = substr($sebelumbulan,0,2);
        $tanggal = substr($date,8,8);
        $data['komputer'] = $this->m_mhs_peminjaman->list_komputer();
        $data['komputerterpinjam'] = $this->m_mhs_peminjaman->list_komputer_terpinjam($tahun,$bulan,$tanggal);
        $data['tanggal'] = $this->input->post("xtanggal");
        $this->load->view("mahasiswa/peminjaman/komputer/v_peminjaman_komputer3", $data);
        }
    }
    public function simpan_pinjam_komputer(){
        if(!$this->session->userdata('email_mahasiswa')){
            redirect('mahasiswa/login');
        }else{
        $kode_komputer = $this->input->post("xkomputer");
        $keterangan = $this->input->post("xketerangan");
        $nim = $this->session->userdata('nim');
        $lab = $this->input->post("xlab");
        $lama = $this->input->post("xlama");
        $tanggal = $this->input->post("xtanggal");
        $temp = 0; // ini buat ngecek apakah komputer itu bisa di pinjam atau tidak selama beberapa hari
        for($i =0; $i<$lama; $i++){
            $tanggal_pinjam = date('Y-m-d', strtotime('+'.$i.'days', strtotime($tanggal)));

            $query = $this->m_mhs_peminjaman->cek_ketersediaan_komputer($tanggal_pinjam, $kode_komputer);
            if($query == null){
                $temp ++;
            }
            if($temp == $lama){
                for($j =0; $j<$lama; $j++){
                    $tanggal_pinjam = date('Y-m-d', strtotime('+'.$j.'days', strtotime($tanggal)));
                    $query = $this->m_mhs_peminjaman->simpan_pinjam_komputer($kode_komputer, $tanggal_pinjam, $keterangan, $nim, $lab, $lama);
                }
                if($query){
                    $this->session->set_flashdata('pesan','Data terkirim');
                }
            }else{
                    $this->session->set_flashdata("pesan","Anda tidak bisa meminjam selama ".$lama." Hari");
            }
        }
       redirect('mahasiswa/riwayat/riwayat_komputer');
        
    }
    }

    // ini untuk fungsi di bagi 3
    public function pinjam_komputer_fungsi(){
        if(!$this->session->userdata('email_mahasiswa')){
            redirect('mahasiswa/login');
        }else{
        $date = date('Y-m-d');
        $idlab = $this->m_mhs_peminjaman->id_lab();
        $data['nama_lab'] = $this->m_mhs_peminjaman->nama_lab($idlab);
        $data['idlab'] = $idlab;
        $tahun = substr($date,0,4);
        $sebelumbulan = substr($date,5);
        $bulan = substr($sebelumbulan,0,2);
        $tanggal = substr($date,8,8);
        $data['ruang'] = $this->m_mhs_peminjaman->ruangan();
        $data['komputer_1'] = $this->m_mhs_peminjaman->list_komputer_1();
        $data['komputer_2'] = $this->m_mhs_peminjaman->list_komputer_2();
        $data['komputer_3'] = $this->m_mhs_peminjaman->list_komputer_3();
        $data['komputerterpinjam'] = $this->m_mhs_peminjaman->list_komputer_terpinjam($tahun,$bulan,$tanggal, $idlab);
        $data['tanggal'] = $date;
        $this->load->view("mahasiswa/peminjaman/komputer/v_peminjaman_komputer_fungsi", $data);
        }
    }

    public function cek_komputer_fungsi(){
        if(!$this->session->userdata('email_mahasiswa')){
            redirect('mahasiswa/login');
        }else{
        $date = $this->input->post("xtanggal");
        $idlab = $this->input->post('xlab');
        $data['nama_lab'] = $this->m_mhs_peminjaman->nama_lab($idlab);
        $data['idlab'] = $idlab;
        $tahun = substr($date,0,4);
        $sebelumbulan = substr($date,5);
        $bulan = substr($sebelumbulan,0,2);
        $tanggal = substr($date,8,8);
        $data['ruang'] = $this->m_mhs_peminjaman->ruangan();
        $data['komputer_1'] = $this->m_mhs_peminjaman->list_komputer_1();
        $data['komputer_2'] = $this->m_mhs_peminjaman->list_komputer_2();
        $data['komputer_3'] = $this->m_mhs_peminjaman->list_komputer_3();
        // $data['komputer'] = $this->m_mhs_peminjaman->list_komputer();
        $data['komputerterpinjam'] = $this->m_mhs_peminjaman->list_komputer_terpinjam($tahun,$bulan,$tanggal, $idlab);
        $data['tanggal'] = $this->input->post("xtanggal");
        $this->load->view("mahasiswa/peminjaman/komputer/v_peminjaman_komputer_fungsi", $data);
        }
    }
    // ini untuk fungsi di bagi 3


    public function peminjaman_alat(){
        if(!$this->session->userdata('email_mahasiswa')){
            redirect('mahasiswa/login');
        }else{
        $this->load->view("mahasiswa/peminjaman/alat/v_peminjaman_alat");
        }
    }

    public function get_autocomplete(){
        if (isset($_GET['term'])) {
            $result = $this->m_mhs_peminjaman->cari_alat($_GET['term']);
             if (count($result) > 0) {
          foreach ($result as $row)
               $arr_result[] = array(
                  'label'   => $row->nama_barang,
                  'id'	=> $row->id,
              );
               echo json_encode($arr_result);
             }
      }
    }
        


    public function cek_ruangan(){
        if(!$this->session->userdata('email_mahasiswa')){
            redirect('mahasiswa/login');
        }else{
        $tanggal = $this->input->post('xtanggal');
        $data['tanggal'] = $tanggal;
        $query = $this->m_mhs_peminjaman->cek_ketersediaan_ruangan($tanggal);
        if($query != null){
            $idruang = $this->m_mhs_peminjaman->ruangan_tersewa($tanggal);
            $data['ruang'] = $this->m_mhs_peminjaman->ruangan_cek($idruang);
            $data['jam'] = $this->m_mhs_peminjaman->jam();
            $this->load->view("mahasiswa/peminjaman/v_peminjaman", $data);
        }else{
            $data['ruang'] = $this->m_mhs_peminjaman->ruangan();
            $data['jam'] = $this->m_mhs_peminjaman->jam();
            $this->load->view("mahasiswa/peminjaman/v_peminjaman", $data);
        }
        }
    }
    public function cek_ruangan_rev(){
        if(!$this->session->userdata('email_mahasiswa')){
            redirect('mahasiswa/login');
        }else{
        $tanggal = $this->input->post('xtanggal');
        $mulai = $this->input->post('xjammulai');
        $selesai = $this->input->post('xjamselesai');
        $idruang = $this->input->post('xruangan');
        $waktu = $this->m_mhs_peminjaman->ambil_waktu($tanggal, $idruang);
        $a = array();
        foreach($waktu as $v){
            $loop = explode(",",$v->durasi);
            foreach ($loop as $key => $value) {
                $a[] = $value;
            }
        }
        // print_r($a);
        $ruangan = array();
        for($i = 1; $i < 11; $i++){
            if(in_array($i,$a)){
                $ruangan[$i] = $i; 
            } else {
                $ruangan[$i] = 0;
            }
        }
        $data['ruangan'] = $ruangan;
        // print_r($ruangan); 
        $data['tanggal'] = $tanggal;
        $query = $this->m_mhs_peminjaman->cek_ketersediaan_ruangan($tanggal);
        if($query != null){
            $data['ruang'] = $this->m_mhs_peminjaman->ruangan();
            $data['jam'] = $this->m_mhs_peminjaman->jam();
            $this->load->view("mahasiswa/peminjaman/v_peminjaman_rev", $data);
        }else{
            $data['ruang'] = $this->m_mhs_peminjaman->ruangan_pilih($idruang);
            $data['jam'] = $this->m_mhs_peminjaman->jam();
            $this->load->view("mahasiswa/peminjaman/v_peminjaman_rev", $data);
        }
        }
    }

    public function simpan_pinjam_alat(){
        if(!$this->session->userdata('email_mahasiswa')){
            redirect('mahasiswa/login');
        }else{
        $date = date("Y-m-d");
        $hari = date('s');
        $nim = $this->input->post('xnim');
        $id_barang = $this->input->post('xid_alat');
        $kode = $nim.$id_barang.$hari;
        $tanggal = $this->input->post('xtanggal');
        $lama = $this->input->post('xlama');
        $tanggal_kembali = date('Y-m-d', strtotime('+'.($lama-1).'days', strtotime($tanggal)));
        echo $tanggal_kembali;
        $data = array(
            'id' => $id_barang,
            'nim_mahasiswa' => $nim,
            'tgl_pinjam' => $tanggal,
            'lama_peminjaman' => $lama,
            'tgl_kembali' => $tanggal_kembali,
            'tujuan' => $this->input->post('xtujuan'),
            'id_status' => 1,
            'create_at' => $date,
            'kode_peminjaman' => $kode
        );
        $query = $this->m_mhs_peminjaman->simpan_pinjam_alat($data);
        if($query){
            $this->session->set_flashdata('pesan', 'Data berhasil di kirim');
        }
        redirect('mahasiswa/riwayat/riwayat_alat');
        }
    }
    public function cetak_surat_alat($kode){
        if(!$this->session->userdata('email_mahasiswa')){
            redirect('mahasiswa/login');
        }else{
        $this->load->library('mypdf');
        $tanggal = $this->m_mhs_peminjaman->tanggal_create_alat($kode);
        $data['no_surat'] = $this->m_mhs_peminjaman->nomer_surat_alat($tanggal);
        //$data['nama_lab'] = $this->m_mhs_peminjaman->nama_lab_terpinjam($kode);
        $data['identitas'] = $this->m_mhs_peminjaman->identitas_alat($kode);
        $detik = date('i');
        $this->mypdf->generate('mahasiswa/surat/index_alat', $data, 'Surat_pemijaman'.date("h:i:sa"), 'A4', 'potrait');
        $this->load->view('mahasiswa/surat/index_alat'); 
    }
    }
}

?>