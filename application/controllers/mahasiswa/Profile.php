<?php 

/**
 * 
 */
class Profile extends CI_Controller
{

	public function __construct(){
		parent::__construct();
		   $this->load->helper('url');
		   $this->load->library('Session');
		   $this->load->model('m_mhs_profile');
   }

	public function index(){
		if(!$this->session->userdata('email_mahasiswa')){
            redirect('mahasiswa/login');
        }else{
			$email = $this->session->userdata('email_mahasiswa');
			$data['profile'] = $this->m_mhs_profile->profile($email);
			$data['email'] = $email;
			$this->load->view('mahasiswa/profile/v_profile', $data);
		}
	}

	public function update_profile(){
		if(!$this->session->userdata('email_mahasiswa')){
            redirect('mahasiswa/login');
        }else{
		$nama = $this->input->post("xnama");
		$nomer = $this->input->post('xnomer');
		$nim = $this->input->post('xnim');
		$query = $this->m_mhs_profile->update_profile($nama, $nim, $nomer);
		if($query){
			$this->session->set_flashdata('pesan','Update Profile Berhasil');
		}
		redirect('mahasiswa/profile');
		}
	}
}


?>