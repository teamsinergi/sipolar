<?php

class Login extends CI_Controller {

    public function __construct(){

            parent::__construct();
                $this->load->helper('url');
                $this->load->library('Session');
                $this->load->model('m_mhs_login');

    }

    public function index(){
        $this->load->view("mahasiswa/login/v_login");
    }

    public function daftar_akun(){
        $nama = $this->input->post('xnama');
        $email = $this->input->post('xemail');
        $nim = $this->input->post('xnim');
        $password = md5($this->input->post('xpassword'));
        $cek_email=$this->m_mhs_login->cek_email($email);
        if($cek_email){
            $set = '123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $code = substr(str_shuffle($set), 0, 12);
            
            $this->m_mhs_login->daftar_mahasiswa($nama, $nim, $email, $code, $password);
            // $id = $this->m_mhs_login->ambil_id($email);
            $this->session->set_flashdata('flash_msg', 'Silahkan verifikasi email anda');
            //set up email
                $config = array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'ssl://smtp.googlemail.com',
                    'smtp_port' => 465,
                    'smtp_user' => 'qwertyzxcvbn081@gmail.com', // change it to yours
                    'smtp_pass' => 'yangbukaemailiniselaingemaantikaharam01', // change it to yours
                    'mailtype' => 'html',
                    'charset' => 'iso-8859-1',
                    'wordwrap' => TRUE
            );
            $message = 	"
                        <html>
                        <head>
                            <title>Email verifikasi</title>
                        </head>
                        <body>
                            <p>Klik Disini untuk aktivasi akun</p>
                            <h4><a href='".base_url()."mahasiswa/login/activate/".$email."/".$code."'>Aktifkan Akun Saya</a></h4>
                        </body>
                        </html>
                        ";
            
            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            $this->email->from($config['smtp_user']);
            $this->email->to($email);
            $this->email->subject('Signup Verification Email');
            $this->email->message($message);
            if($this->email->send()){
                $this->session->set_flashdata('emailterkirim','Kode verifikasi terkirim ke email ');
                }
                else{
                    $this->session->set_flashdata('flash_msg', $this->email->print_debugger());
                }
            redirect('mahasiswa/login');
        }
        else{
            $this->session->set_flashdata('flash_msg', 'Email Sudah Terdaftar');
            redirect('mahasiswa/login');
        }   

    }

    public function login_view(){
        $this->load->view("customer/login/v_login_customer");

    }

    function masuk(){
        $user_login=array(

        'email'=>$this->input->post('xemail'),
        'password'=>md5($this->input->post('xpassword'))
            );

            $data=$this->m_mhs_login->login_mhs($user_login['email'],$user_login['password']);
            if($data)
            {
                $status = $this->m_mhs_login->ambil_status($user_login['email']);
                if($status != 1){
                    $this->session->set_flashdata('flash_msg', 'Akun Belum teraktifasi');
                    $this->load->view("mahasiswa/login/v_login");
                }else{
                    $this->session->set_userdata('nim',$data['nim']);
                    $this->session->set_userdata('email_mahasiswa',$data['email_mahasiswa']);
                    $this->session->set_userdata('nama_mahasiswa',$data['nama_mahasiswa']);
                // <?php echo $this->session->userdata('email_mahasiswa'); buat echo doang biar inget
                    redirect('home');
                }
            }
            else{
                $this->session->set_flashdata('flash_msg', 'Kombinasi email dan password salah.!');
                $this->load->view("mahasiswa/login/v_login");

            }


    }

    function user_profile(){

        $this->load->view('admin/user/v_user_profile.php');
        }

    public function keluar(){
        $this->session->sess_destroy();
        redirect('home', 'refresh');
    }

    public function activate($email, $kode){	
		$email =  $this->uri->segment(4);
		$code = $this->uri->segment(5);	
		//fetch user details
		$user = $this->m_mhs_login->ambilmahasiswa($email);

		//if code matches
		if($user['kode'] == $kode){
			//update user active status
			// $data['active'] = true;
			$query = $this->m_mhs_login->activin($email);
			if($query){
				$this->session->set_flashdata('flash_msg', 'Akun berhasil di aktifkan, silahkan Masuk.!');
			}
			else{
				$this->session->set_flashdata('flash_msg', 'Aktifasi gagal, hubungi laboran sana');
			}
		}
		else{
			$this->session->set_flashdata('flash_msg', 'Kodenya salah bosqu');
		}
		redirect('mahasiswa/login');
    }
    
    public function lupa_password(){
        $this->load->view('customer/login/v_reset_password');
    }

    public function reset_password(){
        $email = $this->input->post('xemail');
        $email_check=$this->m_mhs_login->email_checkpass($email);
        if($email_check != null){

            $id = $this->m_mhs_login->ambil_id($email);
            //set up email
                $config = array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'ssl://smtp.googlemail.com',
                    'smtp_port' => 465,
                    'smtp_user' => 'qwertyzxcvbn081@gmail.com', // change it to yours
                    'smtp_pass' => 'yangbukaemailiniselaingemaantikaharam01', // change it to yours
                    'mailtype' => 'html',
                    'charset' => 'iso-8859-1',
                    'wordwrap' => TRUE
            );
            $message = 	"
                        <html>
                        <head>
                            <title>Reset Password SIPOLAR</title>
                        </head>
                        <body>
                            <h6>Atur Ulang Password SIPOLAT</h6>
                            <p>Klik Disini untuk reset password</p>
                            <h4><a href='".base_url()."mahasiswa/login/update_password/".$id."'>Ubah password akun ISMUBA</a></h4>
                        </body>
                        </html>
                        ";
            
            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            $this->email->from($config['smtp_user']);
            $this->email->to($email);
            $this->email->subject('Reset password Sipolar');
            $this->email->message($message);
            if($this->email->send()){
                $this->session->set_flashdata('flash_msg','Link ubah password terkirim ke email anda');
                }
                else{
                    $this->session->set_flashdata('emaileror', $this->email->print_debugger());
                }
            redirect('mahasiswa/login');
        }
        else{
                $this->session->set_flashdata('flash_msg','Email Tidak Terdaftar');
                redirect('mahasiswa/login');
        }
    }

    function update_password($id){
        $data['email'] = $this->m_mhs_login->ambil_email($id);
        $data['id'] = $id;
        $this->load->view('mahasiswa/login/v_reset_password.php', $data);
    }

    function password_baru(){
        $id = $this->input->post('xid');
        $password = md5($this->input->post('xpassword'));

        $query = $this->m_mhs_login->passworbaru($id, $password);
        if($query){
            $this->session->set_flashdata('flash_msg', 'Ubah Password Berhasil');
            redirect('mahasiswa/login');
        }else{
            $this->session->set_flashdata('flash_msg', 'Ganti Password Gagal');
            redirect('mahasiswa/login');
        }
    }

}

?>
