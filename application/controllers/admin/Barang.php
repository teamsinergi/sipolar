<?php 

class Barang extends CI_Controller{
    public function __construct(){

    parent::__construct();
        $this->load->helper('url');
        $this->load->library('Session');
        $this->load->model('m_mhs_login');
        $this->load->model('m_barang');
        $this->load->model('m_mhs_peminjaman');
        $this->load->library('upload');
        

    }
	public function index()
	{
        if($this->session->userdata('nama_admin')==null)
        {
            redirect('admin/login');
        }else{
        $data['ruang'] = $this->m_mhs_peminjaman->ruangan();
        $data['barang'] = $this->m_barang->list_barang();
        $this->load->view('admin/barang/v_tampil_barang', $data);
        }
    }
    
    public function simpan_barang(){
        if($this->session->userdata('nama_admin')==null){
            redirect('admin/login');
        }else{
        $nama = $this->input->post('xnama');
        $lab = $this->input->post('xlab');
        $banyak = $this->input->post('xbanyak');
        $keterangan = $this->input->post('xketerangan');
        $kepemilikan = $this->input->post('xkepemilikan');
        $kondisi = $this->input->post('xkondisi');
        $tanggal = $this->input->post('xtanggal');
        $status = $this->input->post('xstatus');
        $query= $this->m_barang->simpan_barang($nama, $lab, $banyak, $keterangan, $kepemilikan, $kondisi, $tanggal,$status);
        if($query){
            $this->session->set_flashdata('pesan', 'Barang Berhasil di Tambah');
        }
        redirect('admin/barang');
        }
    }

    public function hapus_barang($id){
        if($this->session->userdata('nama_admin')==null)
        {
            redirect('admin/login');
        }else{
            $query = $this->m_barang->hapus_barang($id);
            $query = $this->m_barang->hapus_barang_c($id);
            if($query){
                $this->session->set_flashdata('pesan', 'Barang Berhasil di hapus');
            }
        }
        
        redirect('admin/barang');
    }

    public function edit_barang(){
        if($this->session->userdata('nama_admin')==null)
        {
            redirect('admin/login');
        }else{
        $id = $this->input->post('xid');
        $nama = $this->input->post('xnama');
        $lab = $this->input->post('xlab');
        $banyak = $this->input->post('xbanyak');
        $keterangan = $this->input->post('xketerangan');
        $kepemilikan = $this->input->post('xkepemilikan');
        $kondisi = $this->input->post('xkondisi');
        $tanggalcek = $this->input->post('xtglcek');
        $status = $this->input->post('xstatus');
        $query= $this->m_barang->edit_barang_all($lab, $id, $nama, $banyak, $keterangan, $kepemilikan, $kondisi, $tanggalcek, $status);
        if($query){
            $this->session->set_flashdata('pesan', 'Barang Berhasil di update');
        }
        }
        redirect('admin/barang');
    }

    public function cek_barang(){
        if($this->session->userdata('nama_admin')==null)
        {
            redirect('admin/login');
        }else{
        $id = $this->input->post('xid');
        $nama = $this->input->post('xnama');
        $lab = $this->input->post('xlab');
        $banyak = $this->input->post('xbanyak');
        $keterangan = $this->input->post('xketerangan');
        $kepemilikan = $this->input->post('xkepemilikan');
        $kondisi = $this->input->post('xkondisi');
        $query= $this->m_barang->cek_barang_all($lab, $id, $nama, $banyak, $keterangan, $kepemilikan, $kondisi);
        if($query){
            $this->session->set_flashdata('pesan', 'Barang Berhasil di update');
        }
        }
        redirect('admin/barang');
    }

    public function export(){
        if($this->session->userdata('nama_admin')==null)
        {
            redirect('admin/login');
        }else{
        $lab =$this->input->post('xlab');
        $data['namalab'] = $this->m_barang->nama_lab($lab);
        $data['barang'] = $this->m_barang->list_barang_export($lab);
        $this->load->view("admin/barang/v_export_barang", $data);
        }
    }
    public function detail_barang($id){
        if($this->session->userdata('nama_admin')==null)
        {
            redirect('admin/login');
        }else{
        $barangawal = $this->m_barang->barang_awal($id); // ini untuk ambil banyak barang asli bukan cadangan
        $barangcadangan = $this->m_barang->barang_cadangan($id); // ini untuk ambil banyak barang cadangan
        $idbarang = array('id'=>$id);
        $data = array(
            'jumlahbarang' => $barangawal+$barangcadangan,
            'barang' => $this->m_barang->list_detail_barang($id),
            'cadanganbarang' => $this->m_barang->cadanganbarang($idbarang),
            'ruang' => $this->m_mhs_peminjaman->ruangan(),
        );
        }
        $this->load->view('admin/barang/v_detail_barang', $data);
    }
    public function tambang_cadangan(){
        if($this->session->userdata('nama_admin')==null)
        {
            redirect('admin/login');
        }else{
        $id = $this->input->post("xid");
        $lab = $this->input->post("xlab");
        $nama = $this->input->post("xnama");
        $banyak = $this->input->post("xbanyak");
        $tanggal = $this->input->post("xtanggal");
        $keterangan = $this->input->post("xketerangan");
        $status = $this->m_barang->status_barang($id);
        $data = array(
            'nama_barang' => $nama,
            'id' => $id,
            'banyak_barang' => $banyak,
            'tanggal' => $tanggal,
            'id_ruang' => $lab,
            'keterangan' => $keterangan,
            'kondisi' => "terawat",
            'kepemilikan' => "kampus",
            'status' => $status
        );
        $query = $this->m_barang->tambah_pengadaan($data);
        if($query){
            $this->session->set_flashdata('pesan', 'Barang Berhasil di tambah');
        }
        }
        redirect("admin/barang/detail_barang/$id");
    }
    public function update_barang_cadangan(){
        if($this->session->userdata('nama_admin')==null)
        {
            redirect('admin/login');
        }else{
        $id = $this->input->post('xidbarang');
        $idbarangasli = $this->input->post('xid');
        $data = array(
            'banyak_barang' => $this->input->post('xbanyak'),
            'kepemilikan' => $this->input->post('xkepemilikan'),
            'kondisi' => $this->input->post('xkondisi'),
            'keterangan' => $this->input->post('xketerangan'),
            'status' => $this->input->post('xstatus')
        );
        $query = $this->m_barang->update_barang_cadangan($id, $data);
        if($query){
            $this->session->set_flashdata('pesan', 'Barang Berhasil di tambah');
        }
        }
        redirect("admin/barang/detail_barang/$idbarangasli");
    }

    public function hapus_barang_cadangan($id_barang, $id){
        if($this->session->userdata('nama_admin')==null)
        {
            redirect('admin/login');
        }else{
        $query = $this->m_barang->hapus_barang_cadangan($id_barang);
        if($query){
            $this->session->set_flashdata('pesan', 'Barang Berhasil di hapus');
        }
        }
        redirect("admin/barang/detail_barang/$id");
    }


    public function export_excel(){
        if($this->session->userdata('nama_admin')==null)
        {
            redirect('admin/login');
        }else{
        // create file name
        $fileName = 'data-' . time() . '.xlsx';
        // load excel library
        $this->load->library('excel');

        // data awal
        $tampilsemua = $this->m_barang->list_barang_export_excel();

        // data awal akhir
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);

        // set Header untuk atas
        $objPHPExcel->getActiveSheet()->mergeCells('B3:D3');
        $objPHPExcel->getActiveSheet()->SetCellValue('B3', 'Barang inventori Laboratorium Teknik Informatika ');
        //buat kasi warna aja
        $objPHPExcel->getActiveSheet()->getStyle('B3:D3')->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'ffff00')
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            )
        );
        $objPHPExcel->getActiveSheet()->SetCellValue('A5', 'No');
        $objPHPExcel->getActiveSheet()->SetCellValue('B5', 'Laboratorium');
        $objPHPExcel->getActiveSheet()->SetCellValue('C5', 'Barang');
        $objPHPExcel->getActiveSheet()->SetCellValue('D5', 'Banyak');
        $objPHPExcel->getActiveSheet()->SetCellValue('E5', 'Kepemilikan');
        $objPHPExcel->getActiveSheet()->SetCellValue('F5', 'Kondisi');
        $objPHPExcel->getActiveSheet()->SetCellValue('G5', 'Keterangan Kondisi');
        $objPHPExcel->getActiveSheet()->SetCellValue('H5', 'Tanggal Pengadaan');
        //buat kasi warna aja
        $objPHPExcel->getActiveSheet()->getStyle('A5:H5')->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '99ccff')
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            )
        );
        //buat kasi warna aja
        $rowCount = 6;
        $no = 0;
        foreach ($tampilsemua as $element) {
            $no++;
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $no);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element->nama_ruang);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element->nama_barang);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element->banyak_barang);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element->kepemilikan);
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element->kondisi);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element->keterangan);
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $element->tanggal);
            $rowCount++;
        }

        $rowCount = $rowCount +2;
        $rowCountkab = $rowCount+1;
    
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save(ROOT_UPLOAD_IMPORT_PATH . $fileName);
        // download file
        header("Content-Type: application/vnd.ms-excel");
        redirect(HTTP_UPLOAD_IMPORT_PATH . $fileName);
        redirect('admin/barang');
    }
    }



}
?>