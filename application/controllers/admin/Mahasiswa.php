<?php 

class Mahasiswa extends CI_Controller{
    public function __construct(){

    parent::__construct();
        $this->load->helper('url');
        $this->load->library('Session');
        $this->load->model('m_mhs_login');
        $this->load->model('m_mhs_login');
    }
	public function index()
	{
                    if($this->session->userdata('nama_admin')==null){
                redirect('admin/login');
            }else{
        $data['mahasiswa'] = $this->m_mhs_login->list_mahasiswa();
        $this->load->view('admin/mahasiswa/v_tampil_mahasiswa', $data);
            }
    }
    
    public function simpan_barang(){
                    if($this->session->userdata('nama_admin')==null){
                redirect('admin/login');
            }else{
        $nama = $this->input->post('xnama');
        $banyak = $this->input->post('xbanyak');
        $keterangan = $this->input->post('xketerangan');
        $query= $this->m_barang->simpan_barang($nama, $banyak, $keterangan);
        if($query){
            $this->session->set_flashdata('pesan', 'Barang Berhasil di Tambah');
        }
        redirect('admin/barang');
    }
    }

    public function hapus_barang($id){
                    if($this->session->userdata('nama_admin')==null){
                redirect('admin/login');
            }else{
        $query = $this->m_barang->hapus_barang($id);
        if($query){
            $this->session->set_flashdata('pesan', 'Barang Berhasil di hapus');
        }
        
        redirect('admin/barang');
    }
    }

    public function edit_barang(){
                    if($this->session->userdata('nama_admin')==null){
                redirect('admin/login');
            }else{
        $id = $this->input->post('xid');
        $nama = $this->input->post('xnama');
        $banyak = $this->input->post('xbanyak');
        $keterangan = $this->input->post('xketerangan');
        $query= $this->m_barang->edit_barang($id, $nama, $banyak, $keterangan);
        if($query){
            $this->session->set_flashdata('pesan', 'Barang Berhasil di update');
        }
        redirect('admin/barang');
    }
    }



}
?>