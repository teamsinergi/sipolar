<?php

class Login extends CI_Controller {

    public function __construct(){

            parent::__construct();
                $this->load->helper('url');
                $this->load->model('m_dsn_login');
                $this->load->library('session');

    }

    public function index(){
        if ($this->session->userdata('user_name')==null){
            $this->load->view('admin/login/login');
        }else{
            redirect('admin/overview');
        }
        
    }

    public function register_user(){

            $user=array(
            'user_name'=>$this->input->post('user_name'),
            'user_email'=>$this->input->post('user_email'),
            'user_password'=>md5($this->input->post('user_password')),
                );
                print_r($user);

        $email_check=$this->M_user->email_check($user['user_email']);

        if($email_check){
        $this->M_user->register_user($user);
        $this->session->set_flashdata('success_msg', 'Registered successfully.Now login to your account.');
        redirect('admin/user/login_view');

        }
        else{
        $this->session->set_flashdata('error_msg', 'Error occured,Try again.');
        redirect('admin/user');
        }

    }

    public function login_view(){
        $this->load->view("admin/user/v_login");

    }

    function login_user(){
        $user_login=array(
        'email'=>$this->input->post('xemail'),
        'password'=>sha1($this->input->post('xpassword'))
            );
            $data=$this->m_dsn_login->login_user($user_login['email'],$user_login['password']);
            if($data)
            {
                $this->session->set_userdata('id',$data['id']);
                $this->session->set_userdata('email',$data['email']);
                $this->session->set_userdata('nama_admin',$data['nama_admin']);
                //$this->session->userdata('email');
                redirect('admin');
            }
            else{
                $this->session->set_flashdata('pesan', 'Email Atau Password salah.!');
                redirect('admin/login');
            }


    }

    function user_profile(){

        $this->load->view('admin/user/v_user_profile.php');
        }

    public function user_logout(){
        $this->session->sess_destroy();
        redirect('admin/login');
    }

    function tampil_user(){
        $data['pengguna']=$this->M_user->admin_user();
		$this->load->view('admin/user/v_admin_user',$data);
    }


        public function edit_user($id){
        $where = array('id_user'=>$id);
        $data['user']=$this->M_user->edit_user($where, 'tbl_user'); 
        $this->load->view("admin/user/v_edit_user", $data);
    }

    public function update_user(){
        $id=$this->input->post('xid');
        $name=$this->input->post('xname');
        $email = $this->input->post('xemail');
        $pass = md5($this->input->post('xpass'));
        $this->M_user->update_user($id, $name, $email, $pass);
        redirect('admin/user/');
        
    }


      public function simpan_user(){
        $id_user = $this->input->post('xid');
        $user_name = $this->input->post('xname');
        $user_email = $this->input->post('xemail');
        $user_password = md5($this->input->post('xpass'));
        $this->M_user->simpan_user($id_user, $user_name, $user_email, $user_password);
        redirect('admin/user/Tampil_user');
    }

        public function delete_user(){
            $iduser= $this->input->post("xid");
            $this->M_user->delete_user($iduser);
            redirect('admin/user/Tampil_user','refresh');
    }

    public function daftar_admin(){
        $this->load->view('admin/user/v_register');
    }

    public function reset_password_admin(){
        $email = $this->input->post('xemail');
        $cekemail = $this->m_dsn_login->cek_email($email);
        if($cekemail == null){
            $this->session->set_flashdata('pesan', 'Email Tidak Terdaftar.!');
            redirect('admin/login');
        }else{
            $email = $this->input->post('xemail');
                $set = '123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';                
                $id = $this->m_dsn_login->ambil_id($email);
                //set up email
                    $config = array(
                        'protocol' => 'smtp',
                        'smtp_host' => 'ssl://smtp.googlemail.com',
                        'smtp_port' => 465,
                        'smtp_user' => 'qwertyzxcvbn081@gmail.com', // change it to yours
                        'smtp_pass' => 'yangbukaemailiniselaingemaantikaharam01', // change it to yours
                        'mailtype' => 'html',
                        'charset' => 'iso-8859-1',
                        'wordwrap' => TRUE
                );
                $message = 	"
                            <html>
                            <head>
                                <title>Reset Password SIPOLAR</title>
                            </head>
                            <body>
                                <p>Klik Disini untuk reset password</p>
                                <h4><a href='".base_url()."admin/login/update_password/".$id."'>Ubah password akun SIPOLAR</a></h4>
                            </body>
                            </html>
                            ";
                
                $this->load->library('email', $config);
                $this->email->set_newline("\r\n");
                $this->email->from($config['smtp_user']);
                $this->email->to($email);
                $this->email->subject('Ubah Password SIPOLAR');
                $this->email->message($message);
                if($this->email->send()){
                    $this->session->set_flashdata('pesan', 'Link ubah password terkirim ke email anda');                    
                }
                else{
                    $this->session->set_flashdata('emaileror', $this->email->print_debugger());
                }
                redirect('admin/login');
        }
    }

    public function update_password($id){
        $data['id'] = $id;
        $this->load->view("admin/login/v_reset_password", $data);
    }
    public function simpan_update_password(){
        $id = $this->input->post('xid');
        $data = array(
            'password' => sha1($this->input->post('xpassword'))
        );
        $query = $this->m_dsn_login->simpan_update_password($id, $data);
        if($query){
            $this->session->set_flashdata('pesan', 'Email Atau Password salah.!');
        }
        redirect('admin/login');
        
    }

}

?>