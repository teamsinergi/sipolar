<?php 

class Admin extends CI_Controller{
    public function __construct(){

    parent::__construct();
        $this->load->helper('url');
        $this->load->library('Session');
        $this->load->model('m_mhs_login');
        $this->load->model('m_dsn_login');
        $this->load->model('m_mhs_peminjaman');

    }
	
	    public function index()
	{
        if($this->session->userdata('nama_admin')==null){
            redirect('admin/login');
        }else{
            $id = $this->session->userdata('id');
            $data['pinjamanmasuk'] = $this->m_mhs_peminjaman->pinjaman_masuk();
            $data['komputermasuk'] = $this->m_mhs_peminjaman->pinjaman_masuk_komputer();
            $data['alatmasuk'] = $this->m_mhs_peminjaman->pinjaman_alat_masuk();
            $data['terpinjam'] = $this->m_mhs_peminjaman->terpinjam();
            $data['komputerterpinjam'] = $this->m_mhs_peminjaman->terpinjam_komputer();
            $data['mahasiswa'] = $this->m_mhs_login->banyak_mahasiswa();
            $data['totalinventori'] = $this->m_mhs_peminjaman->total_inventory();
            $data['admin'] = $this->m_dsn_login->data_admin($id);
            $this->load->view('admin/home', $data);
        }
    }
    public function update_profile(){
        if($this->session->userdata('nama_admin')==null){
            redirect('admin/login');
        }else{
        $id = $this->input->post('xid');
        $data = array(
            'nama_admin' => $this->input->post('xnama'),
            'email_admin' => $this->input->post('xemail')
        );
        $query = $this->m_dsn_login->update_profile($id, $data);
        redirect('admin/admin');
        }

    }

}
 
 
?>