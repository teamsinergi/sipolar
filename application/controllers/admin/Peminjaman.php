<?php 

class Peminjaman extends CI_Controller{
    public function __construct(){
        parent::__construct();
            $this->load->helper('url');
            $this->load->library('Session');
            $this->load->model('m_mhs_peminjaman');
        }
	
	    public function index()
        {
                        if($this->session->userdata('nama_admin')==null){
                redirect('admin/login');
            }else{
            $data['peminjaman'] = $this->m_mhs_peminjaman->peminjaman_masuk();
            $this->load->view('admin/peminjaman/v_peminjaman', $data);
            }
        }

        public function detail($kode){
                        if($this->session->userdata('nama_admin')==null){
                redirect('admin/login');
            }else{
            $data['detail'] = $this->m_mhs_peminjaman->detail($kode);
            $data['tanggal'] = $this->m_mhs_peminjaman->detail_tanggal($kode);
            $data['terboking'] = $this->m_mhs_peminjaman->terboking($data['tanggal']);
            $data['kode'] = $kode;
            $this->load->view('admin/peminjaman/v_detail_peminjaman', $data);
            }
        }

        public function terima_peminjaman($kode){
                        if($this->session->userdata('nama_admin')==null){
                redirect('admin/login');
            }else{
            $query = $this->m_mhs_peminjaman->terima_peminjaman($kode);
            if($query){
                $this->session->set_flashdata('pesan', 'Peminjaman Diterima');
            }
            redirect('admin/peminjaman');
            }
        }

        public function terima_peminjaman_komputer($kode){
                        if($this->session->userdata('nama_admin')==null){
                redirect('admin/login');
            }else{
            $query = $this->m_mhs_peminjaman->terima_peminjaman_komputer($kode);
            if($query){
                $this->session->set_flashdata('pesan', 'Peminjaman Diterima');
            }
            redirect('admin/peminjaman/peminjaman_komputer');
            }
        }

        public function permintaan_ditolak(){
                        if($this->session->userdata('nama_admin')==null){
                redirect('admin/login');
            }else{
            $kode = $this->input->post('xkode');
            $alasan = $this->input->post('xalasan');
            $query = $this->m_mhs_peminjaman->permintaan_ditolak($kode, $alasan);
            if($query){
                $this->session->set_flashdata('pesan', 'Peminjaman Ditolak');
            }
            redirect('admin/peminjaman');
            }
        }

        public function ditolak(){
                        if($this->session->userdata('nama_admin')==null){
                redirect('admin/login');
            }else{
            $data['ditolak'] = $this->m_mhs_peminjaman->ditolak();
            $this->load->view('admin/peminjaman/v_peminjaman_ditolak', $data);
            }
        }
        public function ditolak_komputer(){
                        if($this->session->userdata('nama_admin')==null){
                redirect('admin/login');
            }else{
            $data['ditolak'] = $this->m_mhs_peminjaman->ditolak_komputer();
            $this->load->view('admin/peminjaman/komputer/v_peminjaman_ditolak', $data);
            }
        }
        
        public function diterima(){
                        if($this->session->userdata('nama_admin')==null){
                redirect('admin/login');
            }else{
            $data['diterima'] = $this->m_mhs_peminjaman->diterima();
            $this->load->view('admin/peminjaman/v_peminjaman_diterima', $data);
            }
        }
        public function diterima_komputer(){
                        if($this->session->userdata('nama_admin')==null){
                redirect('admin/login');
            }else{
            $data['diterima'] = $this->m_mhs_peminjaman->diterima_komputer();
            $this->load->view('admin/peminjaman/komputer/v_peminjaman_diterima', $data);
            }
        }
        public function peminjaman_komputer(){
                        if($this->session->userdata('nama_admin')==null){
                redirect('admin/login');
            }else{
            $data['peminjaman'] = $this->m_mhs_peminjaman->peminjaman_masuk_komputer();
            $this->load->view('admin/peminjaman/komputer/v_peminjaman', $data);
            }
        }

        public function detail_peminjaman_komputer($kode){
                        if($this->session->userdata('nama_admin')==null){
                redirect('admin/login');
            }else{
            $data['detail'] = $this->m_mhs_peminjaman->detail_peminjaman_komputer($kode);
            $data['kode'] = $kode;
            $this->load->view('admin/peminjaman/komputer/v_detail_peminjaman', $data);
            }
        }

        public function permintaan_ditolak_komputer(){
                        if($this->session->userdata('nama_admin')==null){
                redirect('admin/login');
            }else{
            $kode = $this->input->post('xkode');
            $alasan = $this->input->post('xalasan');
            $query = $this->m_mhs_peminjaman->permintaan_ditolak_komputer($kode, $alasan);
            if($query){
                $this->session->set_flashdata('pesan', 'Peminjaman Ditolak');
            }
            redirect('admin/peminjaman/peminjaman_komputer');
            }
        }

        public function peminjaman_alat(){
                        if($this->session->userdata('nama_admin')==null){
                redirect('admin/login');
            }else{
            $data['peminjaman'] = $this->m_mhs_peminjaman->peminjaman_alat();
            $this->load->view('admin/peminjaman/alat/v_peminjaman_alat', $data);
            }
        }
        public function terima_peminjaman_alat($kode){
                        if($this->session->userdata('nama_admin')==null){
                redirect('admin/login');
            }else{
            $data = array(
                'id_status' => 2
            );
            $query = $this->m_mhs_peminjaman->terima_peminjaman_alat($data, $kode);
            if($query){
                $this->session->set_flashdata('pesan', 'Peminjaman Diterima');
            }
            redirect('admin/peminjaman/peminjaman_alat');
            }
        }

        public function permintaan_ditolak_alat(){
                        if($this->session->userdata('nama_admin')==null){
                redirect('admin/login');
            }else{
            $kode = $this->input->post('xkode');
            $data = array(
                'kode_peminjaman'=> $kode,
                'alasan_penolakan' => $this->input->post('xalasan'),
                'id_status' => 3
            );
            $query = $this->m_mhs_peminjaman->permintaan_ditolak_alat($data,$kode);
            if($query){
                $this->session->set_flashdata('pesan', 'Peminjaman Diterima');
            }
            redirect('admin/peminjaman/peminjaman_alat');
            }
        }

        public function ditolak_alat(){
                        if($this->session->userdata('nama_admin')==null){
                redirect('admin/login');
            }else{
            $data['peminjaman'] = $this->m_mhs_peminjaman->peminjaman_alat_ditolak();
            $this->load->view('admin/peminjaman/alat/v_peminjaman_alat_ditolak', $data);
            }
        }
        public function diterima_alat(){
                        if($this->session->userdata('nama_admin')==null){
                redirect('admin/login');
            }else{
            $data['peminjaman'] = $this->m_mhs_peminjaman->peminjaman_alat_diterima();
            $this->load->view('admin/peminjaman/alat/v_peminjaman_alat_diterima', $data);
            }
        }

        // export excel peminjaman alat
        public function alat_export_excel(){
                        if($this->session->userdata('nama_admin')==null){
                redirect('admin/login');
            }else{
            // create file name
            $fileName = 'data-' . time() . '.xlsx';
            // load excel library
            $this->load->library('excel');

            // data awal
            $tampilsemua = $this->m_mhs_peminjaman->list_alat_tersewa_export_excel();

            // data awal akhir
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->setActiveSheetIndex(0);

            // set Header untuk atas
            $objPHPExcel->getActiveSheet()->mergeCells('B3:D3');
            $objPHPExcel->getActiveSheet()->SetCellValue('B3', 'Riwayat Peminjaman Alat ');
            //buat kasi warna aja
            $objPHPExcel->getActiveSheet()->getStyle('B3:D3')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => 'ffff00')
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    )
                )
            );
            $objPHPExcel->getActiveSheet()->SetCellValue('A5', 'No');
            $objPHPExcel->getActiveSheet()->SetCellValue('B5', 'Nim');
            $objPHPExcel->getActiveSheet()->SetCellValue('C5', 'Nama mahasiswa');
            $objPHPExcel->getActiveSheet()->SetCellValue('D5', 'Nama Alat');
            $objPHPExcel->getActiveSheet()->SetCellValue('E5', 'Tanggal Pinjam');
            $objPHPExcel->getActiveSheet()->SetCellValue('F5', 'Lama Pinjam');
            $objPHPExcel->getActiveSheet()->SetCellValue('G5', 'Tanggal Kembali');
            $objPHPExcel->getActiveSheet()->SetCellValue('H5', 'Tujuan Peminjaman');
            //buat kasi warna aja
            $objPHPExcel->getActiveSheet()->getStyle('A5:H5')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '99ccff')
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    )
                )
            );
            //buat kasi warna aja
            $rowCount = 6;
            $no = 0;
            foreach ($tampilsemua as $element) {
                $no++;
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $no);
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element->nim_mahasiswa);
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element->nama_mahasiswa);
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element->nama_barang);
                $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element->tgl_pinjam);
                $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element->lama_peminjaman." Hari");
                $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element->tgl_kembali);
                $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $element->tujuan);
                $rowCount++;
            }

            $rowCount = $rowCount +2;
            $rowCountkab = $rowCount+1;
        
            $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
            $objWriter->save(ROOT_UPLOAD_IMPORT_PATH . $fileName);
            // download file
            header("Content-Type: application/vnd.ms-excel");
            redirect(HTTP_UPLOAD_IMPORT_PATH . $fileName);
            redirect('admin/peminjaman/diterima_alat');
            }
        }
        // export excel peminjaman alat

        // export excel peminjaman komputer
        public function komputer_export_excel(){
                        // create file name
            $fileName = 'data-' . time() . '.xlsx';
            // load excel library
            $this->load->library('excel');

            // data awal
            $tampilsemua = $this->m_mhs_peminjaman->list_komputer_tersewa_export_excel();

            // data awal akhir
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->setActiveSheetIndex(0);

            // set Header untuk atas
            $objPHPExcel->getActiveSheet()->mergeCells('B3:D3');
            $objPHPExcel->getActiveSheet()->SetCellValue('B3', 'Riwayat Peminjaman Komputer ');
            //buat kasi warna aja
            $objPHPExcel->getActiveSheet()->getStyle('B3:D3')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => 'ffff00')
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    )
                )
            );
            $objPHPExcel->getActiveSheet()->SetCellValue('A5', 'No');
            $objPHPExcel->getActiveSheet()->SetCellValue('B5', 'Nim');
            $objPHPExcel->getActiveSheet()->SetCellValue('C5', 'Nama mahasiswa');
            $objPHPExcel->getActiveSheet()->SetCellValue('D5', 'Laboratorium');
            $objPHPExcel->getActiveSheet()->SetCellValue('E5', 'Tanggal Pinjam');
            $objPHPExcel->getActiveSheet()->SetCellValue('F5', 'Lama Pinjam');
            $objPHPExcel->getActiveSheet()->SetCellValue('G5', 'Tujuan Peminjaman');
            //buat kasi warna aja
            $objPHPExcel->getActiveSheet()->getStyle('A5:G5')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '99ccff')
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    )
                )
            );
            //buat kasi warna aja
            $rowCount = 6;
            $no = 0;
            foreach ($tampilsemua as $element) {
                $no++;
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $no);
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element->nim_mahasiswa);
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element->nama_mahasiswa);
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element->nama_ruang);
                $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element->tgl_mulai);
                $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element->lama_peminjaman." Hari");
                $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element->keterangan);
                $rowCount++;
            }

            $rowCount = $rowCount +2;
            $rowCountkab = $rowCount+1;
        
            $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
            $objWriter->save(ROOT_UPLOAD_IMPORT_PATH . $fileName);
            // download file
            header("Content-Type: application/vnd.ms-excel");
            redirect(HTTP_UPLOAD_IMPORT_PATH . $fileName);
            redirect('admin/peminjaman/diterima_komputer');

        }
        // export excel peminjaman komputer

        // export excel peminjaman ruangan
        public function ruangan_export_excel(){
                        if($this->session->userdata('nama_admin')==null){
                redirect('admin/login');
            }else{
                        // create file name
            $fileName = 'data-' . time() . '.xlsx';
            // load excel library
            $this->load->library('excel');

            // data awal
            $tampilsemua = $this->m_mhs_peminjaman->list_ruangan_tersewa_export_excel();

            // data awal akhir
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->setActiveSheetIndex(0);

            // set Header untuk atas
            $objPHPExcel->getActiveSheet()->mergeCells('B3:D3');
            $objPHPExcel->getActiveSheet()->SetCellValue('B3', 'Riwayat Peminjaman Ruangan ');
            //buat kasi warna aja
            $objPHPExcel->getActiveSheet()->getStyle('B3:D3')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => 'ffff00')
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    )
                )
            );
            $objPHPExcel->getActiveSheet()->SetCellValue('A5', 'No');
            $objPHPExcel->getActiveSheet()->SetCellValue('B5', 'Nim');
            $objPHPExcel->getActiveSheet()->SetCellValue('C5', 'Nama mahasiswa');
            $objPHPExcel->getActiveSheet()->SetCellValue('D5', 'Laboratorium');
            $objPHPExcel->getActiveSheet()->SetCellValue('E5', 'Tanggal Pinjam');
            $objPHPExcel->getActiveSheet()->SetCellValue('F5', 'Jam Mulai');
            $objPHPExcel->getActiveSheet()->SetCellValue('G5', 'Jam Selesai');
            $objPHPExcel->getActiveSheet()->SetCellValue('H5', 'Tujuan Peminjaman');
            //buat kasi warna aja
            $objPHPExcel->getActiveSheet()->getStyle('A5:H5')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '99ccff')
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    )
                )
            );
            //buat kasi warna aja
            $rowCount = 6;
            $no = 0;
            foreach ($tampilsemua as $element) {
                $no++;
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $no);
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element->nim_mahasiswa);
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element->nama_mahasiswa);
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element->nama_ruang);
                $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element->tgl_pinjam);
                $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element->jam_mulai);
                $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element->jam_selesai);
                $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $element->keterangan);
                $rowCount++;
            }

            $rowCount = $rowCount +2;
            $rowCountkab = $rowCount+1;
        
            $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
            $objWriter->save(ROOT_UPLOAD_IMPORT_PATH . $fileName);
            // download file
            header("Content-Type: application/vnd.ms-excel");
            redirect(HTTP_UPLOAD_IMPORT_PATH . $fileName);
            redirect('admin/peminjaman/diterima');
            }
            
        }
        // export excel peminjaman ruangan
        public function selesai_peminjaman_alat($kode){
                        if($this->session->userdata('nama_admin')==null){
                redirect('admin/login');
            }else{
            $tanggal = date('Y-m-d');
            $data = array(
                'tgl_pengembalian'=> $tanggal,
            );
            $query = $this->m_mhs_peminjaman->selesai_peminjaman_alat($kode, $data);
            if($query){
                $this->session->set_flashdata('pesan', 'Peminjaman Selesai');
            }
            redirect('admin/peminjaman/diterima_alat');
            }
        }
}
 
 
?>