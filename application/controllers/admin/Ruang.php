<?php 

class Ruang extends CI_Controller{
    public function __construct(){

    parent::__construct();
        $this->load->helper('url');
        $this->load->library('Session');
        $this->load->model('m_mhs_login');
        $this->load->model('m_ruang');
    }
	public function index()
	{
                    if($this->session->userdata('nama_admin')==null){
                redirect('admin/login');
            }else{
        $data['ruang'] = $this->m_ruang->list_ruang();
        $this->load->view('admin/ruang/v_ruang', $data);
            }
    }
    public function edit_ruang(){
                    if($this->session->userdata('nama_admin')==null){
                redirect('admin/login');
            }else{
        $nama = $this->input->post('xnama');
        $kapasitas = $this->input->post('xkapasitas');
        $id = $this->input->post('xid');
        $query = $this->m_ruang->update_ruang($nama, $kapasitas, $id);
        if($query){
            $this->session->set_flashdata('pesan', 'Ruangan Berhasil di update');
        }
        redirect('admin/ruang');
    }
    }
    public function hapus_ruang($id){
                    if($this->session->userdata('nama_admin')==null){
                redirect('admin/login');
            }else{
        $query= $this->m_ruang->hapus_ruang($id);
        if($query){
            $this->session->set_flashdata('pesan', 'Ruangan Berhasil di hapus');
        }
        redirect('admin/ruang');
    }
    }
    public function simpan_ruang(){
                    if($this->session->userdata('nama_admin')==null){
                redirect('admin/login');
            }else{
        echo $nama = $this->input->post('xnama');
        echo $kapasitas = $this->input->post('xkapasitas');
        $query = $this->m_ruang->simpan_ruang($nama, $kapasitas);
        if($query){
            $this->session->set_flashdata('pesan', 'Ruangan Berhasil di tambah');
        }
        redirect('admin/ruang');
    }
    }
    

}
?>