<?php 

class Calendar extends CI_Controller{
   
    public function __construct(){
        parent::__construct();
            $this->load->helper('url');
            $this->load->library('Session');
            $this->load->model('m_mhs_peminjaman');
        }

	    public function index()
        {
            if($this->session->userdata('nama_admin')==null){
                redirect('admin/login');
            }else{
            $data['diterima'] = $this->m_mhs_peminjaman->diterima(); 
            $this->load->view('admin/calendar/v_calendar', $data);
            }
        }

        function detailcal($kode)
        {
            if($this->session->userdata('nama_admin')==null)
            {
                redirect('admin/login');
            }else{
            $data['detailpeminjaman'] = $this->m_mhs_peminjaman->detail_peminjam($kode);
            $this->load->view('admin/calendar/v_detail_calendar', $data);
            }
        }

        

}
 
 
?>