<?php
class M_mhs_login extends CI_model{

    public function daftar_mahasiswa($nama, $nim, $email, $kode, $password){
        $query = $this->db->query("INSERT INTO tbl_mahasiswa(nim, nama_mahasiswa, email_mahasiswa, password, kode)
        VALUES('$nim', '$nama','$email','$password','$kode')");
        return $query;
    }

    public function login_mhs($email,$pass){

        $this->db->select('*');
        $this->db->from('tbl_mahasiswa');
        $this->db->where('email_mahasiswa',$email);
        $this->db->where('password',$pass);

        if($query=$this->db->get())
        {
            return $query->row_array();
        }
        else{
            return false;
        }
    }
    public function cek_email($email){

        $this->db->select('*');
        $this->db->from('tbl_mahasiswa');
        $this->db->where('email_mahasiswa',$email);
        $query=$this->db->get();
        if($query->num_rows()>0){
            return false;
        }else{
            return true;
        }
    }

    public function email_checkpass($email){
        $query = $this->db->query("SELECT email_mahasiswa FROM tbl_mahasiswa WHERE email_mahasiswa='$email'");
        return $query->result();
    }
    

    public function update_id($email_sekolah,$id_pelanggan){
        $query = $this->db->query("UPDATE tbl_customer set id_customer='$id_pelanggan' WHERE email_sekolah='$email_sekolah'");
        return $query;
    }

    public function riwayat_customer(){
        $idcustomer = $this->session->userdata('id_sekolah');
        $query = $this->db->query("SELECT sum(tbl_transaksi.banyak_barang) as banyak_barang,tbl_transaksi.kode_transaksi, tbl_transaksi.tanggal, sum(tbl_transaksi.total_harga) as total_harga, tbl_transaksi.status_transaksi AS status 
        FROM tbl_transaksi WHERE tbl_transaksi.id_sekolah='$idcustomer' GROUP BY tbl_transaksi.kode_transaksi ORDER BY tbl_transaksi.tanggal DESC");
        return $query->result();
    }

    public function riwayat_proses(){
        $idcustomer = $this->session->userdata('id_sekolah');
        $query = $this->db->query("SELECT sum(tbl_pesanan.jumlah_buku) as banyak_barang, tbl_pesanan.tanggal, sum(tbl_pesanan.total_harga) as total_harga, tbl_pesanan.id_transaksi as kode_transaksi 
        FROM tbl_pesanan WHERE tbl_pesanan.id_pembeli='$idcustomer' GROUP BY tbl_pesanan.tanggal ORDER BY tbl_pesanan.tanggal DESC");
        return $query->result();
    }

    public function ambil_id($email){
        $query = $this->db->query("SELECT id FROM tbl_mahasiswa WHERE email_mahasiswa='$email'");
        return $query->row()->id;
    }

    function ambilmahasiswa($email){
        $query = $this->db->get_where('tbl_mahasiswa',array('email_mahasiswa'=>$email));
		return $query->row_array();
    }

    function activin($email){
        $status = 1;
        $query = $this->db->query("UPDATE tbl_mahasiswa set aktif='$status' WHERE email_mahasiswa='$email'");
        return $query;
    }

    function ambil_status($email){
        $query = $this->db->query("SELECT aktif FROM tbl_mahasiswa WHERE email_mahasiswa='$email'");
        return $query->row()->aktif;
    }

    function ambil_email($id){
        $query = $this->db->query("SELECT email_mahasiswa FROM tbl_mahasiswa WHERE id='$id'");
        return $query->row()->email_mahasiswa;
    }
    
    function passworbaru($id, $password){
        $query = $this->db->query("UPDATE tbl_mahasiswa set password='$password' WHERE id='$id'");
        return $query;
    }
    function list_mahasiswa(){
        $query = $this->db->query("SELECT *FROM tbl_mahasiswa");
        return $query->result();
    }
    function banyak_mahasiswa(){
        $query = $this->db->query("SELECT count(id) as banyakmahasiswa FROM tbl_mahasiswa");
        return $query->row()->banyakmahasiswa;
    }



}
    


?>
