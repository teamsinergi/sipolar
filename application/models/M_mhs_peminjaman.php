<?php
class M_mhs_peminjaman extends CI_model{

    public function ruangan(){
        $query = $this->db->query("SELECT *FROM tbl_ruang");
        return $query->result();
    }
    public function ruangan_pilih($idruang){
        $query = $this->db->query("SELECT *FROM tbl_ruang WHERE id_ruang='$idruang'");
        return $query->result();
    }
    public function ruangan_cek($idruang){
        $query = $this->db->query("SELECT *FROM tbl_ruang WHERE id_ruang != '$idruang'");
        return $query->result();
    }

    public function ruangan_cek_rev($tanggal,$mulai, $selesai){
        // $query = $this->db->query("SELECT tbl_ruang.id_ruang, tbl_ruang.nama_ruang
        // FROM tbl_ruang, tbl_peminjaman
        // WHERE tbl_ruang.id_ruang = tbl_peminjaman.id_ruang AND tbl_peminjaman.durasi LIKE '%$durasi%'");
        $query = $this->db->query("SELECT id_ruang FROM tbl_peminjaman
        WHERE tgl_pinjam='$tanggal' AND durasi LIKE '%$mulai%' AND durasi LIKE '$selesai'");
        return $query->result();
    }

    public function jam(){
        $query = $this->db->query("SELECT *FROM tbl_jam");
        return $query->result();
    }
    public function simpan_pinjam($nim, $ruangan, $tanggalpinjam, $jammulai, $jamselesai, $keterangan, $durasi){
        $status = 1;
        $date = date("Y-m-d H:i:s");
        $hari = date('s');
        $kode = $nim.$ruangan.$hari;
        $query = $this->db->query("INSERT INTO tbl_peminjaman(nim_mahasiswa,id_ruang, tgl_pinjam, jam_mulai, jam_selesai, keterangan, status, create_at,kode_peminjaman, durasi)
        VALUES('$nim','$ruangan','$tanggalpinjam','$jammulai','$jamselesai','$keterangan','$status','$date','$kode','$durasi')");
        return $query;
    }

    public function peminjaman_masuk(){
        $query = $this->db->query("SELECT tbl_mahasiswa.nim, tbl_mahasiswa.nama_mahasiswa, tbl_ruang.nama_ruang, tbl_peminjaman.jam_mulai as mulai, tbl_peminjaman.jam_selesai as selesai, tbl_peminjaman.keterangan, tbl_peminjaman.tgl_pinjam as tanggal, tbl_peminjaman.kode_peminjaman as kode, tbl_peminjaman.create_at
        FROM tbl_peminjaman, tbl_ruang, tbl_mahasiswa 
        WHERE tbl_peminjaman.status='1' AND tbl_peminjaman.id_ruang = tbl_ruang.id_ruang AND tbl_peminjaman.nim_mahasiswa = tbl_mahasiswa.nim ORDER BY tbl_peminjaman.tgl_pinjam ASC");
        return $query->result();
    }
    public function detail($kode){
        $query = $this->db->query("SELECT tbl_mahasiswa.nim, tbl_mahasiswa.nama_mahasiswa, tbl_ruang.nama_ruang, tbl_peminjaman.jam_mulai as mulai, tbl_peminjaman.jam_selesai as selesai, tbl_peminjaman.keterangan, tbl_peminjaman.tgl_pinjam as tanggal, tbl_peminjaman.kode_peminjaman as kode
        FROM tbl_peminjaman, tbl_ruang, tbl_mahasiswa 
        WHERE tbl_peminjaman.status='1' AND tbl_peminjaman.id_ruang = tbl_ruang.id_ruang AND tbl_peminjaman.nim_mahasiswa = tbl_mahasiswa.nim AND tbl_peminjaman.kode_peminjaman='$kode'");
        return $query->result();
    }
    public function detail_tanggal($kode){
        $query = $this->db->query("SELECT tgl_pinjam as tanggal FROM tbl_peminjaman WHERE kode_peminjaman='$kode'");
        return $query->row()->tanggal;
    }

    public function terboking($tanggal){
        $query = $this->db->query("SELECT tbl_mahasiswa.nim, tbl_mahasiswa.nama_mahasiswa, tbl_ruang.nama_ruang, tbl_peminjaman.jam_mulai as mulai, tbl_peminjaman.jam_selesai as selesai, tbl_peminjaman.keterangan, tbl_peminjaman.tgl_pinjam as tanggal, tbl_peminjaman.kode_peminjaman as kode
        FROM tbl_peminjaman, tbl_ruang, tbl_mahasiswa 
        WHERE tbl_peminjaman.status='1' AND tbl_peminjaman.id_ruang = tbl_ruang.id_ruang AND tbl_peminjaman.nim_mahasiswa = tbl_mahasiswa.nim AND tbl_peminjaman.tgl_pinjam='$tanggal'");
        return $query->result();
    }

    public function terima_peminjaman($kode){
        $query = $this->db->query("UPDATE tbl_peminjaman SET status='2' WHERE kode_peminjaman='$kode'");
        return $query;
    }
    public function terima_peminjaman_komputer($kode){
        $query = $this->db->query("UPDATE tbl_peminjaman_komputer SET status='2' WHERE kode_peminjaman_komputer='$kode'");
        return $query;
    }

    public function permintaan_ditolak($kode, $alasan){
        $query = $this->db->query("UPDATE tbl_peminjaman SET alasan='$alasan', status='3' WHERE kode_peminjaman='$kode'");
        return $query;
    }

    public function ditolak(){
        $query = $this->db->query("SELECT tbl_mahasiswa.nim, tbl_mahasiswa.nama_mahasiswa, tbl_ruang.nama_ruang, tbl_peminjaman.jam_mulai as mulai, tbl_peminjaman.jam_selesai as selesai, tbl_peminjaman.keterangan, tbl_peminjaman.tgl_pinjam as tanggal, tbl_peminjaman.kode_peminjaman as kode
        FROM tbl_peminjaman, tbl_ruang, tbl_mahasiswa 
        WHERE tbl_peminjaman.status='3' AND tbl_peminjaman.id_ruang = tbl_ruang.id_ruang AND tbl_peminjaman.nim_mahasiswa = tbl_mahasiswa.nim");
        return $query->result();
    }
    public function ditolak_komputer(){
        $query = $this->db->query("SELECT tbl_peminjaman_komputer.kode_komputer, tbl_mahasiswa.nim, tbl_mahasiswa.nama_mahasiswa, tbl_ruang.nama_ruang, tbl_peminjaman_komputer.tgl_mulai, tbl_peminjaman_komputer.keterangan,tbl_peminjaman_komputer.kode_peminjaman_komputer
        FROM tbl_peminjaman_komputer, tbl_ruang, tbl_mahasiswa 
        WHERE tbl_peminjaman_komputer.status='3' AND tbl_peminjaman_komputer.id_ruang = tbl_ruang.id_ruang AND tbl_peminjaman_komputer.nim_mahasiswa = tbl_mahasiswa.nim GROUP BY tbl_peminjaman_komputer.kode_peminjaman_komputer");
        return $query->result();
    }

    public function diterima(){
        $query = $this->db->query("SELECT tbl_mahasiswa.nim, tbl_mahasiswa.nama_mahasiswa, tbl_ruang.nama_ruang, tbl_peminjaman.jam_mulai as mulai, tbl_peminjaman.jam_selesai as selesai, tbl_peminjaman.keterangan, tbl_peminjaman.tgl_pinjam as tanggal, tbl_peminjaman.kode_peminjaman as kode, year(tbl_peminjaman.tgl_pinjam) as tahun, month(tbl_peminjaman.tgl_pinjam) as bulan, day(tbl_peminjaman.tgl_pinjam) as hari, HOUR(tbl_peminjaman.jam_mulai) as jammulai, MINUTE(tbl_peminjaman.jam_mulai) as menitmulai
        FROM tbl_peminjaman, tbl_ruang, tbl_mahasiswa 
        WHERE tbl_peminjaman.status='2' AND tbl_peminjaman.id_ruang = tbl_ruang.id_ruang AND tbl_peminjaman.nim_mahasiswa = tbl_mahasiswa.nim");
        return $query->result();
    }
    public function diterima_komputer(){
        $query = $this->db->query("SELECT tbl_peminjaman_komputer.lama_peminjaman, tbl_peminjaman_komputer.kode_komputer, tbl_mahasiswa.nim, tbl_mahasiswa.nama_mahasiswa, tbl_ruang.nama_ruang, tbl_peminjaman_komputer.tgl_mulai, tbl_peminjaman_komputer.keterangan,tbl_peminjaman_komputer.kode_peminjaman_komputer
        FROM tbl_peminjaman_komputer, tbl_ruang, tbl_mahasiswa 
        WHERE tbl_peminjaman_komputer.status='2' AND tbl_peminjaman_komputer.id_ruang = tbl_ruang.id_ruang AND tbl_peminjaman_komputer.nim_mahasiswa = tbl_mahasiswa.nim GROUP BY tbl_peminjaman_komputer.kode_peminjaman_komputer");
        return $query->result();
    }

    public function identitas($kode){
        $query = $this->db->query("SELECT tbl_mahasiswa.nama_mahasiswa, tbl_peminjaman.nim_mahasiswa, tbl_ruang.nama_ruang, tbl_peminjaman.keterangan, tbl_peminjaman.tgl_pinjam, tbl_peminjaman.jam_mulai, tbl_peminjaman.jam_selesai, tbl_mahasiswa.email_mahasiswa
        FROM tbl_peminjaman, tbl_mahasiswa, tbl_ruang
        WHERE tbl_peminjaman.nim_mahasiswa = tbl_mahasiswa.nim AND tbl_peminjaman.id_ruang = tbl_ruang.id_ruang AND tbl_peminjaman.kode_peminjaman ='$kode'  ");
        return $query->result();
    }

    public function identitas_alat($kode){
        $this->db->select('*');
        $this->db->from('tbl_peminjaman_alat');
        $this->db->join('tbl_mahasiswa', 'tbl_mahasiswa.nim = tbl_peminjaman_alat.nim_mahasiswa');
        $this->db->join('tbl_barang', 'tbl_barang.id = tbl_peminjaman_alat.id');
        $this->db->where('tbl_peminjaman_alat.kode_peminjaman',$kode);
        $query = $this->db->get();
        return $query->result();
    }
    
    public function identitas_komputer($kode){
        $query = $this->db->query("SELECT tbl_peminjaman_komputer.keterangan, tbl_mahasiswa.nama_mahasiswa, tbl_mahasiswa.email_mahasiswa, tbl_mahasiswa.no_hp, tbl_peminjaman_komputer.nim_mahasiswa, tbl_peminjaman_komputer.kode_komputer, tbl_peminjaman_komputer.tgl_mulai, tbl_ruang.nama_ruang
        FROM tbl_peminjaman_komputer, tbl_ruang, tbl_mahasiswa
        WHERE tbl_peminjaman_komputer.nim_mahasiswa = tbl_mahasiswa.nim AND tbl_peminjaman_komputer.id_ruang = tbl_ruang.id_ruang AND tbl_peminjaman_komputer.kode_peminjaman_komputer='$kode' GROUP BY tbl_peminjaman_komputer.kode_peminjaman_komputer");
        return $query->result();
    }

    public function detail_peminjam($kode){
        $query = $this->db->query("SELECT tbl_ruang.nama_ruang, tbl_mahasiswa.nama_mahasiswa, tbl_peminjaman.tgl_pinjam, tbl_peminjaman.jam_mulai, tbl_peminjaman.jam_selesai, tbl_peminjaman.keterangan, tbl_peminjaman.kode_peminjaman as kode, tbl_peminjaman.create_at
        FROM tbl_peminjaman, tbl_ruang, tbl_mahasiswa
        WHERE tbl_peminjaman.nim_mahasiswa = tbl_mahasiswa.nim AND tbl_peminjaman.id_ruang = tbl_ruang.id_ruang AND tbl_peminjaman.kode_peminjaman='$kode'");
        return $query->result();
    }
    public function pinjaman_masuk(){
        $query = $this->db->query("SELECT count(id) as peminjamanmasuk FROM tbl_peminjaman WHERE status='1'");
        return $query->row()->peminjamanmasuk;
    }

    public function pinjaman_masuk_komputer(){
        $query = $this->db->query("SELECT count(kode_peminjaman_komputer) as peminjamanmasuk_komputer FROM tbl_peminjaman_komputer WHERE status='1'");
        return $query->row()->peminjamanmasuk_komputer;
    }
    public function pinjaman_alat_masuk(){
        $query = $this->db->query("SELECT count(id_peminjaman) as peminjaman_alat FROM tbl_peminjaman_alat WHERE id_status='1'");
        return $query->row()->peminjaman_alat;
    }
    public function terpinjam(){
        $query = $this->db->query("SELECT count(id) as terpinjam FROM tbl_peminjaman WHERE status='2'");
        return $query->row()->terpinjam;
    }
    public function terpinjam_komputer(){
        $query = $this->db->query("SELECT count(id) as terpinjam_komputer FROM tbl_peminjaman_komputer WHERE status='2'");
        return $query->row()->terpinjam_komputer;
    }
    function list_komputer(){
        $query = $this->db->query("SELECT *FROM tbl_komputer");
        return $query->result();
    }
    function list_komputer_1(){
        $query = $this->db->query("SELECT *FROM tbl_komputer WHERE kode_komputer <= 12");
        return $query->result();
    }
    function list_komputer_2(){
        $query = $this->db->query("SELECT *FROM tbl_komputer WHERE kode_komputer >= 13 AND kode_komputer <=30");
        return $query->result();
    }
    function list_komputer_3(){
        $query = $this->db->query("SELECT *FROM tbl_komputer WHERE kode_komputer >= 31 AND kode_komputer <=42");
        return $query->result();
    }
    public function list_komputer_terpinjam($tahun, $bulan, $tanggal, $idlab){
        $query = $this->db->query("SELECT *FROM tbl_peminjaman_komputer WHERE year(tgl_mulai)='$tahun' AND MONTH(tgl_mulai)='$bulan' AND DAY(tgl_mulai)='$tanggal' AND id_ruang='$idlab' AND status != 3");
        return $query->result();
    }
    
    public function simpan_pinjam_komputer($kode_komputer, $tanggal, $keterangan, $nim, $lab, $lama){
        $date = date('Y-m-d h:i:s');
        $date = date("Y-m-d H:i:s");
        $hari = date('s');
        $kode = $nim.$kode_komputer.$hari;
        $query = $this->db->query("INSERT INTO tbl_peminjaman_komputer(kode_komputer, nim_mahasiswa, tgl_mulai, keterangan, create_at, status, kode_peminjaman_komputer, id_ruang, lama_peminjaman)
        VALUES('$kode_komputer','$nim','$tanggal','$keterangan','$date','1','$kode','$lab','$lama')");
        return $query;
    }
    public function peminjaman_masuk_komputer(){
        $query = $this->db->query("SELECT tbl_peminjaman_komputer.lama_peminjaman, tbl_peminjaman_komputer.kode_peminjaman_komputer, tbl_peminjaman_komputer.kode_komputer, tbl_peminjaman_komputer.kode_komputer, tbl_peminjaman_komputer.nim_mahasiswa, tbl_peminjaman_komputer.tgl_mulai, tbl_peminjaman_komputer.keterangan, tbl_peminjaman_komputer.id,
        tbl_mahasiswa.nama_mahasiswa, tbl_status.keterangan as alasanminjam, tbl_ruang.nama_ruang
        FROM tbl_peminjaman_komputer, tbl_mahasiswa, tbl_status, tbl_ruang
        WHERE tbl_ruang.id_ruang = tbl_peminjaman_komputer.id_ruang AND tbl_peminjaman_komputer.nim_mahasiswa = tbl_mahasiswa.nim AND tbl_peminjaman_komputer.status = tbl_status.id AND tbl_peminjaman_komputer.status='1' GROUP BY tbl_peminjaman_komputer.kode_peminjaman_komputer ORDER BY tbl_peminjaman_komputer.create_at DESC");
        return $query->result();
    }
    public function detail_peminjaman_komputer($kode){
        $query = $this->db->query("SELECT tbl_peminjaman_komputer.kode_peminjaman_komputer, tbl_peminjaman_komputer.kode_komputer, tbl_peminjaman_komputer.kode_komputer, tbl_peminjaman_komputer.nim_mahasiswa, tbl_peminjaman_komputer.tgl_mulai, tbl_peminjaman_komputer.keterangan, tbl_peminjaman_komputer.id,
        tbl_mahasiswa.nama_mahasiswa, tbl_status.keterangan as alasanminjam
        FROM tbl_peminjaman_komputer, tbl_mahasiswa, tbl_status
        WHERE tbl_peminjaman_komputer.nim_mahasiswa = tbl_mahasiswa.nim AND tbl_peminjaman_komputer.status = tbl_status.id AND tbl_peminjaman_komputer.kode_peminjaman_komputer='$kode' GROUP BY tbl_peminjaman_komputer.kode_peminjaman_komputer");
        return $query->result();
    }
    public function permintaan_ditolak_komputer($kode, $alasan){
        $query = $this->db->query("UPDATE tbl_peminjaman_komputer SET alasan_penolakan='$alasan', status='3' WHERE kode_peminjaman_komputer='$kode'");
        return $query;
    }
    public function id_lab(){
        $query = $this->db->query("SELECT id_ruang FROM tbl_ruang LIMIT 1");
        return $query->row()->id_ruang;
    }
    public function nama_lab($idlab){
        $query = $this->db->query("SELECT nama_ruang FROM tbl_ruang WHERE id_ruang ='$idlab'");
        return $query->row()->nama_ruang;
    }
    public function nomer_surat($tanggal){
        $query = $this->db->query("SELECT COUNT(id) as no_surat FROM tbl_peminjaman WHERE create_at<'$tanggal' AND status=2");
        return $query->row()->no_surat;
    }
    public function nomer_surat_alat($tanggal){
        $query = $this->db->query("SELECT COUNT(id) as no_surat FROM tbl_peminjaman_alat WHERE create_at<'$tanggal' AND id_status=2");
        return $query->row()->no_surat;
    }
    public function nomer_surat_komputer($tanggal){
        $query = $this->db->query("SELECT COUNT(id) as no_surat FROM tbl_peminjaman_komputer WHERE create_at<'$tanggal' AND status=2");
        return $query->row()->no_surat;
    }
    public function tanggal_create($kode){
        $query = $this->db->query("SELECT create_at FROM tbl_peminjaman WHERE kode_peminjaman='$kode'");
        return $query->row()->create_at;
    }
    public function tanggal_create_alat($kode){
        $query = $this->db->query("SELECT create_at FROM tbl_peminjaman_alat WHERE kode_peminjaman='$kode'");
        return $query->row()->create_at;
    }

    public function tanggal_create_komputer($kode){
        $query = $this->db->query("SELECT create_at FROM tbl_peminjaman_komputer WHERE kode_peminjaman_komputer='$kode'");
        return $query->row()->create_at;
    }
    public function nama_lab_terpinjam($kode){
        $query = $this->db->query("SELECT tbl_ruang.nama_ruang
        FROM tbl_ruang, tbl_peminjaman
        WHERE tbl_ruang.id_ruang= tbl_peminjaman.id_ruang AND tbl_peminjaman.kode_peminjaman='$kode'");
        return $query->row()->nama_ruang;
    }
    public function nama_lab_terpinjam_komputer($kode){
        $query = $this->db->query("SELECT tbl_ruang.nama_ruang
        FROM tbl_ruang, tbl_peminjaman_komputer
        WHERE tbl_ruang.id_ruang= tbl_peminjaman_komputer.id_ruang AND tbl_peminjaman_komputer.kode_peminjaman_komputer='$kode'");
        return $query->row()->nama_ruang;
    }
    public function total_inventory(){
        $query = $this->db->query("SELECT count(id) as banyakinventori FROM tbl_barang");
        return $query->row()->banyakinventori;
    }
    public function tanggal_pinjam($kode){
        $query = $this->db->query("SELECT tgl_mulai FROM tbl_peminjaman_komputer WHERE kode_peminjaman_komputer ='$kode'");
        return $query->row()->tgl_mulai;
    }
    public function namalab($kode){
        $query = $this->db->query("SELECT nama_ruang FROM tbl_ruang, tbl_peminjaman_komputer
        WHERE tbl_peminjaman_komputer.id_ruang = tbl_ruang.id_ruang AND tbl_peminjaman_komputer.kode_peminjaman_komputer='$kode'");
        return $query->row()->nama_ruang;
    }
    public function kode_komputer($kode){
        $query = $this->db->query("SELECT kode_komputer FROM tbl_peminjaman_komputer
        WHERE tbl_peminjaman_komputer.kode_peminjaman_komputer='$kode'");
        return $query->row()->kode_komputer;
    }
    public function cek_ketersediaan_komputer($tanggal_pinjam, $kode_komputer){
        $query = $this->db->query("SELECT *FROM tbl_peminjaman_komputer WHERE kode_komputer='$kode_komputer' AND tgl_mulai='$tanggal_pinjam'");
        return $query->result();
    }
    public function ruangan_tersewa($tanggal){
        $query = $this->db->query("SELECT id_ruang FROM tbl_peminjaman WHERE tgl_pinjam ='$tanggal'");
        return $query->row()->id_ruang;
    }
    public function cek_ketersediaan_ruangan($tanggal){
        $query = $this->db->query("SELECT *FROM tbl_peminjaman WHERE tgl_pinjam='$tanggal'");
        return $query->result();
    }
    public function cari_alat($nama){
        $query = $this->db->query("SELECT * FROM tbl_barang WHERE status=1 AND nama_barang like '%$nama%' LIMIT 5");
        return $query->result();
    }
    public function simpan_pinjam_alat($data){
        $this->db->insert('tbl_peminjaman_alat', $data);
    }
    public function peminjaman_alat(){
        $this->db->select('*');
        $this->db->from('tbl_peminjaman_alat');
        $this->db->join('tbl_barang', 'tbl_peminjaman_alat.id=tbl_barang.id');
        $this->db->join('tbl_mahasiswa', 'tbl_peminjaman_alat.nim_mahasiswa=tbl_mahasiswa.nim');
        $this->db->where('id_status',1);
        $query = $this->db->get();
        return $query->result();
    }
    public function peminjaman_alat_diterima(){
        $this->db->select('*');
        $this->db->from('tbl_peminjaman_alat');
        $this->db->join('tbl_barang', 'tbl_peminjaman_alat.id=tbl_barang.id');
        $this->db->join('tbl_mahasiswa', 'tbl_peminjaman_alat.nim_mahasiswa=tbl_mahasiswa.nim');
        $this->db->where('id_status',2);
        $query = $this->db->get();
        return $query->result();
    }
    public function peminjaman_alat_ditolak(){
        $this->db->select('*');
        $this->db->from('tbl_peminjaman_alat');
        $this->db->join('tbl_barang', 'tbl_peminjaman_alat.id=tbl_barang.id');
        $this->db->join('tbl_mahasiswa', 'tbl_peminjaman_alat.nim_mahasiswa=tbl_mahasiswa.nim');
        $this->db->where('id_status',3);
        $query = $this->db->get();
        return $query->result();
    }
    public function terima_peminjaman_alat($data, $kode){
        $this->db->where('kode_peminjaman', $kode);
        $this->db->update('tbl_peminjaman_alat', $data);
    }
    public function permintaan_ditolak_alat($data,$kode){
        $this->db->where('kode_peminjaman', $kode);
        $this->db->update('tbl_peminjaman_alat',$data);
    }

    public function list_alat_tersewa_export_excel(){
        $this->db->select('*');
        $this->db->from('tbl_peminjaman_alat');
        $this->db->join('tbl_mahasiswa', 'tbl_mahasiswa.nim = tbl_peminjaman_alat.nim_mahasiswa');
        $this->db->join('tbl_barang', 'tbl_barang.id = tbl_peminjaman_alat.id');
        $this->db->where('tbl_peminjaman_alat.id_status', 2);
        $query = $this->db->get();
        return $query->result();
    }

    public function list_komputer_tersewa_export_excel(){
        $this->db->select('*');
        $this->db->from('tbl_peminjaman_komputer');
        $this->db->join('tbl_mahasiswa', 'tbl_mahasiswa.nim = tbl_peminjaman_komputer.nim_mahasiswa');
        $this->db->join('tbl_ruang', 'tbl_ruang.id_ruang = tbl_peminjaman_komputer.id_ruang');
        $this->db->group_by("tbl_peminjaman_komputer.kode_peminjaman_komputer");
        $this->db->where('tbl_peminjaman_komputer.status', 2);
        $query = $this->db->get();
        return $query->result();
    }
    public function list_ruangan_tersewa_export_excel(){
        $this->db->select('*');
        $this->db->from('tbl_peminjaman');
        $this->db->join('tbl_mahasiswa', 'tbl_mahasiswa.nim = tbl_peminjaman.nim_mahasiswa');
        $this->db->join('tbl_ruang', 'tbl_ruang.id_ruang = tbl_peminjaman.id_ruang');
        $this->db->group_by("tbl_peminjaman.kode_peminjaman");
        $this->db->where('tbl_peminjaman.status', 2);
        $query = $this->db->get();
        return $query->result();
    }
    public function selesai_peminjaman_alat($kode, $data){
        $this->db->where('kode_peminjaman', $kode);
        $this->db->update('tbl_peminjaman_alat', $data);
    }
    public function tanggal_selesai_komputer($kode){
        $query = $this->db->query("SELECT tgl_mulai
        FROM tbl_peminjaman_komputer
        WHERE kode_peminjaman_komputer ='$kode' ORDER BY tgl_mulai DESC LIMIT 1");
        return $query->row()->tgl_mulai;
    }

    public function ambil_jam_mulai($mulai){
        $query = $this->db->query("SELECT mulai FROM tbl_jam WHERE jam_ke='$mulai'");
        return $query->row()->mulai;
    }
    public function ambil_jam_selesai($selesai){
        $query = $this->db->query("SELECT mulai FROM tbl_jam WHERE jam_ke='$selesai'");
        return $query->row()->mulai;
    }
    
    public function ruangan_tersewa_tanggal($tanggal){
        $this->db->select('*');
        $this->db->from('tbl_peminjaman');
        $this->db->where('tgl_pinjam', $tanggal);
        $query = $this->db->get();
        return $query->result();
    }

    public function ambil_id_ruang_tersewa($tanggal, $i){
        $query = $this->db->query("SELECT id_ruang FROM tbl_peminjaman
        WHERE tgl_pinjam ='$tanggal' AND durasi LIKE '%$i%' GROUP BY kode_peminjaman");
        $row = $query->row();
        if (isset($row))
        {
                return $row->id_ruang;
        }
    }

    public function update_cek_ketersediaan($idlab,$tanggal,$mulai, $selesai){
        $query = $this->db->query("UPDATE tbl_peminjaman set cek_ketersediaan='1'
        WHERE id_ruang='$idlab' AND tgl_pinjam='$tanggal' AND durasi LIKE '%$mulai%' OR durasi LIKE '%$selesai%'");
    }
    public function ambil_waktu($tanggal, $idruang){
        $query = $this->db->query("SELECT durasi FROM tbl_peminjaman WHERE id_ruang='$idruang' AND tgl_pinjam='$tanggal'");
        return $query->result();
    }

}
?>