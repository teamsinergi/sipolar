<?php 
class M_barang extends CI_model{

    public function simpan_barang($nama, $lab, $banyak, $keterangan, $kepemilikan, $kondisi, $tanggal, $status){
        $query = $this->db->query("INSERT INTO tbl_barang(nama_barang, banyak_barang, keterangan, tanggal, id_ruang, kepemilikan, kondisi, status) VALUES ('$nama','$banyak','$keterangan','$tanggal','$lab','$kepemilikan','$kondisi','$status')");
        return $query;
    }
    public function list_barang(){
        // $query = $this->db->query("SELECT tbl_barang.tanggal_cek, tbl_barang.id, tbl_barang.tanggal_cek, tbl_barang.id_ruang, tbl_barang.id, tbl_barang.nama_barang, tbl_barang.banyak_barang, tbl_barang.keterangan, tbl_barang.kepemilikan, tbl_barang.kondisi, tbl_barang.tanggal, tbl_ruang.nama_ruang
        // FROM tbl_barang, tbl_ruang
        // WHERE tbl_barang.id_ruang = tbl_ruang.id_ruang");
        $this->db->select('*, tbl_barang.status as status');
        $this->db->from('tbl_barang');
        $this->db->join('tbl_ruang', 'tbl_barang.id_ruang=tbl_ruang.id_ruang');
        $query = $this->db->get();
        return $query->result();
    }
    public function list_detail_barang($id){
        $query = $this->db->query("SELECT tbl_barang.status, tbl_barang.id, tbl_barang.tanggal_cek, tbl_barang.id_ruang, tbl_barang.id, tbl_barang.nama_barang, tbl_barang.banyak_barang, tbl_barang.keterangan, tbl_barang.kepemilikan, tbl_barang.kondisi, tbl_barang.tanggal, tbl_ruang.nama_ruang
        FROM tbl_barang, tbl_ruang
        WHERE tbl_barang.id_ruang = tbl_ruang.id_ruang AND tbl_barang.id='$id'");
        return $query->result();
    }
    public function cadanganbarang($id){
        $this->db->select('*, tbl_barang_cadangan.status');
        $this->db->from('tbl_barang_cadangan');
        $this->db->join('tbl_ruang', 'tbl_barang_cadangan.id_ruang=tbl_ruang.id_ruang');
        $this->db->where($id);
        $query = $this->db->get();
        return $query->result();

        // $query = $this->db->query("SELECT tbl_barang_cadangan.id, tbl_barang_cadangan.nama_barang, tbl_ruang.nama_ruang, tbl_barang_cadangan.banyak_barang, tbl_barang_cadangan.keterangan, tbl_barang_cadangan.kepemilikan, tbl_barang_cadangan.kondisi, tbl_barang_cadangan.tanggal
        // FROM tbl_barang_cadangan, tbl_ruang
        // WHERE tbl_barang_cadangan.id_ruang = tbl_ruang.id_ruang AND tbl_barang_cadangan.id='$id'");
        // return $query->result();
    }
    public function list_barang_export($lab){
        $query = $this->db->query("SELECT tbl_barang.tanggal_cek, tbl_barang.id_ruang, tbl_barang.id, tbl_barang.nama_barang, tbl_barang.banyak_barang, tbl_barang.keterangan, tbl_barang.kepemilikan, tbl_barang.kondisi, tbl_barang.tanggal, tbl_ruang.nama_ruang
        FROM tbl_barang, tbl_ruang
        WHERE tbl_barang.id_ruang = tbl_ruang.id_ruang AND tbl_barang.id_ruang='$lab'");
        return $query->result();
    }
    public function hapus_barang($id){
        $query = $this->db->query("DELETE FROM tbl_barang WHERE id='$id'");
        return $query;
    }
    public function hapus_barang_c($id){
        $this->db->delete('tbl_barang_cadangan', array('id' => $id));
    }
    public function edit_barang_all($lab, $id, $nama, $banyak, $keterangan, $kepemilikan, $kondisi, $tanggalcek, $status){
        $query = $this->db->query("UPDATE tbl_barang SET id_ruang='$lab',nama_barang='$nama', banyak_barang='$banyak', keterangan='$keterangan', kepemilikan='$kepemilikan', kondisi='$kondisi', tanggal_cek='$tanggalcek', status='$status' WHERE id='$id'");
        return $query;
    }

    public function cek_barang_all($lab, $id, $nama, $banyak, $keterangan, $kepemilikan, $kondisi){
        $tanggal = date('Y-m-d');
        $query = $this->db->query("UPDATE tbl_barang SET id_ruang='$lab',nama_barang='$nama', banyak_barang='$banyak', keterangan='$keterangan', kepemilikan='$kepemilikan', kondisi='$kondisi', tanggal_cek='$tanggal' WHERE id='$id'");
        return $query;
    }
    public function nama_lab($lab){
        $query = $this->db->query("SELECT nama_ruang FROM tbl_ruang WHERE id_ruang='$lab'");
        return $query->row()->nama_ruang;
    }
    public function tambah_pengadaan($data){
        $this->db->insert('tbl_barang_cadangan', $data);
    }
    public function barang_awal($id){
        $query = $this->db->query("SELECT banyak_barang FROM tbl_barang WHERE id='$id'");
        return $query->row()->banyak_barang;
    }
    public function barang_cadangan($id){
        $query = $this->db->query("SELECT sum(banyak_barang) as banyak_barang FROM tbl_barang_cadangan WHERE id='$id'");
        return $query->row()->banyak_barang;
    }
    
    public function update_barang_cadangan($id, $data){
        $this->db->where('id_barang', $id);
        $this->db->update('tbl_barang_cadangan', $data);
    }

    public function list_barang_export_excel(){
        $query = $this->db->query("SELECT tbl_barang.tanggal_cek, tbl_barang.id_ruang, tbl_barang.id, tbl_barang.nama_barang, tbl_barang.banyak_barang, tbl_barang.keterangan, tbl_barang.kepemilikan, tbl_barang.kondisi, tbl_barang.tanggal, tbl_ruang.nama_ruang
        FROM tbl_barang, tbl_ruang
        WHERE tbl_barang.id_ruang = tbl_ruang.id_ruang");
        return $query->result();
    }
    public function hapus_barang_cadangan($id_barang){
        $this->db->delete('tbl_barang_cadangan', array('id_barang' => $id_barang));
    }
    public function status_barang($id){
        $this->db->select('status');
        $this->db->from('tbl_barang');
        $this->db->where('id',$id);
        $query = $this->db->get();
        return $query->row()->status;
    }

}

?>