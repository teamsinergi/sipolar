<?php
class M_mhs_riwayat extends CI_model{

    public function riwayat_peminjaman($nim){
        $query = $this->db->query("SELECT tbl_peminjaman.kode_peminjaman,tbl_peminjaman.alasan, tbl_peminjaman.nim_mahasiswa, tbl_peminjaman.keterangan, tbl_peminjaman.tgl_pinjam, tbl_peminjaman.jam_mulai, tbl_peminjaman.jam_selesai, tbl_peminjaman.status as id_status, tbl_peminjaman.kode_peminjaman as kode,
        tbl_mahasiswa.nama_mahasiswa, tbl_ruang.nama_ruang, tbl_status.keterangan as status
        FROM tbl_peminjaman, tbl_mahasiswa, tbl_ruang, tbl_status
        WHERE tbl_peminjaman.nim_mahasiswa = tbl_mahasiswa.nim AND tbl_peminjaman.id_ruang = tbl_ruang.id_ruang AND tbl_peminjaman.status = tbl_status.id AND tbl_peminjaman.nim_mahasiswa='$nim' GROUP BY tbl_peminjaman.kode_peminjaman ORDER BY tbl_peminjaman.create_at DESC");
        return $query->result();
    }
    public function riwayat_peminjaman_komputer($nim){
        $query = $this->db->query("SELECT  tbl_peminjaman_komputer.lama_peminjaman, tbl_ruang.nama_ruang, tbl_peminjaman_komputer.alasan_penolakan, tbl_peminjaman_komputer.kode_peminjaman_komputer as kode, tbl_peminjaman_komputer.status, tbl_peminjaman_komputer.kode_komputer, tbl_peminjaman_komputer.tgl_mulai, tbl_peminjaman_komputer.keterangan, tbl_mahasiswa.nama_mahasiswa, tbl_status.id as id_status
        FROM tbl_ruang, tbl_peminjaman_komputer, tbl_mahasiswa, tbl_status WHERE tbl_ruang.id_ruang=tbl_peminjaman_komputer.id_ruang AND tbl_peminjaman_komputer.status=tbl_status.id AND tbl_peminjaman_komputer.nim_mahasiswa = tbl_mahasiswa.nim AND tbl_peminjaman_komputer.nim_mahasiswa='$nim' GROUP BY kode_peminjaman_komputer ORDER BY tbl_peminjaman_komputer.create_at DESC");
        return $query->result();
    }
    public function riwayat_alat($nim){
        $this->db->select('*');
        $this->db->from('tbl_peminjaman_alat');
        $this->db->join('tbl_barang', 'tbl_peminjaman_alat.id = tbl_barang.id');
        $this->db->join('tbl_status', 'tbl_peminjaman_alat.id_status = tbl_status.id');
        $this->db->where($nim);
        $query = $this->db->get();
        return $query->result();
    }


}

?>