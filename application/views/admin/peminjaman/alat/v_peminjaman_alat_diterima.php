<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin SIPOLAR</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

<?php $this->load->view('admin/partials/upper-section.php'); ?>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php
    
      $this->load->view("admin/partials/header.php");
      $this->load->view("admin/partials/nav-sidebar.php");
  ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Peminjaman Alat
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Peminjaman Alat</li>
      </ol>
    </section>
      

        <section class="content">

               <div class="box">
            
            
            <div class="box-body">
            <div class="box-body">
              <div class="col">
                <a href="<?php echo base_url().'admin/peminjaman/alat_export_excel'?>" class="btn btn-primary">Export Excel</a>
              </div>
                <br>
            <table id="example1" class="table table-bordered table-hover">
                <thead>
               <tr>
                   <th>No</th>
                   <th>Nim</th>
                   <th>Nama</th>
                   <th>Nama Alat</th>
                   <th>Tanggal Pinjam</th>
                   <th>Tujuan Pinjam</th>
                   <th>Status Pengembalian</th>
                   <th>Kode Peminjaman</th>
                   <th>Option</th>
               </tr>
               </thead>
               
              <tbody>
              <?php  $no=0; foreach($peminjaman as $data): $no++?>
               <tr>
                   <td><?php echo $no?></td>
                   <td><?php echo $data->nim_mahasiswa?></td>
                   <td><?php echo $data->nama_mahasiswa?></td>
                   <td><?php echo $data->nama_barang?></td>
                   <td><?php echo $data->tgl_pinjam?></td>
                   <td><?php echo $data->tujuan?></td>
                   <td>
                      <?php if($data->tgl_pengembalian=="0000-00-00" && $data->tgl_kembali < date('Y-m-d')){?>
                        <span class="badge badge-pill badge-info">Melewati Batas Pengembalian</span>                        
                      <?php }elseif($data->tgl_pengembalian=="0000-00-00" || $data->tgl_pengembalian==null){?>
                        <span class="badge badge-pill badge-info">Belum Kembali</span>
                      <?php }elseif($data->tgl_pengembalian!="0000-00-00"){?>
                        <span class="badge badge-pill badge-info">Sudah Kembali</span>                      
                      <?php }?>
                   </td>
                   <td><?php echo $data->kode_peminjaman?></td>
                   <td>
                   <?php if($data->tgl_pengembalian=="0000-00-00" || $data->tgl_pengembalian==null){?>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal<?= $data->kode_peminjaman?>">
                      Pengembalian
                    </button>
                   <?php }else{?>
                    <button type="button" class="btn btn-primary  disabled" data-toggle="modal" data-target="">
                      Pengembalian
                    </button>
                   <?php }?>
                   </td>
               </tr>
               <?php endforeach?>
               </tbody>
             </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.
      /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
    <!-- The Modal pengembalian -->
    <?php foreach($peminjaman as $data):?>
    <div class="modal" id="myModal<?= $data->kode_peminjaman?>">
      <div class="modal-dialog">
        <div class="modal-content">
          <!-- Modal Header -->
          <div class="modal-header">
            <h4 class="modal-title">Pengembalian</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>

          <!-- Modal body -->
          <div class="modal-body">
            Apakah Peminjaman dengan kode <?= $data->kode_peminjaman ?> sudah selesai.?
          </div>

          <!-- Modal footer -->
          <div class="modal-footer">
            <a href='<?php echo site_url("admin/peminjaman/selesai_peminjaman_alat/$data->kode_peminjaman")?>' class="btn btn-info" role="button">Sudah</a>
          </div>

        </div>
      </div>
    </div>
    <?php endforeach?>
    <!-- The Modal pengembalian -->

 <?php 
    $this->load->view('admin/partials/footer.php');
  ?>

 
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo site_url('assets/adminlte/bower_components/jquery/dist/jquery.min.js')?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo site_url('assets/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js')?>"></script>
<!-- DataTables -->
<script src="<?php echo site_url('assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo site_url('assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
<!-- Slimscroll -->
<script src="<?php echo site_url('assets/adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')?>"></script>
<!-- FastClick -->
<script src="<?php echo site_url('assets/adminlte/bower_components/fastclick/lib/fastclick.js')?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo site_url('assets/adminlte/dist/js/adminlte.min.js')?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo site_url('assets/adminlte/dist/js/pages/dashboard.js')?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo site_url('assets/adminlte/dist/js/demo.js')?>"></script>

<script>
   $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>
