<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin SIPOLAR</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <?php $this->load->view('admin/partials/upper-section.php'); ?>
  
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php
    
      $this->load->view("admin/partials/header.php");
      $this->load->view("admin/partials/nav-sidebar.php");
  ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Peminjaman Alat
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Peminjaman Komputer</li>
      </ol>
    </section>
      

        <section class="content">

               <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Peminjam Masuk</h3>
                <!-- alert -->
                
                <!-- <div class="alert alert-success alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                  <strong> </strong>
                </div> -->
                <!-- alert -->
            </div>

            
            
            <div class="box-body">
              <div class="col">
                  <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#pinjamModal">
                    <i class="fa fa-plus"></i> Tambah Data
                  </button> -->
                </div>
                <br>
              <table id="example1" class="table table-bordered table-hover">
                
                 <thead>

                <tr>
                    <th>No</th>
                    <th>Nim</th>
                    <th>Nama</th>
                    <th>Nama Alat</th>
                    <th>Tanggal Pinjam</th>
                    <th>Lama Pinjam</th>
                    <th>Tujuan Pinjam</th>
                    <th>Option</th>
                </tr>
                </thead>
                
               <tbody>
               <?php  $no=0; foreach($peminjaman as $data): $no++?>
                <tr>
                    <td><?php echo $no?></td>
                    <td><?php echo $data->nim_mahasiswa?></td>
                    <td><?php echo $data->nama_mahasiswa?></td>
                    <td><?php echo $data->nama_barang?></td>
                    <td><?php echo $data->tgl_pinjam?></td>
                    <td><?php echo $data->lama_peminjaman." Hari"?></td>
                    <td><?php echo $data->tujuan?></td>
                    <td>
                      <a href="<?php echo site_url('admin/peminjaman/terima_peminjaman_alat/'.$data->kode_peminjaman)?>" class="btn btn-sm btn-info pull-right" style="margin-right: 10px;">
                        Terima
                      </a>
                      <a data-toggle="modal" data-target="#myModal<?php echo $data->kode_peminjaman?>" class="btn btn-sm btn-danger pull-right" style="margin-right: 10px;">
                        Tolak
                      </a>
                    </td>
                </tr>
                <?php endforeach?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <?php foreach($peminjaman as $data):?>
  <div class="modal fade" id="myModal<?php echo $data->kode_peminjaman?>" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body">
        <h2>PERMINTAAN DITOLAK</h2>
            <form action="<?php echo site_url('admin/peminjaman/permintaan_ditolak_alat')?>" method="POST">
              <div class="form-group">
                <label for="comment">Alasan Penolakan :</label>
                <textarea class="form-control" rows="5" id="comment" name="xalasan" required></textarea>
              </div>
              <div class="form-group" hidden>
                <label for="comment">Kode:</label>
                <input type="text" name="xkode" value="<?php echo $data->kode_peminjaman;?>">
              </div>
        </div>
        <div class="modal-footer">
              <button type="submit" class="btn btn-primary">Kirim</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
        </div>
        </form>
      </div>
      
    </div>
  </div>
  <?php endforeach?>
  <!-- /.content-wrapper -->
 <?php 
    $this->load->view('admin/partials/footer.php');
  ?>

 
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo site_url('assets/adminlte/bower_components/jquery/dist/jquery.min.js')?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo site_url('assets/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js')?>"></script>
<!-- DataTables -->
<script src="<?php echo site_url('assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo site_url('assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
<!-- Slimscroll -->
<script src="<?php echo site_url('assets/adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')?>"></script>
<!-- FastClick -->
<script src="<?php echo site_url('assets/adminlte/bower_components/fastclick/lib/fastclick.js')?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo site_url('assets/adminlte/dist/js/adminlte.min.js')?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo site_url('assets/adminlte/dist/js/pages/dashboard.js')?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo site_url('assets/adminlte/dist/js/demo.js')?>"></script>

<script>
   $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>
