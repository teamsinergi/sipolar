<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin SIPOLAR</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

<?php $this->load->view('admin/partials/upper-section.php'); ?>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php
    
      $this->load->view("admin/partials/header.php");
      $this->load->view("admin/partials/nav-sidebar.php");
  ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Detail Peminjaman Ruang
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>


 
    <section class="content">
       <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-warning"></i>

              <h3 class="box-title">Jadwal Peminjam Tanggal : <?php echo $tanggal;?> </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-hover">
                <thead>
                <tr>
                    <th>Nama</th>
                    <th>Ruangan</th>
                    <th>Tanggal Pinjam</th>
                    <th>Jam Mulai</th>
                    <th>Jam Selesei</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($terboking as $data): ?>
                <tr>
                    <td><?php echo $data->nama_mahasiswa;?></td>
                    <td><?php echo $data->nama_ruang;?></td>
                    <td><?php echo $data->tanggal;?></td>
                    <td><?php echo $data->mulai;?></td>
                    <td><?php echo $data->selesai;?></td>
                    
                </tr>
                <?php endforeach?>
                </tbody>
            </table>
            </div>
            <!-- /.box-body -->
          </div>

      <!-- Detail peminjam -->
          <div class="box box-info">

            <div class="box-header with-border">
              <h3 class="box-title">Detail Peminjam</h3>
              <br>

              <?php foreach($detail as $data): ?>
                  <h5> <b>NIM &emsp;  : <?php echo $data->nim;?> </b> </h5> 
              
                  <h5> <b>Nama &nbsp; : <?php echo $data->nama_mahasiswa;?> </b> </h5> 

            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>Ruangan</th>
                    <th>Tanggal Pinjam</th>
                    <th>Durasi Pinjam</th>
                    <th>Keterangan</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                      <td><?php echo $data->nama_ruang;?></td>
                     <td><?php echo $data->tanggal;?></td>
                    <td><span class="label label-success">
                        <?php echo $data->mulai;?> - <?php echo $data->selesai;?></span></td>
                    <td><?php echo $data->keterangan;?></td>
                  </tr>

                
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">

              <a href="<?php echo site_url('admin/peminjaman/terima_peminjaman/'.$data->kode)?>" class="btn btn-sm btn-info pull-right" style="margin-right: 10px;">
                Terima
              </a>

              <a data-toggle="modal" data-target="#myModal" class="btn btn-sm btn-danger pull-right" style="margin-right: 10px;">
                Tolak
              </a>
            </div>

            <?php endforeach?>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body">
        <h2>PERMINTAAN DITOLAK</h2>
            <form action="<?php echo site_url('admin/peminjaman/permintaan_ditolak')?>" method="POST">
              <div class="form-group">
                <label for="comment">Alasan Penolakan :</label>
                <textarea class="form-control" rows="5" id="comment" name="xalasan" required></textarea>
              </div>
              <div class="form-group" hidden>
                <label for="comment">Kode:</label>
                <input type="text" name="xkode" value="<?php echo $kode;?>">
              </div>
        </div>
        <div class="modal-footer">
          
              <button type="submit" class="btn btn-primary">Kirim</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
        </div>
        </form>
      </div>
      
    </div>
  </div>
  
  
  <!-- /.content-wrapper -->
 <?php 
    $this->load->view('admin/partials/footer.php');
  ?>

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo site_url('assets/adminlte/bower_components/jquery/dist/jquery.min.js')?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo site_url('assets/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js')?>"></script>
<!-- DataTables -->
<script src="<?php echo site_url('assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo site_url('assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
<!-- Slimscroll -->
<script src="<?php echo site_url('assets/adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')?>"></script>
<!-- FastClick -->
<script src="<?php echo site_url('assets/adminlte/bower_components/fastclick/lib/fastclick.js')?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo site_url('assets/adminlte/dist/js/adminlte.min.js')?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo site_url('assets/adminlte/dist/js/pages/dashboard.js')?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo site_url('assets/adminlte/dist/js/demo.js')?>"></script>

<script>
   $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>
