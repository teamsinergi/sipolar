<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin SIPOLAR</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <?php $this->load->view('admin/partials/upper-section.php'); ?>
  
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php
    
      $this->load->view("admin/partials/header.php");
      $this->load->view("admin/partials/nav-sidebar.php");
  ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Peminjaman Ruang
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Peminjaman Ruangan</li>
      </ol>
    </section>
      

        <section class="content">

               <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Peminjam Masuk</h3>
                <!-- alert -->
                <?php $pesan = $this->session->flashdata('pesan'); 
                if($pesan){
                ?>
                <div class="alert alert-success alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                  <strong><?php echo $pesan?></strong>
                </div>
                <?php }?>
                <!-- alert -->
            </div>

            
            
            <div class="box-body">
              <div class="col">
                  <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#pinjamModal">
  <i class="fa fa-plus"></i> Tambah Data
</button> -->
                </div>
                <br>
              <table id="example1" class="table table-bordered table-hover">
                
                 <thead>

                <tr>
                    <th>Nim</th>
                    <th>Nama</th>
                    <th>Ruangan</th>
                    <th>Tanggal Peminjam</th>
                    <th>Jam Mulai</th>
                    <th>Jam Selesei</th>
                    <th>Tujuan Peminjam</th>
                    <th>Option</th>
                </tr>
                </thead>
                
               <tbody>
                <?php foreach($peminjaman as $data): ?>
                <tr>
                    <td><?php echo $data->nim;?></td>
                    <td><?php echo $data->nama_mahasiswa;?></td>
                    <td><?php echo $data->nama_ruang;?></td>
                    <td><?php echo $data->tanggal;?></td>
                    <td><?php echo $data->mulai;?></td>
                    <td><?php echo $data->selesai;?></td>
                    <td><?php echo $data->keterangan;?></td>
                    <td>
                        <a href="<?php echo site_url('admin/peminjaman/detail/'.$data->kode)?>" class="btn btn-primary">Detail</a>
                    </td>
                </tr>
                <?php endforeach?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  
  <div class="modal fade" id="pinjamModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
  <!-- /.content-wrapper -->
 <?php 
    $this->load->view('admin/partials/footer.php');
  ?>

 
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo site_url('assets/adminlte/bower_components/jquery/dist/jquery.min.js')?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo site_url('assets/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js')?>"></script>
<!-- DataTables -->
<script src="<?php echo site_url('assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo site_url('assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
<!-- Slimscroll -->
<script src="<?php echo site_url('assets/adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')?>"></script>
<!-- FastClick -->
<script src="<?php echo site_url('assets/adminlte/bower_components/fastclick/lib/fastclick.js')?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo site_url('assets/adminlte/dist/js/adminlte.min.js')?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo site_url('assets/adminlte/dist/js/pages/dashboard.js')?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo site_url('assets/adminlte/dist/js/demo.js')?>"></script>

<script>
   $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>
