<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin SIPOLAR</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
 
  <?php $this->load->view('admin/partials/upper-section.php'); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php
    
      $this->load->view("admin/partials/header.php");
      $this->load->view("admin/partials/nav-sidebar.php");
  ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Peminjaman Ruang
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Peminjaman Ruangan</li>
      </ol>
    </section>
      

        <section class="content">

               <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Peminjam Masuk</h3>
            </div>

            
            
            <div class="box-body">
              <div class="col">
                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#pinjamModal">
  <i class="fa fa-plus"></i> Tambah Data
</button>
                </div>
                                <!-- alert -->
                                <?php $pesan = $this->session->flashdata('pesan'); 
                if($pesan){
                ?>
                <div class="alert alert-success alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                  <strong><?php echo $pesan?></strong>
                </div>
                <?php }?>
                <!-- alert -->
                <br>
              <table id="example1" class="table table-bordered table-hover">
                
                 <thead>

                <tr>
                    <th>No</th>
                    <th>Nama Lab</th>
                    <th>Kapasitas</th>
                    <th>option</th>
                </tr>
                </thead>
                
               <tbody>
                <?php $no=0; foreach($ruang as $data): $no++?>
                <tr>
                    <td><?php echo $no?></td>
                    <td><?php echo $data->nama_ruang;?></td>
                    <td><?php echo $data->kapasitas;?></td>
                    <td>
                         
                    <a data-toggle="modal" data-target="#ModalEdit<?php echo $data->id_ruang?>" class="btn btn-small"><i class="fa fa-edit" style="color: #3C8DBC; font-size: 20px; font-weight: bold;"></i> </a>
                          <a data-toggle="modal" data-target="#ModalHapus<?php echo $data->id_ruang?>" class="btn btn-small"><i class="fa fa-trash" style="color: #D73925; font-size: 20px; font-weight: bold;"></i> </a>
                    </td>
                </tr>
                <?php endforeach?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.
      /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <form action="<?php echo site_url('admin/ruang/simpan_ruang')?>" method="POST">  
  <div class="modal fade" id="pinjamModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">TAMBAH RUANGAN</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      
            <div class="form-group">
                <label for="usr">Nama Lab:</label>
                <input type="text" class="form-control" id="usr" name="xnama">
            </div>
            <div class="form-group">
                <label for="usr">Kapasistas:</label>
                <input type="text" class="form-control" id="usr" name="xkapasitas">
            </div>
       
       </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
    </div>
  </div>
</div>
</form>
  <!-- /.content-wrapper -->
  <?php 
    $this->load->view('admin/partials/footer.php');
  ?>

  <!-- ============================= HAPUS ========================= -->
  <?php foreach($ruang as $data):?>
  <div class="modal fade" id="ModalHapus<?php echo $data->id_ruang;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Hapus Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?php echo site_url('admin/ruang/hapus_ruang/'.$data->id_ruang)?>" metdod="POST">
      <div class="modal-body">
        <div class="container">
          <h3>Apakah anda yakin menghapus <strong><?php echo $data->nama_ruang?></strong> .?</h3>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-danger">Hapus</button>
      </div>
      </form>
    </div>
  </div>
</div>
<?php endforeach?>
 <!-- =================================================================================== -->
 <!-- ============================= Edit ========================= -->
 <?php foreach($ruang as $data):?>
  <form action="<?php echo site_url('admin/ruang/edit_ruang')?>" method="POST">
  <div class="modal fade" id="ModalEdit<?php echo $data->id_ruang?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <div class="form-group">
                <label for="usr">Name:</label>
                <input type="text" class="form-control" id="usr" name="xnama" value="<?php echo $data->nama_ruang?>">
            </div>
            <div class="form-group">
                <label for="usr">Kapasistas:</label>
                <input type="text" class="form-control" id="usr" name="xkapasitas" value="<?php echo $data->kapasitas?>">
            </div>
            <div class="form-group" hidden>
                <label for="usr">id:</label>
                <input type="text" class="form-control" id="usr" name="xid" value="<?php echo $data->id_ruang?>">
            </div>
       </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Update</button>
      </div>
    </div>
  </div>
</div>
</form>
<?php endforeach?>





<!-- jQuery 3 -->
<script src="<?php echo site_url('assets/adminlte/bower_components/jquery/dist/jquery.min.js')?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo site_url('assets/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js')?>"></script>
<!-- DataTables -->
<script src="<?php echo site_url('assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo site_url('assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
<!-- Slimscroll -->
<script src="<?php echo site_url('assets/adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')?>"></script>
<!-- FastClick -->
<script src="<?php echo site_url('assets/adminlte/bower_components/fastclick/lib/fastclick.js')?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo site_url('assets/adminlte/dist/js/adminlte.min.js')?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo site_url('assets/adminlte/dist/js/pages/dashboard.js')?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo site_url('assets/adminlte/dist/js/demo.js')?>"></script>

<script>
   $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>