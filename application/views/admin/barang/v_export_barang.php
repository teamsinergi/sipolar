<style type="text/css">

    table {

       border-collapse: collapse;
       margin: 0 auto;

    }

    #exmaple1 table, th {

       border: 1px solid black;
       text-align: center;
       padding: 6px;
       font-size: 15px;

    }

    #exmaple1 table, td {

       border: 1px solid black;
       text-align: center;
       padding: 6px;
       font-size: 14px;

    }
</style>


<h1 style="text-align: center;"><?php echo $namalab?></h1>

<table id="example1" class="table table-bordered table-hover">
                 <thead>
                <tr>
                    <th>No</th>
                    <th>Laboratorium</th>
                    <th>Nama barang</th>
                    <th>Banyak barang</th>
                    <th>Kepemilikan</th>
                    <th>Kondisi</th>
                    <th>keterangan</th>
                    <th>Tanggal Pengadaan</th>
                    <th>Cek Terakhir</th>

                </tr>
                </thead>
                <?php $no=0; foreach($barang as $data): $no++?>
                    <tr>
                        <td><?php echo $no?></td>
                        <td><?php echo $data->nama_ruang?></td>
                        <td><?php echo $data->nama_barang?></td>
                        <td><?php echo $data->banyak_barang?></td>
                        <td><?php echo $data->kepemilikan?></td>
                        <td><?php echo $data->kondisi?></td>
                        <td><?php echo $data->keterangan?></td>
                        <td><?php echo $data->tanggal?></td>
                        <td><?php echo $data->tanggal_cek?></td>
                    </tr>
                <?php endforeach?>
                <tbody>
                </tbody>
              </table>