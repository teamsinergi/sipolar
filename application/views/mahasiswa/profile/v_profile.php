<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/js/custom.min.js'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/vendor/fontawesome-free/css/all.min.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/custom.css'); ?> ">
<link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

<body id="page-top">

  <?php
    if(!$this->session->userdata('email_mahasiswa')){
      $this->load->view("mahasiswa/partials/navbar-unlogged.php");
    }else{
      $this->load->view("mahasiswa/partials/navbar-logged.php");
    }

  ?>
  <!-- Header -->
  <div class="body-profile">
    <div class="container d-flex h-100 align-items-center">
        <div class="container" style="color:white;">
<br>
  <!-- alert -->
  <?php $pesan = $this->session->flashdata('pesan'); 
  if($pesan){
  ?>
  <div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong><?php echo $pesan?></strong>
  </div>
  <?php }?>
  <!-- alert -->
          <?php foreach($profile as $data):?>
          <div class="profile-box">

            <div class="col-md-7"> <h1>Profil Pengguna</h1></div>
              <form action="<?php echo site_url('mahasiswa/peminjaman/simpan_pinjam')?>" method="POST">
                <div class="row"> 
                  <div class="col" >
                      <label for="usr">Nama</label>
                      <h3 style="color: #111;"><?php echo $data->nama_mahasiswa;?></h3>
                  </div>
                </div>

                <div class="row">
                  <div class="col">
                      <label for="pwd">NIM/NIP</label>
                      <h5 style="color: #111;"><?php echo $data->nim;?></h5>
                  </div>
                  </div>

                  <div class="row">

                    <div class="col-md-4">
                      <label for="sel1">Prodi</label>
                      <h3 style="color: #111;">Teknik Informatika</h3>
                      </div>

                    <div class="col">    
                      <label for="sel1">Fakultas</label>
                      <h3 style="color: #111;">Fakultas Teknologi Industri</h3>
                      </div> 
                  </div> 
                  <div class="col" id="p-em-box">               
                  
                    <div class="col-md-4">
                      <label for="sel1">No. Telepon</label>
                      <h4 style="color: #111;"><?php echo $data->no_hp;?></h4>
                      </div>
                  

                    <div class="col">    
                      <label for="sel1">Alamat E-mail</label>
                      <h4 style="color: #111;"><?php echo $data->email_mahasiswa;?></h4>
                      </div> 
                    </div>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                     Edit Profil
                    </button>

              </form>
            </div>
                <?php endforeach?>
                   
        </div>
    </div>
    <div class="container">
      <div class="modal" id="myModal">
        <div class="modal-dialog">
          <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
              <h4 class="modal-title">Sunting Profil</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
            <form action="<?php echo site_url('mahasiswa/profile/update_profile')?>" method="POST">
            <?php foreach($profile as $data):?>
              <div class="form-group">
                <label for="usr">Name:</label>
                <input type="text" class="form-control" id="usr" name="xnama" value="<?php echo $data->nama_mahasiswa?>">
              </div>
              <div class="form-group">
                <label for="usr">Nim:</label>
                <input type="text" class="form-control" id="usr" name="xnim" value="<?php echo $data->nim?>" readonly>
              </div>
              <div class="form-group">
                <label for="usr">Nomer hp:</label>
                <input type="text" class="form-control" id="usr" name="xnomer" value="<?php echo $data->no_hp?>">
              </div>
              
            <?php endforeach?>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  <script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js')?>"></script>
  <script src="<?php echo base_url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')?>"></script>

  <!-- Plugin JavaScript -->
  <script src="<?php echo base_url('assets/vendor/jquery-easing/jquery.easing.min.js')?>"></script>

  <!-- Custom scripts for this template -->
  <script src="<?php echo base_url('assets/js/grayscale.min.js')?>"></script>

</body>
</html>