<!DOCTYPE html>
<html lang="en">
<head>
  <title>Sipolar</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/vendor/fontawesome-free/css/all.min.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/custom.css'); ?> ">
<link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <?php 
        $flash_msg = $this->session->flashdata('flash_msg');
    ?>
</head>
<body class="body-login-form">

<div class="container">
<div class="row">

<?php if($flash_msg):?>
                                <div class="alert alert-danger alert-dismissible fade show">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <strong><?php echo $flash_msg?></strong>
                                </div>
                            <?php endif ?>

    <div class="col-md-10 offest=md-1" style="margin: 0 auto">
        <div class="row" >
            <div class="col-md-5 register-left">
             <a href="<?php echo site_url('home')?>">
                <img src="<?php echo site_url('assets/img/logo wh.png') ?>"></a>   
                
                <p>Sistem Pengelolaan Laboratorium </p>

                                <h6 class="">Suda punya akun.?</h2>
              <a href="<?php echo base_url('mahasiswa/login');?>"><button type="button" class="btn btn-primary">Masuk</button> </a>  
            </div>

            <div class="col-md-7 login-right">
                <h3>Daftar Akun</h3>
                <div class="register-form">
                 <form action="<?php echo site_url('mahasiswa/login/daftar_akun')?>" method="POST">

                                <div class="form-group">
                                    <label for="nama">Nama</label>
                                    <input type="text" class="form-control" id="email" name='xnama'>
                                </div>
                                <div class="form-group">
                                    <label for="nim">Nim</label>
                                    <input type="text" class="form-control" id="email" name='xnim'>
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" id="email" name='xemail'>
                                </div>
                                <div class="form-group">
                                    <label for="pwd">Password:</label>
                                    <input type="password" class="form-control" id="pwd" name='xpassword'>
                                </div>
                                <button type="submit" class="btn btn-primary">Masuk</button>
                            </form>
                </div>
                
            </div>
        </div>
    </div>
</div>
</div>
<br>
<br>
<!--     <div class="row">
        <div class='col-6'>
            <div class="container">
                <h2>Masuk</h2>
                <div class="card">
                    <div class="card-header">Masuk</div>
                    <div class="card-body">
                        <div>
                            <?php if($flash_msg):?>
                                <div class="alert alert-danger alert-dismissible fade show">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <strong><?php echo $flash_msg?></strong>
                                </div>
                            <?php endif ?>
                        </div>
                        <div>
                            <form action="<?php echo site_url('mahasiswa/login/masuk')?>" method="POST">
                                <div class="form-group">
                                    <label for="email">Email address:</label>
                                    <input type="email" class="form-control" id="email" name='xemail'>
                                </div>
                                <div class="form-group">
                                    <label for="pwd">Password:</label>
                                    <input type="password" class="form-control" id="pwd" name='xpassword'>
                                </div>
                                <button type="submit" class="btn btn-primary">Masuk</button>
                            </form>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
        
    </div> -->
   <!--  <div class='col-6'>
            <div class="container">
                <h2>Daftar akun</h2>
                <div class="card">
                    <div class="card-header">Daftar akun</div>
                    <div class="card-body">
                        <div>
                            <form action="<?php echo site_url('mahasiswa/login/daftar_akun')?>" method="POST">
                                <div class="form-group">
                                    <label for="email">Nama:</label>
                                    <input type="text" class="form-control" id="nama" name="xnama">
                                </div>
                                <div class="form-group">
                                    <label for="email">Nim</label>
                                    <input type="text" class="form-control" id="nip/nik" name="xnim">
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" id="email" name="xemail">
                                </div>
                                <div class="form-group">
                                    <label for="pwd">Password:</label>
                                    <input type="password" class="form-control" id="pwd" name="xpassword">
                                </div>
                                <button type="submit" class="btn btn-primary">Daftar</button>
                            </form>
                        </div>
                    </div> 
                </div>
            </div>
        </div> -->


</body>

</html>
