<!DOCTYPE html>
<html>
<head>
  <title></title>

 
</head>


<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/js/custom.min.js'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/vendor/fontawesome-free/css/all.min.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/custom.css'); ?> ">
<link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

<body id="page-top">


  <?php
    if(!$this->session->userdata('email_mahasiswa')){
      $this->load->view("mahasiswa/partials/navbar-unlogged.php");
    }else{
      $this->load->view("mahasiswa/partials/navbar-logged.php");
    }

  ?>
  
  <!-- Header -->
  <div class="body-history-table">
    <div class="container d-flex h-100 align-items-center">
        <div class="container" style="color:white;">

  <!-- alert -->
  <?php $pesan = $this->session->flashdata('pesan'); 
  if($pesan){
  ?>
  <div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong><?php echo $pesan?></strong>
  </div>
  <?php }?>
  <!-- alert -->
<div class="tbl-riwayat">
  <div class="col-md-7">
  <h1>Riwayat Peminjaman Ruang</h1></div>
<table id="riwayat-mhs" class="table">
  <thead style="text-align: center;">
                   
                   <th>No</th>
                    <th>Ruangan</th>
                     <th>Tanggal Pinjam</th>
                     <th>Jam Mulai - Selesai</th>
                      <th>Tujuan Peminjaman</th>
                      <th>status</th>
                       <th>Aksi</th>
                   </thead>
    <tbody>
    <?php $no=0; foreach($riwayat as $data): $no++?>
    <tr style="text-align: center;">
    <td><?php echo $no?></td>
    <td><?php echo $data->nama_ruang;?></td>
    <td><?php echo $data->tgl_pinjam;?></td>
    <td><?php echo $data->jam_mulai." - ".$data->jam_selesai;?></td>
    <td><?php echo $data->keterangan;?></td>
    <td><?php echo $data->status;?></td>
    <td>
        <?php if($data->id_status == 2){?>
          <a href="<?php echo site_url('mahasiswa/peminjaman/cetak_surat/'.$data->kode)?>" class="btn btn-success"><span class="fa fa-print"></span> Cetak </a>

        <?php }elseif ($data->id_status == 3 ) { ?>
          <a href="#" class="btn btn-info" data-toggle="modal" data-target="#myModal<?php echo $data->kode_peminjaman?>"><span class="fa fa-info-circle"></span> Detail</a>

        <?php }else{?>
          <a style="color: grey; font-weight: bold;">Belum Di Proses</a>
        <?php }?>
    </td>
    </tr>
    <?php endforeach?>
    </tbody>
</table>
</div>

</div>
    </div>

  </div>

  <?php foreach($riwayat as $data):?>
  <div class="container">
      <div class="modal" id="myModal<?php echo $data->kode_peminjaman?>">
        <div class="modal-dialog">
          <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
              <h4 class="modal-title">Alasan Ditolak:</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
              <?php echo $data->alasan?>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
            </form>
              <button type="button" class="btn btn-danger" data-dismiss="modal">Kembali</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php endforeach?>

<script type="text/javascript">
    $(document).ready(function() {
    $('#example').DataTable();
} );
</script>
  <script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js')?>"></script>
  <script src="<?php echo base_url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')?>"></script>

  <!-- Plugin JavaScript -->
  <script src="<?php echo base_url('assets/vendor/jquery-easing/jquery.easing.min.js')?>"></script>

  <!-- Custom scripts for this template -->
  <script src="<?php echo base_url('assets/js/grayscale.min.js')?>"></script>

  <!-- DataTables -->
<script src="<?php echo site_url('assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo site_url('assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>

<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

<script>
   $(function () {
    $('#riwayat-mhs').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
    
  });
</script>
</body>
</html>