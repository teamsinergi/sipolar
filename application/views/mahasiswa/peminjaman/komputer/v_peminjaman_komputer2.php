<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/js/custom.min.js'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/vendor/fontawesome-free/css/all.min.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/custom.css'); ?> ">
<link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

<body id="page-top">

  <?php
    if(!$this->session->userdata('email_mahasiswa')){
      $this->load->view("mahasiswa/partials/navbar-unlogged.php");
    }else{
      $this->load->view("mahasiswa/partials/navbar-logged.php");
    }

  ?>

  <!-- Header -->
  <div class="body-rent-form">
    <div class="container d-flex h-100 align-items-center">
        <div class="container" style="color:white;">
<br>
          <div class="register-form" style="width: 40rem; margin: 0 auto;">

            <div class="col-md-7"><h1>Peminjaman Ruang</h1></div>
                  <h1>ini kursi komputer</h1>
<div class="form-group">
    <form action="<?php echo site_url('mahasiswa/peminjaman/cek_komputer')?>" method="POST">
        <label for="usr">cek tanggal:</label>
        <input type="date" class="form-control" id="usr" name="xtanggal" onchange="this.form.submit();">
    </form>  
</div>
    <div  class="row">

      <!-- ========== POJOK KIRI ============-->
      <div id="" class="mx-auto" >
            <div  class="col">
                    <button type="button" class="btn btn-primary" style="margin-bottom:10px;" data-toggle="modal" data-target="#pinkomModal" disabled>A1</button>
                    <button type="button" class="btn btn-primary" style="margin-bottom:10px;" data-toggle="modal" data-target="#pinkomModal">A2</button>
            </div>

            <div  class="col">
                    <button type="button" class="btn btn-primary" style="margin-bottom:10px;" data-toggle="modal" data-target="#pinkomModal">A3</button>
                    <button type="button" class="btn btn-primary" style="margin-bottom:10px;" data-toggle="modal" data-target="#pinkomModal">A4</button>
            </div>

            <div  class="col">
                    <button type="button" class="btn btn-primary" style="margin-bottom:10px;" data-toggle="modal" data-target="#pinkomModal">A5</button>
                    <button type="button" class="btn btn-primary" style="margin-bottom:10px;" data-toggle="modal" data-target="#pinkomModal">A6</button>
            </div>
      </div>
      <!-- ========== BATAS POJOK KIRI ============-->

          <div id="" class="mx-auto">
            <div class="col">
                    <button type="button" class="btn btn-primary" style="margin-bottom:10px;" data-toggle="modal" data-target="#pinkomModal">A7</button>
                    <button type="button" class="btn btn-primary" style="margin-bottom:10px;" data-toggle="modal" data-target="#myModal>">A8</button>
                    <button type="button" class="btn btn-primary" style="margin-bottom:10px;" data-toggle="modal" data-target="#myModal>">A9</button>
            </div>

            <div class="col">
                    <button type="button" class="btn btn-primary" style="margin-bottom:10px;" data-toggle="modal" data-target="#pinkomModal">A10</button>
                    <button type="button" class="btn btn-primary" style="margin-bottom:10px;" data-toggle="modal" data-target="#pinkomModal">A11</button>
                    <button type="button" class="btn btn-primary" style="margin-bottom:10px;" data-toggle="modal" data-target="#pinkomModal">A12</button>
            </div>

            <div class="col">
                    <button type="button" class="btn btn-primary" style="margin-bottom:10px;"data-toggle="modal" data-target="#pinkomModal">A12</button>
                    <button type="button" class="btn btn-primary" style="margin-bottom:10px;" data-toggle="modal" data-target="#pinkomModal">A13</button>
                    <button type="button" class="btn btn-primary" style="margin-bottom:10px;" data-toggle="modal" data-target="#pinkomModal">A14</button>
            </div>
        </div>

        <div id="" class="mx-auto">
           <div class="col">
                    <button type="button" class="btn btn-primary " style="margin-bottom:10px;" data-toggle="modal" data-target="#pinkomModal">A15</button>
                    <button type="button" class="btn btn-primary " style="margin-bottom:10px;" data-toggle="modal" data-target="#pinkomModal">A16</button>
            </div>

            <div class="col">
                    <button type="button" class="btn btn-primary " style="margin-bottom:10px; " data-toggle="modal" data-target="#pinkomModal">A17</button>
                    <button type="button" class="btn btn-primary " style="margin-bottom:10px;" data-toggle="modal" data-target="#pinkomModal">A18</button>
            </div>

            <div class="col">
                    <button type="button" class="btn btn-primary " style="margin-bottom:10px;" data-toggle="modal" data-target="#pinkomModal">A19</button>
                    <button type="button" class="btn btn-primary" style="margin-bottom:10px;" data-toggle="modal" data-target="#pinkomModal">A20</button>
            </div>
        </div>
  
                </div>
        </div>
      
    </div>


  </div>

  
<!-- Modal -->
<div class="modal fade" id="pinkomModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
    
    



  <script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js')?>"></script>
  <script src="<?php echo base_url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')?>"></script>

  <!-- Plugin JavaScript -->
  <script src="<?php echo base_url('assets/vendor/jquery-easing/jquery.easing.min.js')?>"></script>

  <!-- Custom scripts for this template -->
  <script src="<?php echo base_url('assets/js/grayscale.min.js')?>"></script>

</body>
</html>