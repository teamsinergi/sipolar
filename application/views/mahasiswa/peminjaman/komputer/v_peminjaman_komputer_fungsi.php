<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/js/custom.min.js'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/vendor/fontawesome-free/css/all.min.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/custom.css'); ?> ">
<link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

<!-- ini buat algoritma semuanya  -->
<?php 
  $tempkomputerterpinjam = array();
  foreach($komputerterpinjam as $kunci){
	  array_push($tempkomputerterpinjam, $kunci->kode_komputer);    
  }
  	$biarjadisatu = array();
  	$simpansemua_1 = array(); // ini variable untuk simpan semua yang terboking atau belum untuk nanti jaga jaga
  	$simpansemua_2 = array(); // ini variable untuk simpan semua yang terboking atau belum untuk nanti jaga jaga
  	$simpansemua_3 = array(); // ini variable untuk simpan semua yang terboking atau belum untuk nanti jaga jaga
    
    // ini untuk algortima komputer kiri (buka)
		foreach($komputer_1 as $data):
			if($komputerterpinjam == null){

			}else{
				foreach($komputerterpinjam as $key){
          if(in_array($data->kode_komputer, $tempkomputerterpinjam)){
            if(in_array($data->kode_komputer, $biarjadisatu)){
              
            }else{
              array_push($biarjadisatu, $data->kode_komputer);
              array_push($simpansemua_1, 0);
            }
          }else{
            if(in_array($data->kode_komputer, $biarjadisatu)){
              
            }else{
              array_push($biarjadisatu, $data->kode_komputer);
              array_push($simpansemua_1, $data->kode_komputer);
            }
          }
				}
			}
    endforeach
	  // ini untuk algortima komputer kiri (tutup)
?>
<?php 
    // ini untuk algortima komputer tengah (buka)
		foreach($komputer_2 as $data):
			if($komputerterpinjam == null){

			}else{
				foreach($komputerterpinjam as $key){
          if(in_array($data->kode_komputer, $tempkomputerterpinjam)){
            if(in_array($data->kode_komputer, $biarjadisatu)){
              
            }else{
              array_push($biarjadisatu, $data->kode_komputer);
              array_push($simpansemua_2, 0);
            }
          }else{
            if(in_array($data->kode_komputer, $biarjadisatu)){
              
            }else{
              array_push($biarjadisatu, $data->kode_komputer);
              array_push($simpansemua_2, $data->kode_komputer);
            }
          }
				}
			}
    endforeach
	  // ini untuk algortima komputer tengah (tutup)
?>
<?php 
    // ini untuk algortima komputer tengah (buka)
		foreach($komputer_3 as $data):
			if($komputerterpinjam == null){

			}else{
				foreach($komputerterpinjam as $key){
          if(in_array($data->kode_komputer, $tempkomputerterpinjam)){
            if(in_array($data->kode_komputer, $biarjadisatu)){
              
            }else{
              array_push($biarjadisatu, $data->kode_komputer);
              array_push($simpansemua_3, 0);
            }
          }else{
            if(in_array($data->kode_komputer, $biarjadisatu)){
              
            }else{
              array_push($biarjadisatu, $data->kode_komputer);
              array_push($simpansemua_3, $data->kode_komputer);
            }
          }
				}
			}
    endforeach
	  // ini untuk algortima komputer tengah (tutup)
?>

<!-- ini buat algoritma semuanya  -->

<body id="page-top">

  <?php
    if(!$this->session->userdata('email_mahasiswa')){
      $this->load->view("mahasiswa/partials/navbar-unlogged.php");
    }else{
      $this->load->view("mahasiswa/partials/navbar-logged.php");
    }

  ?>

  <!-- Header -->
  <div class="body-rent-form">
    <div class="container d-flex h-100 align-items-center">
        <div class="container" style="color:white;">
<br>
          <div class="register-form" style="width: 40rem; margin: 0 auto;">

            <div class="col-md-12"><h1>Peminjaman Komputer</h1></div>
<div class="form-group">
    <form action="<?php echo site_url('mahasiswa/peminjaman/cek_komputer_fungsi')?>" method="POST">
      <div class="form-group">
        <label for="sel1">Ruangan / Laboratorium :</label>
        <select class="form-control" id="sel1" name="xlab" onchange="this.form.submit();">
          <option value="<?php echo $idlab?>"><?php echo $nama_lab?></option>      
          <?php foreach($ruang as $key):?>
            <option value="<?php echo $key->id_ruang?>"><?php echo $key->nama_ruang?></option>
          <?php endforeach?>
        </select>
      </div>
      <div class="form-group">
          <label for="usr">Tanggal Peminjaman:</label>
          <input type="date" class="form-control" id="usr" name="xtanggal" value="<?php echo $tanggal?>" onchange="this.form.submit();">
      </div>
    </form>  
</div>
    <div  class="row"> 
      <?php if($komputerterpinjam == null){?>
              <!-- ========== List komputer ============-->
                        <div class="row">
                          <div  class="col col-md-3 col-sm-1">
                            <?php foreach($komputer_1 as $data):?>
                                  <button type="button" class="btn btn-primary btn-sm" style="margin-bottom:10px;" data-toggle="modal" data-target="#pinkomModal<?php echo $data->kode_komputer?>"><?php echo "K".$data->kode_komputer?></button>
                            <?php endforeach?>
                          </div>
                          <div  class="col col-md-5">
                            <?php foreach($komputer_2 as $data):?>
                            <button type="button" class="btn btn-primary btn-sm" style="margin-bottom:10px;" data-toggle="modal" data-target="#pinkomModal<?php echo $data->kode_komputer?>"><?php echo "K".$data->kode_komputer?></button>
                            <?php endforeach?>
                          </div>
                          <div  class="col col-md-3 col-sm-1">
                            <?php foreach($komputer_3 as $data):?>
                            <button type="button" class="btn btn-primary btn-sm" style="margin-bottom:10px;" data-toggle="modal" data-target="#pinkomModal<?php echo $data->kode_komputer?>"><?php echo "K".$data->kode_komputer?></button>
                            <?php endforeach?>
                          </div>
                      </div>
              <!-- ========== List komputer Ku Bopsku============-->
      <?php }else{?>
              <!-- ========== List komputer ============-->
              <div class="row">
                        <!-- ========== List komputer 1 ============-->
                          <div  class="col col-md-3 col-sm-1">
                            <?php foreach($simpansemua_1 as $value):?>
                              <?php if($value == "0"){?>
                                  <button type="button" class="btn btn-danger btn-sm" style="margin-bottom:10px;" data-toggle="modal">BO</button>
                              <?php }else{?>
                                  <button type="button" class="btn btn-primary btn-sm" style="margin-bottom:10px;" data-toggle="modal" data-target="#pinkomModal<?php echo $value?>"><?php echo "K".$value?></button>
                              <?php }?>
                            <?php endforeach?>
                          </div>
                        <!-- ========== List komputer 1 ============-->
                          <div  class="col col-4">
                            <?php foreach($simpansemua_2 as $value):?>
                                <?php if($value == "0"){?>
                                    <button type="button" class="btn btn-danger btn-sm" style="margin-bottom:10px;" data-toggle="modal">BO</button>
                                <?php }else{?>
                                    <button type="button" class="btn btn-primary btn-sm" style="margin-bottom:10px;" data-toggle="modal" data-target="#pinkomModal<?php echo $value?>"><?php echo "K".$value?></button>
                                <?php }?>
                              <?php endforeach?>
                          </div>
                          <div  class="col col-md-3 col-sm-1">
                          <?php foreach($simpansemua_3 as $value):?>
                                <?php if($value == "0"){?>
                                    <button type="button" class="btn btn-danger btn-sm" style="margin-bottom:10px;" data-toggle="modal">BO</button>
                                <?php }else{?>
                                    <button type="button" class="btn btn-primary btn-sm" style="margin-bottom:10px;" data-toggle="modal" data-target="#pinkomModal<?php echo $value?>"><?php echo "K".$value?></button>
                                <?php }?>
                              <?php endforeach?>
                          </div>
                      </div>
              <!-- ========== List komputer ============-->
      <?php }?>

             </div>
           </div>
         </div>
       </div>
     </div>

  
<!-- Modal komputer 1 -->
<?php foreach($komputer_1 as $data):?>
<div class="modal fade" id="pinkomModal<?php echo $data->kode_komputer?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Peminjaman Komputer</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <!-- body -->
      <div class="modal-body" style="font-color:black;">
        <form action="<?php echo site_url('mahasiswa/peminjaman/simpan_pinjam_komputer')?>" method="POST">
            <div class="form-group">
                <label for="usr">Kode Komputer:</label>
                <input type="text" class="form-control" id="usr" name="xkomputer" value="<?php echo $data->kode_komputer?>" readonly>
            </div>
            <div class="form-group">
                <label for="pwd">Tanggal Pinjam:</label>
                <input type="date" class="form-control" id="pwd" name="xtanggal" value="<?php echo $tanggal?>" readonly>
            </div>
            <label for="sel1">Laboratorium :</label>
            <select class="form-control" id="sel1" name="xlab" readonly>
              <option value="<?php echo $idlab?>"><?php echo $nama_lab?></option>
            </select>
            <div class="form-group">
              <label for="sel1">Lama Peminjaman :</label>
              <select class="form-control" id="sel1" name="xlama">
                  <option value="1">1 Hari</option>
                  <option value="2">2 Hari</option>
                  <option value="3">3 Hari</option>
              </select>
            </div>
                  
            <div class="form-group">
                <label for="pwd">Tujaun Peminjaman</label>
                <input type="text" class="form-control" id="pwd" name="xketerangan" required>
            </div>
            
        </div>
      <!-- body -->
      <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Kirim</button>
        </form>
        </div>
    </div>
  </div>
</div>
<?php endforeach?>
<!-- modal komputer 1 -->

<!-- Modal komputer 3 -->
<?php foreach($komputer_3 as $data):?>
<div class="modal fade" id="pinkomModal<?php echo $data->kode_komputer?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <!-- body -->
      <div class="modal-body" style="font-color:black;">
        <form action="<?php echo site_url('mahasiswa/peminjaman/simpan_pinjam_komputer')?>" method="POST">
            <div class="form-group">
                <label for="usr">Kode Komputer:</label>
                <input type="text" class="form-control" id="usr" name="xkomputer" value="<?php echo $data->kode_komputer?>" readonly>
            </div>
            <div class="form-group">
                <label for="pwd">Tanggal Pinjam:</label>
                <input type="date" class="form-control" id="pwd" name="xtanggal" value="<?php echo $tanggal?>" readonly>
            </div>
            <label for="sel1">Laboratorium :</label>
            <select class="form-control" id="sel1" name="xlab" readonly>
              <option value="<?php echo $idlab?>"><?php echo $nama_lab?></option>
            </select>
            <div class="form-group">
              <label for="sel1">Lama Peminjaman :</label>
              <select class="form-control" id="sel1" name="xlama">
                  <option value="1">1 Hari</option>
                  <option value="2">2 Hari</option>
                  <option value="3">3 Hari</option>
              </select>
            </div>
            
            <div class="form-group">
                <label for="pwd">Tujuan Peminjaman</label>
                <input type="text" class="form-control" id="pwd" name="xketerangan" required>
            </div>
        </div>
      <!-- body -->
      <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Kirim</button>
        </form>
        </div>
    </div>
  </div>
</div>
<?php endforeach?>
<!-- modal komputer 3 -->

<!-- Modal komputer 2 -->
<?php foreach($komputer_2 as $data):?>
<div class="modal fade" id="pinkomModal<?php echo $data->kode_komputer?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <!-- body -->
      <div class="modal-body" style="font-color:black;">
        <form action="<?php echo site_url('mahasiswa/peminjaman/simpan_pinjam_komputer')?>" method="POST">
            <div class="form-group">
                <label for="usr">Kode Komputer:</label>
                <input type="text" class="form-control" id="usr" name="xkomputer" value="<?php echo $data->kode_komputer?>" readonly>
            </div>
            <div class="form-group">
                <label for="pwd">Tanggal Pinjam:</label>
                <input type="date" class="form-control" id="pwd" name="xtanggal" value="<?php echo $tanggal?>" readonly>
            </div>
            <label for="sel1">Laboratorium :</label>
            <select class="form-control" id="sel1" name="xlab" readonly>
              <option value="<?php echo $idlab?>"><?php echo $nama_lab?></option>
            </select>
            <div class="form-group">
              <label for="sel1">Lama Peminjaman :</label>
              <select class="form-control" id="sel1" name="xlama">
                  <option value="1">1 Hari</option>
                  <option value="2">2 Hari</option>
                  <option value="3">3 Hari</option>
              </select>
            </div>
            <div class="form-group">
                <label for="pwd">Tujuan Peminjaman</label>
                <input type="text" class="form-control" id="pwd" name="xketerangan" required>
            </div>
        </div>
      <!-- body -->
      <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Kirim</button>
        </form>
        </div>
    </div>
  </div>
</div>
<?php endforeach?>
<!-- modal komputer 2 -->
    
    



  <script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js')?>"></script>
  <script src="<?php echo base_url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')?>"></script>

  <!-- Plugin JavaScript -->
  <script src="<?php echo base_url('assets/vendor/jquery-easing/jquery.easing.min.js')?>"></script>

  <!-- Custom scripts for this template -->
  <script src="<?php echo base_url('assets/js/grayscale.min.js')?>"></script>

</body>
</html>