<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/js/custom.min.js'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/vendor/fontawesome-free/css/all.min.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/custom.css'); ?> ">
<link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

<body id="page-top">

  <?php
    if(!$this->session->userdata('email_mahasiswa')){
      $this->load->view("mahasiswa/partials/navbar-unlogged.php");
    }else{
      $this->load->view("mahasiswa/partials/navbar-logged.php");
    }

  ?>
      <!-- ini untuk simpan yang tersewa -->
    <?php $tempkomputerterpinjam = array();
    foreach($komputerterpinjam as $kunci){
        array_push($tempkomputerterpinjam, $kunci->kode_komputer);    
    }
    $biarjadisatu = array();
    $simpansemua = array(); // ini variable untuk simpan semua yang terboking atau belum untuk nanti jaga jaga
    ?>
    <!-- ini untuk simpan yang tersewa -->
    <?php foreach($komputer as $data):?>
    <?php if($komputerterpinjam==null){?>
        
    <?php }else{?>
        <?php foreach($komputerterpinjam as $key):?>
        
                <?php if(in_array($data->kode_komputer, $tempkomputerterpinjam)){?>
                                <?php if(in_array($data->kode_komputer, $biarjadisatu)){
                                    
                                    }else{?>
                                <!-- <button type="button" class="btn btn-danger col-12 disabled" style="margin-bottom:10px;"><?php echo $data->kode_komputer?></button> -->
                                    <?php array_push($biarjadisatu, $data->kode_komputer);
                                        array_push($simpansemua, 0);
                                    }?>
                <?php }else{?>
                                <?php if(in_array($data->kode_komputer, $biarjadisatu)){
                                    }else{?>
                                        <!-- <button type="button" class="btn btn-primary col-12" style="margin-bottom:10px;" data-toggle="modal" data-target="#myModal<?php echo $data->id_komputer?>"><?php echo $data->kode_komputer?></button> -->
                                        <?php array_push($biarjadisatu, $data->kode_komputer);
                                        array_push($simpansemua, $data->kode_komputer);
                                    }?>
                <?php }?>
            </div>
        <?php endforeach?>
    <?php }?>
    <?php endforeach?>
    <?php 

    print_r($simpansemua);
    print_r($tempkomputerterpinjam);
    ?>
    </div>
  <!-- Header -->
  <div class="body-rent-form">
    <div class="container d-flex h-100 align-items-center">
        <div class="container" style="color:white;">
        <br>
          <div class="register-form" style="width: 60rem; margin: 0 auto;">

            <div class="col-md-12"><h1>Peminjaman Komputer</h1></div>
<div class="form-group">
    <form action="<?php echo site_url('mahasiswa/peminjaman/cek_komputer')?>" method="POST">
        <label for="usr">cek tanggal:</label>
        <input type="date" class="form-control" id="usr" name="xtanggal" value="<?php echo $tanggal?>"onchange="this.form.submit();">
    </form>  
</div>
<div class="row">
    <?php if($komputerterpinjam==null){?>
        <?php foreach($komputer as $data):?>
        <div class="col-2">
        <button type="button" class="btn btn-primary col-12" style="margin-bottom:10px;" data-toggle="modal" data-target="#myModal<?php echo $data->kode_komputer?>"><?php echo $data->kode_komputer?></button>
        </div>
        <?php endforeach?>
    <?php }else{?>
        <!-- ini untuk fixnya -->
        <?php foreach($simpansemua as $value){
            if($value=="0"){?>
            <div class="col-2">
                <button type="button" class="btn btn-danger col-12" style="margin-bottom:5px;" data-toggle="modal" data-target="#myModal<?php echo $value?>">-</button>
            </div>
            <?php }else{?>
            <div class="col-2">
                <button type="button" class="btn btn-primary col-12" style="margin-bottom:5px;" data-toggle="modal" data-target="#myModal<?php echo $value?>"><?php echo $value?></button>
            </div>
            <?php }?>
            <?php }?>
            <?php }?>
        </div>
        <!-- ini untuk fixnya -->

      


  </div>

  
<!-- Modal -->
<?php foreach($komputer as $data):?>
<div class="modal" id="myModal<?php echo $data->kode_komputer?>">
    <div class="modal-dialog">
      <div class="modal-content text-success">
    
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Pinjam Komputer + meja mejanya </h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body" style="font-color:black;">
        <form action="<?php echo site_url('mahasiswa/peminjaman/simpan_pinjam_komputer')?>" method="POST">
            <div class="form-group">
                <label for="usr">Kode Komputer:</label>
                <input type="text" class="form-control" id="usr" name="xkomputer" value="<?php echo $data->kode_komputer?>" readonly>
            </div>
            <div class="form-group">
                <label for="pwd">Tanggal Pinjam:</label>
                <input type="date" class="form-control" id="pwd" name="xtanggal" value="<?php echo $tanggal?>" readonly>
            </div>
            <div class="form-group">
                <label for="pwd">Alasan Minjam</label>
                <input type="text" class="form-control" id="pwd" name="xketerangan">
            </div>
        </div>
    
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Submit</button>
        </form>
        </div>
        
      </div>
    </div>
  </div>
<?php endforeach?>
    </div>
  </div>
    
    



  <script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js')?>"></script>
  <script src="<?php echo base_url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')?>"></script>

  <!-- Plugin JavaScript -->
  <script src="<?php echo base_url('assets/vendor/jquery-easing/jquery.easing.min.js')?>"></script>

  <!-- Custom scripts for this template -->
  <script src="<?php echo base_url('assets/js/grayscale.min.js')?>"></script>

</body>
</html>