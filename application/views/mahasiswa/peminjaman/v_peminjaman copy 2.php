<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>
<?php
ini_set( "display_errors", 0); 
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/js/custom.min.js'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/vendor/fontawesome-free/css/all.min.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/custom.css'); ?> ">
<link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

<body id="page-top">

  <?php
    if(!$this->session->userdata('email_mahasiswa')){
      $this->load->view("mahasiswa/partials/navbar-unlogged.php");
    }else{
      $this->load->view("mahasiswa/partials/navbar-logged.php");
    }

  ?>
    <!-- Navigation -->
  <!-- <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="#page-top">SIPOLAR</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#">Menu 1</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#">Menu 2</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#">Menu 3</a>
          </li>

          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#signup"><div id="btn-signin" class="btn btn-primary">Sign In</div></a>
          </li>

        </ul>
      </div>
    </div>
  </nav> -->

  <!-- Header -->
  <div class="body-rent-form">
    <div class="container d-flex h-100 align-items-center" style="width: 700px;">
        <div class="container" style="color:white;">
<br>
          <div class="register-form">

            <div class="col-md-7"><h1>Peminjaman Ruang</h1></div>
              <form action="<?php echo site_url('mahasiswa/peminjaman/cek_ruangan')?>" method="POST">               
                <div class="form-group">
                    <label for="pwd">Taggal Peminjaman</label>
                    <input type="date" class="form-control" id="pwd" name="xtanggal" value="<?php echo $tanggal?>" onchange="this.form.submit();">
                </div>
              </form>
                  <form action="<?php echo site_url('mahasiswa/peminjaman/simpan_pinjam')?>" method="POST">
                <div class="row">
                  <div class="col">
                    <label for="sel1">Jam Mulai</label>
                    <select class="form-control" id="sel1" name="xjammulai" required>
                        <?php foreach($jam as $data):?>
                            <option value="<?php echo $data->jam_ke?>"><?php echo "Jam ke ".$data->jam_ke." = ".$data->mulai;?></option>
                        <?php endforeach?>
                    </select>
                    </div>

                  <div class="col">    
                    <label for="sel1">Jam Selesai</label>
                    <select class="form-control" id="sel1" name="xjamselesai" required>
                        <?php foreach($jam as $data):?>
                            <option value="<?php echo $data->jam_ke?>"><?php echo "Jam ke ".$data->jam_ke." = ".$data->mulai;?></option>
                        <?php endforeach?>
                    </select>
                    </div> 
                </div>                

              <div class="row" hidden> 
                <div class="col" >
                    <label for="usr">Nomor Induk Mahasiswa</label>
                    <input type="text" class="form-control" id="usr" name="xnim" value="<?php echo $this->session->userdata('nim');?>" readonly>
                </div>
                <div class="col" >
                    <label for="usr">Nama</label>
                    <input type="text" class="form-control" id="usr" name="xnama" value="<?php echo $this->session->userdata('nama_mahasiswa');?>" readonly>
                </div>
                <div class="col" hidden>
                    <label for="usr">tanggal</label>
                    <input type="text" class="form-control" id="usr" name="xtanggal" value="<?php echo $tanggal?>" readonly>
                </div>
              </div>

                <div class="form-group">
                    <label for="sel1">Ruangan / Laboratorium</label>
                    <select class="form-control" id="sel1" name="xruangan">
                        <?php foreach($ruang as $data):?>
                            <option value="<?php echo $data->id_ruang?>"><?php echo $data->nama_ruang;?></option>
                        <?php endforeach?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="pwd">Keterangan / Tujuan Peminjaman</label>
                    <textarea  class="form-control" id="pwd" name="xketerangan" required></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Kirim</button>
            </form>
                </div>
                       
          <!-- <div class="col md-7 regis-form">             
            <form action="<?php echo site_url('mahasiswa/peminjaman/simpan_pinjam')?>" method="POST">
              <div class="row"> <div class="form-group" >
                    <label for="usr">Nim:</label>
                    <input type="text" class="form-control" id="usr" name="xnim" value="<?php echo $this->session->userdata('nim');?>">
                </div>
                <div class="form-group" >
                    <label for="usr">Nama:</label>
                    <input type="text" class="form-control" id="usr" name="xnama" value="<?php echo $this->session->userdata('nama_mahasiswa');?>">
                </div></div>
               
                <div class="form-group">
                    <label for="sel1">Ruangan:</label>
                    <select class="form-control" id="sel1" name="xruangan">
                        <?php foreach($ruang as $data):?>
                            <option value="<?php echo $data->id_ruang?>"><?php echo $data->nama_ruang;?></option>
                        <?php endforeach?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="pwd">Taggal pinjam:</label>
                    <input type="date" class="form-control" id="pwd" name="xtanggal">
                </div>
                <div class="form-group">
                    <label for="sel1">Jam mulai:</label>
                    <select class="form-control" id="sel1" name="xjammulai">
                        <?php foreach($jam as $data):?>
                            <option value="<?php echo $data->id?>"><?php echo "Jam ke ".$data->jam_ke." = ".$data->pukul;?></option>
                        <?php endforeach?>
                    </select>
                </div>                
                <div class="form-group">
                    <label for="sel1">Jam selesai:</label>
                    <select class="form-control" id="sel1" name="xjamselesai">
                        <?php foreach($jam as $data):?>
                            <option value="<?php echo $data->id?>"><?php echo "Jam ke ".$data->jam_ke." = ".$data->pukul;?></option>
                        <?php endforeach?>
                    </select>
                </div>                
                <div class="form-group">
                    <label for="pwd">Keterangan:</label>
                    <input type="text" class="form-control" id="pwd" name="xketerangan">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
            </div> -->
        </div>
    </div>

  </div>

  <script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js')?>"></script>
  <script src="<?php echo base_url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')?>"></script>

  <!-- Plugin JavaScript -->
  <script src="<?php echo base_url('assets/vendor/jquery-easing/jquery.easing.min.js')?>"></script>

  <!-- Custom scripts for this template -->
  <script src="<?php echo base_url('assets/js/grayscale.min.js')?>"></script>

</body>
</html>