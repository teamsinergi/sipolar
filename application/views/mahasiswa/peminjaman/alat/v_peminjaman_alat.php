<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>

<style type="text/css">

  .ui-menu .ui-widget .ui-widget-content .ui-autocomplete .ui-front {
    max-width: 200px;

  }
  .ui-menu-item-wrapper{
    background-color: #fff;
    padding: 10px;
    list-style: none;
    max-width: 500px;
    max-height: 200px;
  }

  .ui-menu-item-wrapper ul li{
    list-style-type: none;
  }

  .ui-menu li{
    list-style: none;

  }
</style>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.11/css/select2.min.css" rel="stylesheet" />


<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/js/custom.min.js'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/vendor/fontawesome-free/css/all.min.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/custom.css'); ?> ">
<link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

<body id="page-top">

  <?php
    if(!$this->session->userdata('email_mahasiswa')){
      $this->load->view("mahasiswa/partials/navbar-unlogged.php");
    }else{
      $this->load->view("mahasiswa/partials/navbar-logged.php");
    }

  ?>


  <!-- Header -->
  <div class="body-rent-form" >
    <div class="container d-flex h-100 align-items-center" style="width: 700px;">
        <div class="container" style="color:white;">
<br>
          <div class="register-form" >

            <div class="col-md-7"><h1>Peminjaman Alat</h1></div>
                  <form action="<?php echo site_url('mahasiswa/peminjaman/simpan_pinjam_alat')?>" method="POST">
              <div class="row" hidden> 
                <div class="col" >
                    <label for="usr">Nomor Induk Mahasiswa</label>
                    <input type="text" class="form-control" name="xnim" style="width:500px;" value="<?php echo $this->session->userdata('nim');?>" readonly>
                </div>
                <div class="col" >
                    <label for="usr">Nama</label>
                    <input type="text" class="form-control" name="xnama" style="width:500px;" value="<?php echo $this->session->userdata('nama_mahasiswa');?>" readonly>
                </div>
              </div>
              <div class="form-group">
                  <label>Nama Alat</label>
                  <input type="text" class="form-control" id="nalat" placeholder="Nama Alat" name="xnama_alat">
              </div>
              <div class="form-group" hidden>
                  <label>Id</label>
                  <input type="text" class="form-control" id="title" name="xid_alat" >
              </div>
                <div class="row">
                  <div class="col">
                   <label for="pwd">Tanggal pinjam</label>
                    <input type="date" class="form-control" id="pwd" name="xtanggal" required>
                  </div>
              </div>
              <div class="form-group">
                  <label>Lama Pinjam</label>
                  <select class="form-control" id="sel1" name="xlama" required>
                    <option value="1">1 Hari</option>
                    <option value="2">2 Hari</option>
                    <option value="3">3 Hari</option>
                  </select>
              </div>
              <div class="form-group">
                  <label>Keterangan / Tujuan Peminjaman</label>
                  <input type="text" class="form-control" id="title" name="xtujuan" required>
              </div>                
                <button type="submit" class="btn btn-primary">Kirim</button>
            </form>
                </div>
       
        </div>
    </div>

  </div>

  <script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js')?>"></script>

  <script src="<?php echo base_url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')?>"></script>

  <!-- Plugin JavaScript -->
  <script src="<?php echo base_url('assets/vendor/jquery-easing/jquery.easing.min.js')?>"></script>

  <!-- Custom scripts for this template -->
  <script src="<?php echo base_url('assets/js/grayscale.min.js')?>"></script>

            <script src="<?php echo base_url().'assets/js/bootstrap.js'?>" type="text/javascript"></script>
            <script src="<?php echo base_url().'assets/js/jquery-ui.js'?>" type="text/javascript"></script>

  <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.full.js"></script>
            <script type="text/javascript">

                $(document).ready(function(){
                    $('#nalat').autocomplete({
                            source: "<?php echo site_url('mahasiswa/peminjaman/get_autocomplete');?>",
                            select: function (event, ui) {
                                $('[name="xnama_alat"]').val(ui.item.label);
                                $('[name="xid_alat"]').val(ui.item.id); 

                            }
                        });
                });

                // In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
    $('#xnalat').select2();
});
              </script>

</body>
</html>
