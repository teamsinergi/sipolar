
<script src="<?php echo base_url('../assets/vendor/jquery/jquery.min.js')?>"></script>
  <script src="<?php echo base_url('../assets/vendor/bootstrap/js/bootstrap.bundle.min.js')?>"></script>

  <!-- Plugin JavaScript -->
  <script src="<?php echo base_url('../assets/vendor/jquery-easing/jquery.easing.min.js')?>"></script>

  <!-- Custom scripts for this template -->
  <script src="<?php echo base_url('../assets/js/custom.  min.js')?>"></script>





<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="<?php echo site_url('home')?>">SIPOLAR</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
          <!--   <a class="nav-link js-scroll-trigger" href="#">Menu 1</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#">Menu 2</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#">Menu 3</a>
          </li> -->

          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo base_url('mahasiswa/login');?>"><div id="btn-signin" class="btn btn-primary">Masuk</div></a>
          </li>
          
        </ul>
      </div>
    </div>
  </nav>