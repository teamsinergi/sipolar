
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="<?php echo site_url('home')?>">SIPOLAR</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
        <!-- <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo site_url('mahasiswa/peminjaman/pinjam_komputer_fungsi')?>">Pinjam komputer</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo site_url('mahasiswa/peminjaman')?>">Pinjam Ruangan</a>
          </li> -->

          <li class="dropdown" style="height: 60px;">
            <a class="nav-link js-scroll-trigger" href="#" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Peminjaman <i class="fa fa-chevron-down"></i> </a>
             <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item"href="<?php echo site_url('mahasiswa/peminjaman')?>">Pinjam Ruangan</a>
                <a class="dropdown-item" href="<?php echo site_url('mahasiswa/peminjaman/pinjam_komputer_fungsi')?>">Pinjam Komputer</a>
                <a class="dropdown-item"href="<?php echo site_url('mahasiswa/peminjaman/peminjaman_alat')?>">Pinjam Alat</a>
            </div>
          </li>

          <li class="dropdown" style="height: 60px;">
            <a class="nav-link js-scroll-trigger" href="#" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Riwayat <i class="fa fa-chevron-down"></i> </a>
             <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="<?php echo site_url('mahasiswa/riwayat')?>">Riwayat Peminjaman Ruangan</a>
                <a class="dropdown-item" href="<?php echo site_url('mahasiswa/riwayat/riwayat_komputer')?>">Riwayat Peminjaman Komputer</a>
                <a class="dropdown-item" href="<?php echo site_url('mahasiswa/riwayat/riwayat_alat')?>">Riwayat Peminjaman Alat</a>

              </div>
          </li>

          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo site_url('mahasiswa/profile')?>">Profile</a>
          </li>

          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo site_url('mahasiswa/login/keluar')?>"><div id="btn-signout" class="btn btn-outline-warning">Sign Out</div></a>
          </li>
          
        </ul>
      </div>
    </div>
  </nav>

 <!--   -->