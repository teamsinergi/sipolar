<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<HTML>
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META http-equiv="X-UA-Compatible" content="IE=8">
<TITLE>Surat Peminjaman Ruang</TITLE>
<META>
<STYLE type="text/css">

body {margin-top: 0px;margin-left: 0px;}

#page_1 {position:relative; overflow: hidden;margin: 29px 0px 111px 68px;padding: 0px;border: none;width: 726px;}

#page_1 #p1dimg1 {position:absolute;top:0px;left:0px;z-index:-1;width:663px;height:95px;}
#page_1 #p1dimg1 #p1img1 {width:663px;height:95px;}




#page_2 {position:relative; overflow: hidden;margin: 82px 0px 478px 76px;padding: 0px;border: none;width: 718px;}





.dclr {clear:both;float:none;height:1px;margin:0px;padding:0px;overflow:hidden;}

.ft0{font: bold 19px 'Calibri';line-height: 22px;}
.ft1{font: bold 19px 'Calibri';line-height: 23px;}
.ft2{font: bold 19px 'Calibri';line-height: 20px;}
.ft3{font: 11px 'Arial';color: #545454;line-height: 12px;}
.ft4{font: 11px 'Arial';text-decoration: underline;color: #1155cc;line-height: 14px;}
.ft5{font: 11px 'Arial';color: #545454;line-height: 14px;}
.ft6{font: bold 16px 'Calibri';line-height: 19px;}
.ft7{font: 15px 'Times New Roman';line-height: 17px;}
.ft8{font: 15px 'Times New Roman';line-height: 23px;}
.ft9{font: 1px 'Times New Roman';line-height: 1px;}
.ft10{font: 14px 'Times New Roman';line-height: 16px;}
.ft11{font: 15px 'Times New Roman';line-height: 22px;}

.p0{text-align: left;padding-left: 201px;margin-top: 4px;margin-bottom: 0px;}
.p1{text-align: left;padding-left: 283px;margin-top: 1px;margin-bottom: 0px;}
.p2{text-align: left;padding-left: 284px;margin-top: 0px;margin-bottom: 0px;}
.p3{text-align: left;padding-left: 205px;margin-top: 0px;margin-bottom: 0px;}
.p4{text-align: left;padding-left: 253px;margin-top: 0px;margin-bottom: 0px;}
.p5{text-align: left;padding-left: 148px;margin-top: 19px;margin-bottom: 0px;}
.p6{text-align: left;padding-left: 106px;margin-top: 3px;margin-bottom: 0px;}
.p7{text-align: left;padding-left: 220px;margin-top: 26px;margin-bottom: 0px;}
.p8{text-align: left;padding-left: 8px;margin-top: 23px;margin-bottom: 0px;}
.p9{text-align: left;padding-left: 26px;margin-top: 8px;margin-bottom: 0px;}
.p10{text-align: left;padding-left: 26px;margin-top: 9px;margin-bottom: 0px;}
.p11{text-align: left;padding-left: 8px;margin-top: 34px;margin-bottom: 0px;}
.p12{text-align: left;padding-left: 8px;margin-top: 8px;margin-bottom: 0px;}
.p13{text-align: left;padding-left: 8px;padding-right: 76px;margin-top: 33px;margin-bottom: 0px;}
.p14{text-align: left;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p15{text-align: center;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p16{text-align: center;padding-left: 1px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p17{text-align: left;padding-left: 68px;margin-top: 85px;margin-bottom: 0px;}
.p18{text-align: left;padding-left: 245px;margin-top: 33px;margin-bottom: 0px;}
.p19{text-align: left;padding-left: 187px;margin-top: 8px;margin-bottom: 0px;}
.p20{text-align: left;padding-left: 229px;margin-top: 85px;margin-bottom: 0px;}
.p21{text-align: left;padding-left: 239px;margin-top: 0px;margin-bottom: 0px;}
.p22{text-align: left;padding-left: 72px;margin-top: 11px;margin-bottom: 0px;}
.p23{text-align: left;margin-top: 34px;margin-bottom: 0px;}
.p24{text-align: left;padding-left: 1px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p25{text-align: left;padding-right: 76px;margin-top: 34px;margin-bottom: 0px;}
.p26{text-align: left;margin-top: 32px;margin-bottom: 0px;}
.p27{text-align: left;padding-left: 353px;margin-top: 33px;margin-bottom: 0px;}
.p28{text-align: left;padding-left: 437px;margin-top: 34px;margin-bottom: 0px;}
.p29{text-align: left;padding-left: 371px;margin-top: 84px;margin-bottom: 0px;}

.td0{padding: 0px;margin: 0px;width: 334px;vertical-align: bottom;}
.td1{padding: 0px;margin: 0px;width: 186px;vertical-align: bottom;}
.td2{padding: 0px;margin: 0px;width: 125px;vertical-align: bottom;}
.td3{padding: 0px;margin: 0px;width: 294px;vertical-align: bottom;}
.td4{padding: 0px;margin: 0px;width: 419px;vertical-align: bottom;}

.tr0{height: 19px;}
.tr1{height: 51px;}
.tr2{height: 20px;}
.tr3{height: 24px;}
.tr4{height: 26px;}
.tr5{height: 25px;}

.t0{width: 520px;margin-left: 49px;margin-top: 2px;font: 15px 'Times New Roman';}
.t1{width: 419px;margin-left: 18px;margin-top: 5px;font: 15px 'Times New Roman';}

</STYLE>
</HEAD>

<BODY>
<DIV id="page_1">
<DIV id="p1dimg1">
<IMG src="data:image/jpg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCABeApcDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD0PwX4L8K3XgXw9cXHhrRpp5dMtnkkksImZ2MSkkkrkknnNbn/AAgng/8A6FTQ/wDwXQ//ABNHgT/knnhr/sFWv/opa6CgDn/+EE8H/wDQqaH/AOC6H/4mmyeCPBkMTyy+F9BSNFLM7WEICgdSTt4FbGpajZ6Rp0+oahcJb2sC75JH6Af1J6ADkkgCvHbkeJ/jDqMiWsjaZ4RjkKrIf+W+0jkrwXYnnH3Vx1yOajG+r2JlK2nUb4j8VfDyzuTp3h3wVo+s6gzeWjR6dF5W85AAwuZDnHCjBB4atXwH4VudRvLy48U+AtAtLOaJHtVXTbdPLYE5XbguMg5+bpt960NDn8HeGtGZfCptTfPFE5uLpT5rQvIEaZs7WMaBWdgNowuflDBqW6ufFfiKKK40ucmNFnVZrVzDFI8Uy7ZY+Tv3oxwjtsyh5I5NtK1kvvITd7tnUf8ACCeD/wDoVND/APBdF/8AE1yvjrwYlvp9o3hLwVoM9ys++fdp9tgxhT8uGAzkkfd54961Ly38U3N7rEhsZDZ3UMiWsaX2yWJ41UxfdYBAziQ5ViT5iBsBajurfxBo8C2tpd6pNJFYx/ZAALhbq7y5kE0kgYonEfVkGGO05HEpWZbd0cDo3izwbb3f9m+Mfh9pOk3i/flTSk2DOSC0ZXeoxt6bs5zwK9StPB/ga/tY7qz8OeHri3kGUlisYWVu3BC4NYd1rXh/xgbq11rSUn0yLaYbgxSmRd7qkWMID+9yGUoxyvDAY55GTRfEXwsu5NX8N3R1jw0rubu3MgOza21gwHRhjG9RwVO4ADBpxUttGQpNb6o9Q/4QTwf/ANCpof8A4Lof/iaP+EE8H/8AQqaH/wCC6H/4mrugeINN8TaTHqWl3Amt3JUgjDIw6qw7Ef1BGQQa06yasa7nP/8ACCeD/wDoVND/APBdD/8AE0f8IJ4P/wChU0P/AMF0P/xNdBRQBwfgvwX4VuvAvh64uPDWjTTy6ZbPJJJYRMzsYlJJJXJJPOa3P+EE8H/9Cpof/guh/wDiaPAn/JPPDX/YKtf/AEUtdBQBz/8Awgng/wD6FTQ//BdD/wDE0f8ACCeD/wDoVND/APBdD/8AE10FFAHP/wDCCeD/APoVND/8F0P/AMTR/wAIJ4P/AOhU0P8A8F0P/wATXQUUAc//AMIJ4P8A+hU0P/wXQ/8AxNH/AAgng/8A6FTQ/wDwXQ//ABNdBRQBz/8Awgng/wD6FTQ//BdD/wDE0f8ACCeD/wDoVND/APBdD/8AE10FFAHP/wDCCeD/APoVND/8F0P/AMTR/wAIJ4P/AOhU0P8A8F0P/wATXQUUAc//AMIJ4P8A+hU0P/wXQ/8AxNH/AAgng/8A6FTQ/wDwXQ//ABNdBRQBz/8Awgng/wD6FTQ//BdD/wDE0f8ACCeD/wDoVND/APBdD/8AE10FFAHP/wDCCeD/APoVND/8F0P/AMTR/wAIJ4P/AOhU0P8A8F0P/wATXQUUAc//AMIJ4P8A+hU0P/wXQ/8AxNH/AAgng/8A6FTQ/wDwXQ//ABNdBRQBz/8Awgng/wD6FTQ//BdD/wDE0f8ACCeD/wDoVND/APBdD/8AE10FFAHP/wDCCeD/APoVND/8F0P/AMTR/wAIJ4P/AOhU0P8A8F0P/wATXQUUAc//AMIJ4P8A+hU0P/wXQ/8AxNH/AAgng/8A6FTQ/wDwXQ//ABNdBRQBz/8Awgng/wD6FTQ//BdD/wDE0f8ACCeD/wDoVND/APBdD/8AE10FFAHP/wDCCeD/APoVND/8F0P/AMTR/wAIJ4P/AOhU0P8A8F0P/wATXQUUAc//AMIJ4P8A+hU0P/wXQ/8AxNH/AAgng/8A6FTQ/wDwXQ//ABNdBRQBz/8Awgng/wD6FTQ//BdD/wDE0f8ACCeD/wDoVND/APBdD/8AE10FFAHP/wDCCeD/APoVND/8F0P/AMTR/wAIJ4P/AOhU0P8A8F0P/wATXQUUAc//AMIJ4P8A+hU0P/wXQ/8AxNH/AAgng/8A6FTQ/wDwXQ//ABNdBRQBz/8Awgng/wD6FTQ//BdD/wDE0f8ACCeD/wDoVND/APBdD/8AE10FFAHP/wDCCeD/APoVND/8F0P/AMTR/wAIJ4P/AOhU0P8A8F0P/wATXQUUAc//AMIJ4P8A+hU0P/wXQ/8AxNH/AAgng/8A6FTQ/wDwXQ//ABNdBRQBz/8Awgng/wD6FTQ//BdD/wDE0f8ACCeD/wDoVND/APBdD/8AE10FFAHP/wDCCeD/APoVND/8F0P/AMTR/wAIJ4P/AOhU0P8A8F0P/wATXQUUAc//AMIJ4P8A+hU0P/wXQ/8AxNH/AAgng/8A6FTQ/wDwXQ//ABNdBRQBz/8Awgng/wD6FTQ//BdD/wDE0f8ACCeD/wDoVND/APBdD/8AE10FFAHP/wDCCeD/APoVND/8F0P/AMTR/wAIJ4P/AOhU0P8A8F0P/wATXQUUAc//AMIJ4P8A+hU0P/wXQ/8AxNH/AAgng/8A6FTQ/wDwXQ//ABNdBRQBz/8Awgng/wD6FTQ//BdD/wDE0f8ACCeD/wDoVND/APBdD/8AE10FFAHP/wDCCeD/APoVND/8F0P/AMTR/wAIJ4P/AOhU0P8A8F0P/wATXQUUAc//AMIJ4P8A+hU0P/wXQ/8AxNH/AAgng/8A6FTQ/wDwXQ//ABNdBRQBz/8Awgng/wD6FTQ//BdD/wDE0f8ACCeD/wDoVND/APBdD/8AE10FFAHP/wDCCeD/APoVND/8F0P/AMTR/wAIJ4P/AOhU0P8A8F0P/wATXQUUAc//AMIJ4P8A+hU0P/wXQ/8AxNH/AAgng/8A6FTQ/wDwXQ//ABNdBRQB5P8AF/wn4b0z4W6zeWHh/SrS6j8jZNBZRxuuZ4wcMBkZBI/Gitj42/8AJIdd/wC3f/0ojooA6DwJ/wAk88Nf9gq1/wDRS10Fc/4E/wCSeeGv+wVa/wDopab458QN4Y8G6jqkWPtCIEgBI/1jEKpweuM7segNNK7sJuyuec+MLu7+JPxBi8G6dKU0rT3L3sq8ZZThzz1252AY+8SemCOv1uCWDSfDQ8I6kba2Ey2sIgkDo0boQH2kN5uzaGI/u7ySuN6+Vxane/Dv4fadLYSeTrfiCT7XJMQGaK3T/VjBJB3bt2SOjMD0r1v4f263+jweJrvTIrPUtQRnkETMEYMRmQIeFMm1GOPvYUk1tNWS7GUXd+Zc03w3YaHdSalfXkbXEtw9wBnyreOZxh3jQsSGYdcs2Mtt2hiK2v7W03/oIWn/AH+X/GuW1HQJdc8cyf2jBO2mxW4ETD5VzgcZHuT+VWp/BPhi1iMtwnkxggF5LgqOfcmuB1a0m7RVlpq7foekqGGjGPNN3avok9+m+5v/ANrab/0ELT/v8v8AjR/a2m/9BC0/7/L/AI1if8IB4e/59pP+/wA3+Nc4vhdvPnQeFnmVHKg/bjEBycYJJ3jGDnA6kY4qJ1a8d4r72/8A20unQws72m/mor85HWalpWl+IFmktbiBNRWNhDdwPuaF9jojkKRuK+Y5GehJxiuSudWm8Hz2lk9rawNL5VrDaNdOllBbg5chmUGaUjJLbTjKryzBZIpNGvdL1nTbqz0lNPlWXIjk1NCbjkfKucdQSOM/erqtUsJdb0SW51HQY2vrQSPb2huAwm+XIRmXGUZgMoeDtGc1rh8RKbcZxtb7v0MsXhI0kp053T9L79le/qm/M8vt7iP4W+OLS+spDJ4Q15FkRiWOxDgg/wB7KbgRkElWxyc492ByMjpXzxpniXUvidaaz4c1mWKS8ljN7pW1QgjmjyfKUdwykjLE4AJ5NekfB/xE2u+B4beYg3Gmt9lbkZZAAUOO3Hy++wmuypF2u9zgpyV7LY7+iiisTU5/wJ/yTzw1/wBgq1/9FLXQVz/gT/knnhr/ALBVr/6KWugoAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA8/+Nv8AySHXf+3f/wBKI6KPjb/ySHXf+3f/ANKI6KAOg8Cf8k88Nf8AYKtf/RS1lfEzwjqXjPQLbTtOntYmjuRO5uGYA4VlABUH+8e1avgT/knnhr/sFWv/AKKWt2WaKCJpZpEjjUZZ3YAD6k002ndCaurM+Zvi/NGPHP8AZsEQit9Ms4LSJR/cC7x/6Hj8K+h7TQrePwvZ6HdAzQQW0Vu3zFd2xQOowe1fOPxbw/xJ1OZCGimSCSN1OQ6+SgyD3HBr6alSLUrDCTSCKZAyywSFGweQQwrWsr00iKLtUbvbU4ODwrpzeO7qxewf+zkgDou58bsL/FnPUnvXQSeBNBK/uLeS2fBG+OQscEEEYfcOh9M1g61Y61pGsgwNreoafJHlFhu5Nyt3yQD/AC5z7Gs26l1K6h8t9D8QuufuzXDyqe33XiIzzweoODXg3oUnKMqet30/JqO3bU+mccTXUJwraWXX13Tkte+h6jBDHbwRwRLtjjUIoznAAwOtc4PFV8LmeL/hHr2RY3KgxAkjBIw2QBnAB4JGGHNUIvF+sQwpGPCWosEULud3ZjjuSY+TT/8AhM9Z/wChQv8A83/+N11SxdJ2tJr/ALdf+Rwwy+tG/NBSv/eivykUNdu9S1bVNHu49A1FFsZ/NdWj5YbkOB/3zXa6ZfSahamaWyuLNgxXy5xhj05+nP6Vxc02r+KPEOjO+h3FhHYzea7zbtpG5SeSo5+Xp716DTwvvTnNN2bW6tfT0T02Fj/cp06UkrpPZ3tr3u077+R8t6Cp8OfGW2tbMkJb6wbNS3XyzIYj/wCOk17V4G8E33hLxD4huWksxp2ozb7eGEsWjUO5QHIAGFfGBmvF5D5/xw46HxEBken2jFfTtvdW90rNbzxTKrFWMbhgCOoOO9erWb080eLSS18mS0UUVzm5z/gT/knnhr/sFWv/AKKWugrn/An/ACTzw1/2CrX/ANFLXQUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAef8Axt/5JDrv/bv/AOlEdFHxt/5JDrv/AG7/APpRHRQB0HgT/knnhr/sFWv/AKKWsj4s6OdX+HmoeXGZJrQrdxgHGNh+Y/8AfBetfwJ/yTzw1/2CrX/0Utb7KrqVYAqRggjgimnZ3E1dWPlvxWDq/g7wvr0eG8m2Ok3IQcRPCSY8n1ZGz+Fe8fDbW7bWPh/pcsTKptIFtZlLZKNGAvPpkAN9GFeSS6Rb+E/Fur+C9XkEOha0oNrcuciBskwyH/dbKNyM9ScV2/wn8Ba74Sub671W4jiS4TyvscZD7ircSFhwOM4Ho3OMYrepZx/IxhdSOx8SW+oalaRJo+qxWro+6T96V3Ajj5lyR9O+faucPhvxiCQfEUYxyc3kvH/jtWby20dY7qzuLXWpLe3hMKj7OCm1VRCEOPm3hQpznqegyae1lpcl1dzppmtS3MmnmzlO4FjB8yH7zcsditnljlSMgmuCphYVJczbXo2j0qOOqUYckYxfrFN/eyj/AMI94uyB/wAJJDk4I/0yTv0/h70Dw94uIyPEsJHXIvJPf/Z9j+RqzD4e8N2tnJCItV8pYxE2VkkJjj86E4IB4ZWkHHIVlxtO2oIfC+gR/ZZRDrSxrClt5buADGqvH+8/i+4XwTztkwn91c/qFP8Aml/4EzX+1K38kP8AwFG14Z0/WdPvLg6rrUV3GUCrEJjIQ2euWAI9MDrn2FbWsava6Jot3qt24FvbRGQncBu9FGeMk4A9SRXI3NhoV3La6vJBrq3cFr/Z0bCN/Pjj5BcjBYEgsu7r83HPIi+IXgXUdd8IWul6HeFI7El0s5SMTAD5E3nptGQM8HIyeM11UqUYJRu7eepxV68qsnNpX8lZfcjxv4dr5PiC68SXYD22jW0l5K0vSSUgrGm7oHZ2BGeu016f8CdKkt/Dmo6vMJA9/cBVLdHSMH5ge/zO4/4DXBX2lT21tpnw60oq+rXk6z6tIvRZMZWIsvVI1yzdRnkdxX0Po+l22iaPaaZaLiC1iWJMgZOB1OO5PJ9ya6astPX8jlpR19C7RRRXObnP+BP+SeeGv+wVa/8Aopa6Cuf8Cf8AJPPDX/YKtf8A0UtdBQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQB5/wDG3/kkOu/9u/8A6UR0UfG3/kkOu/8Abv8A+lEdFAHQeBP+SeeGv+wVa/8Aopa6Cuf8Cf8AJPPDX/YKtf8A0UtdBQBynj7wXB410E2u5Ir6Al7Sdhwrd1Pfa2ADj0BwcYrkfBvxGOiXH/CJeMibS+smFvFeOMRuo4XeT04xh+hHJx1PrNc54t8E6P4xsvK1CErcIMQ3UWBJH7Z7j2PHPY81cZK3LLYiUXe8dy54m1+38M+HL3WLgB1t48omceY54Vc4OMkgZ7de1cRpfxz8MXflJfQX1hIy5kZoxJGh9AVO4/XaK5PUPDnjXwVZSWE9pF4m8Mc7oGQuEXnkD78TAZOVJVSc8msnw74U8K/EG9mtdH/tXRb5I2nkjl2XNuihgu1Wyrk/N3960jCFrshzleyPXR8WPBDIXGupgetvKD+WzNZmofG3whZuq273t/kZ3W9vtAPofMKn8ga5U/s+y+YAPEqFO5Nkc/lv/rWT4j+HPh7wHbQ3OvX2raktwWWEWMMcKhlAOHZmbGfYdjQo0m9HcTlUtseweB/Gdr420V76GH7NNFKY5rcyByndTnAyCO+ByCO1YXjf4oWehO+kaKp1HXZD5SRwrvWJycANjq2eijJyOcd+B0a28XeJbE2Pg/RYvDWhTffuA7BpRyctOw3ychgNgwA2DxXpvgr4aaP4NUXC/wCm6nzm8lXBUHjCLztGPqeTzjik4xi7v7ilKUlZfeVPhv8AD+TwtFPqmrS/aNcvc+c+7d5Sk5K7j95ieWPrgDpk9/RRWUpOTuzRJJWQUUUUhnP+BP8Aknnhr/sFWv8A6KWugrn/AAJ/yTzw1/2CrX/0UtdBQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQB5/8bf+SQ67/wBu/wD6UR0UfG3/AJJDrv8A27/+lEdFAHQeBP8Aknnhr/sFWv8A6KWugrwvw18ffCujeFdI0u40/WWnsrKG3kaOGIqWRApIzIDjI9BWp/w0d4P/AOgbrn/fiH/47QB7BRXj/wDw0d4P/wCgbrn/AH4h/wDjtH/DR3g//oG65/34h/8AjtAHsFRLa26XT3SwRC4dQjShBvZR0BPUivJP+GjvB/8A0Ddc/wC/EP8A8do/4aO8H/8AQN1z/vxD/wDHaAPYKimtoLgxGeCOUxP5kZdA2xsEbhnocE8+9eSf8NHeD/8AoG65/wB+If8A47R/w0d4P/6Buuf9+If/AI7QB7BRXj//AA0d4P8A+gbrn/fiH/47R/w0d4P/AOgbrn/fiH/47QB7BRXj/wDw0d4P/wCgbrn/AH4h/wDjtH/DR3g//oG65/34h/8AjtAHsFFeP/8ADR3g/wD6Buuf9+If/jtH/DR3g/8A6Buuf9+If/jtAHoHgT/knnhr/sFWv/opa6CvC/DXx98K6N4V0jS7jT9ZaeysobeRo4YipZECkjMgOMj0Fan/AA0d4P8A+gbrn/fiH/47QB7BRXj/APw0d4P/AOgbrn/fiH/47R/w0d4P/wCgbrn/AH4h/wDjtAHsFFeP/wDDR3g//oG65/34h/8AjtH/AA0d4P8A+gbrn/fiH/47QB7BRXj/APw0d4P/AOgbrn/fiH/47R/w0d4P/wCgbrn/AH4h/wDjtAHsFFeP/wDDR3g//oG65/34h/8AjtH/AA0d4P8A+gbrn/fiH/47QB7BRXj/APw0d4P/AOgbrn/fiH/47R/w0d4P/wCgbrn/AH4h/wDjtAHsFFeP/wDDR3g//oG65/34h/8AjtH/AA0d4P8A+gbrn/fiH/47QB7BRXj/APw0d4P/AOgbrn/fiH/47R/w0d4P/wCgbrn/AH4h/wDjtAHsFFeP/wDDR3g//oG65/34h/8AjtH/AA0d4P8A+gbrn/fiH/47QB7BRXj/APw0d4P/AOgbrn/fiH/47R/w0d4P/wCgbrn/AH4h/wDjtAHsFFeP/wDDR3g//oG65/34h/8AjtH/AA0d4P8A+gbrn/fiH/47QB7BRXj/APw0d4P/AOgbrn/fiH/47R/w0d4P/wCgbrn/AH4h/wDjtAHsFFeP/wDDR3g//oG65/34h/8AjtH/AA0d4P8A+gbrn/fiH/47QB7BRXj/APw0d4P/AOgbrn/fiH/47R/w0d4P/wCgbrn/AH4h/wDjtAHsFFeP/wDDR3g//oG65/34h/8AjtH/AA0d4P8A+gbrn/fiH/47QB7BRXj/APw0d4P/AOgbrn/fiH/47R/w0d4P/wCgbrn/AH4h/wDjtAHsFFeP/wDDR3g//oG65/34h/8AjtH/AA0d4P8A+gbrn/fiH/47QB7BRXj/APw0d4P/AOgbrn/fiH/47R/w0d4P/wCgbrn/AH4h/wDjtAHsFFeP/wDDR3g//oG65/34h/8AjtH/AA0d4P8A+gbrn/fiH/47QB7BRXj/APw0d4P/AOgbrn/fiH/47R/w0d4P/wCgbrn/AH4h/wDjtAHsFFeP/wDDR3g//oG65/34h/8AjtH/AA0d4P8A+gbrn/fiH/47QB7BRXj/APw0d4P/AOgbrn/fiH/47R/w0d4P/wCgbrn/AH4h/wDjtAHsFFeP/wDDR3g//oG65/34h/8AjtH/AA0d4P8A+gbrn/fiH/47QB7BRXj/APw0d4P/AOgbrn/fiH/47R/w0d4P/wCgbrn/AH4h/wDjtAHsFFeP/wDDR3g//oG65/34h/8AjtH/AA0d4P8A+gbrn/fiH/47QB7BRXj/APw0d4P/AOgbrn/fiH/47R/w0d4P/wCgbrn/AH4h/wDjtAHsFFeP/wDDR3g//oG65/34h/8AjtH/AA0d4P8A+gbrn/fiH/47QB7BRXj/APw0d4P/AOgbrn/fiH/47R/w0d4P/wCgbrn/AH4h/wDjtAHsFFeP/wDDR3g//oG65/34h/8AjtH/AA0d4P8A+gbrn/fiH/47QB7BRXj/APw0d4P/AOgbrn/fiH/47R/w0d4P/wCgbrn/AH4h/wDjtAHsFFeP/wDDR3g//oG65/34h/8AjtH/AA0d4P8A+gbrn/fiH/47QB7BRXj/APw0d4P/AOgbrn/fiH/47R/w0d4P/wCgbrn/AH4h/wDjtAHsFFeP/wDDR3g//oG65/34h/8AjtH/AA0d4P8A+gbrn/fiH/47QB7BRXj/APw0d4P/AOgbrn/fiH/47R/w0d4P/wCgbrn/AH4h/wDjtAHQfG3/AJJDrv8A27/+lEdFecfET41+G/F3gTUtDsLLVY7q68rY88UYQbZUc5IkJ6Ke1FAH/9k=" id="p1img1"></DIV>


<DIV class="dclr"></DIV>
<P class="p0 ft0">Laboratorium Program Studi Teknik Informatika</P>
<P class="p1 ft1">Fakultas Teknologi Industri</P>
<P class="p2 ft2">Universitas Ahmad Dahlan</P>
<P class="p3 ft3">Jalan Ring Road Selatan, Tamanan, Banguntapan, Bantul Yogyakarta 55166</P>
<P class="p4 ft5">Email: <A href="mailto:laboratorium@tif.uad.ac.id"><SPAN class="ft4">laboratorium@tif.uad.ac.id</SPAN></A><A href="mailto:laboratorium@tif.uad.ac.id">. </A>Web: labs.tif.uad.ac.id</P>
<P class="p5 ft1">FORMULIR PENGAJUAN PEMINJAMAN RUANG</P>
<P class="p6 ft1">LABORATORIUM PROGRAM STUDI TEKNIK INFORMATIKA</P>
<P class="p7 ft6">No : <NOBR>…………/Lab-Inf/………../20….</NOBR></P>
<P class="p8 ft7">Yang bertanda tangan di bawah ini:</P>
<P class="p9 ft7">Nama lengkap : ……………………………………………….</P>
<P class="p10 ft7">NIM/NIP : ……………………………………………….</P>
<P class="p9 ft7">Program Studi/ Fakultas : ………………………………………...</P>
<P class="p9 ft7">No. HP : ……………………………………………….</P>
<P class="p9 ft7">Email<SPAN style="padding-left:60px;">: ………………………………………………..</SPAN></P>
<P class="p11 ft7">Mengajukan peminjaman ruangan lab : ……………………………………………….</P>
<P class="p11 ft7">Untuk kepentingan : …………………………………………………</P>
<P class="p12 ft7">pelaksanaan :</P>
<P class="p9 ft7">Tanggal : ………….……….. s.d. ………………..</P>
<P class="p10 ft7">Waktu Kerja : …………….…….. s.d. ………………..</P>
<P class="p13 ft8">Demikian form peminjaman ruangan laboratorium ini saya ajukan dan saya siap bertanggung jawab apabila ada kerusakan pada laboratorium yang dipinjam.</P>
<TABLE cellpadding=0 cellspacing=0 class="t0">
<TR>
	<TD class="tr0 td0"><P class="p14 ft9">&nbsp;</P></TD>
	<TD class="tr0 td1"><P class="p15 ft10">Yogyakarta, ………..……2019</P></TD>
</TR>
<TR>
	<TD class="tr1 td0"><P class="p14 ft7">Pembimbing TA/Riset/Kelompok Studi</P></TD>
	<TD class="tr1 td1"><P class="p16 ft7">Peminjam,</P></TD>
</TR>
</TABLE>
<P class="p17 ft7">………………………………….<SPAN style="padding-left:117px;">………………………………….</SPAN></P>
<P class="p18 ft7">Menyetujui & Mengetahui,</P>
<P class="p19 ft7">Kepala Laboratorium ………………………</P>
<P class="p20 ft7">………………………………….</P>
</DIV>
<DIV id="page_2">


<P class="p21 ft1">SURAT PERNYATAAN</P>
<P class="p22 ft1">BERSEDIA MENGGANTI KERUSAKAN RUANGAN YANG DIPINJAM</P>
<P class="p23 ft7">Yang bertanda tangan di bawah ini:</P>
<TABLE cellpadding=0 cellspacing=0 class="t1">
<TR>
	<TD class="tr2 td2"><P class="p14 ft7">Nama lengkap</P></TD>
	<TD class="tr2 td3"><P class="p24 ft7">: .……………………………………………….</P></TD>
</TR>
<TR>
	<TD class="tr3 td2"><P class="p14 ft7">NIM/NIP</P></TD>
	<TD class="tr3 td3"><P class="p24 ft7">: ……………………………………………….</P></TD>
</TR>
<TR>
	<TD colspan=2 class="tr4 td4"><P class="p14 ft7">Program Studi/ Fakultas : …………………………………………..</P></TD>
</TR>
<TR>
	<TD class="tr5 td2"><P class="p14 ft7">No. HP</P></TD>
	<TD class="tr5 td3"><P class="p24 ft7">: ………………………………………………..</P></TD>
</TR>
<TR>
	<TD class="tr4 td2"><P class="p14 ft7">Email</P></TD>
	<TD class="tr4 td3"><P class="p14 ft10">: …………………………………………………..</P></TD>
</TR>
</TABLE>
<P class="p25 ft11">bersedia mengganti semua kerusakan ruangan dan semua barang yang ada di dalamnya yang saya pinjam dan siap menanggung semua biaya yang harus dipertanggungjawabkan sesuai ketentuan laboratorium.</P>
<P class="p26 ft7">Demikian surat pernyataan ini saya buat tanpa ada paksaan dari pihak manapun.</P>
<P class="p27 ft7">Yogyakarta, ……………..……20……</P>
<P class="p28 ft7">Peminjam,</P>
<P class="p29 ft7">………………………………….</P>
</DIV>
</BODY>
</HTML>
