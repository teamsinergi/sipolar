<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/laporan/style.css')?>">
</head>
<body>

<div class="main-container">
	<div class="top-banner2">
		<h4>SURAT PERNYATAAN <br>
BERSEDIA MENGGANTI KERUSAKAN RUANGAN YANG DIPINJAM</h4>
	</div>

<div class="main-content">
	<div class="data-diri">
	Yang bertanda tangan di bawah ini:
	<ul>
		<li>Nama lengkap  &emsp;: .........................................................</li>
		<li>NIM/NIP  &emsp;&emsp;&emsp;: .........................................................</li>
		<li>Program Studi/ Fakultas : .............................................</li>
		<li>No. HP &emsp;&emsp;&emsp;&emsp;: ........................................................</li>
		<li>Email&emsp;&emsp;&emsp;&emsp;&ensp; : .........................................................</li>
</ul>


<div class="demikian">

bersedia mengganti semua kerusakan ruangan dan semua barang yang ada di dalamnya yang saya pinjam dan
siap menanggung semua biaya yang harus dipertanggungjawabkan sesuai ketentuan laboratorium.

</div>

<br>


<div class="demikian">

Demikian surat pernyataan ini saya buat tanpa ada paksaan dari pihak manapun

</div>
<div class="yogyakarta">
	Yogyakarta, ................. 2019
</div>
<br>
<div class="peminjam">
	Peminjam,
</div>
<br><br><br><br><br>
<div class="titik2">
	&emsp;&emsp;&emsp;&emsp;&emsp;..........................................
	<br><br>
</div>


</div>
</div>

</body>
</html>