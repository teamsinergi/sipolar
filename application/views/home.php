<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/vendor/fontawesome-free/css/all.min.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/js/custom.min.js'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/custom.css'); ?> ">
<link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

<body id="page-top">

  <?php
    if(!$this->session->userdata('email_mahasiswa')){
      $this->load->view("mahasiswa/partials/navbar-unlogged.php");
    }else{
      $this->load->view("mahasiswa/partials/navbar-logged.php");
    }

  ?>
    <!-- Navigation -->
  <!-- <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="#page-top">SIPOLAR</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#">Menu 1</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#">Menu 2</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#">Menu 3</a>
          </li>

          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#signup"><div id="btn-signin" class="btn btn-primary">Sign In</div></a>
          </li>

        </ul>
      </div>
    </div>
  </nav> -->

  <!-- Header -->
  <header class="masthead">
    <div class="container d-flex h-100 align-items-center">
      <div class="mx-auto text-center">
        <div id="main-img" class="mx-auto my-0"><img src="<?php echo base_url('assets/img/logo.png')?>"> </div>
        <h2 class="text-white-50 mx-auto mt-2 mb-5">Sistem Pengelolaan Lab Riset<br>Praktikum, Riset, KKSC, Multimedia dan Rendering</h2>
        <!-- <a href="#about" class="btn btn-primary js-scroll-trigger">MONITORING</a> -->
      </div>
    </div>

  </header>
<script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js')?>"></script>
  <script src="<?php echo base_url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')?>"></script>

  <!-- Plugin JavaScript -->
  <script src="<?php echo base_url('assets/vendor/jquery-easing/jquery.easing.min.js')?>"></script>

  <!-- Custom scripts for this template -->
  <script src="<?php echo base_url('assets/js/grayscale.min.js')?>"></script>
  

</body>
</html>